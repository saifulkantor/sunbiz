<?php
$namawebsite = explode("||",$pengaturan[1]->data);
$detailwebsite = $pengaturan[2]->data;
$contact = $pengaturan[3]->data;
$email = $pengaturan[4]->data;
$address = $pengaturan[5]->data;
$sosmed = explode("||",$pengaturan[6]->data);
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'PT. Indraco') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    		function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- Styles -->
    <link href="{{ url('/template/mygarden/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('/template/mygarden/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />

    <script type="text/javascript" src="{{ url('/template/mygarden/js/jquery-2.1.4.min.js') }}"></script>
    <!-- //js -->
    <link href="{{ url('/template/mygarden/css/mislider.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('/template/mygarden/css/mislider-custom.css') }}" rel="stylesheet" type="text/css" />
    <!-- font-awesome-icons -->
    <link href="{{ url('/template/mygarden/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ url('/css/home.css') }}" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Bree+Serif&amp;subset=latin-ext" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- banner -->
<?php if (Route::getCurrentRoute()->uri() == '/') {
    echo '<div class="banner">';
} else {
  echo '<div class="banner1">';
}
?>
		<div class="container">
			<div class="w3_agileits_banner_main_grid">
				<div class="w3_agile_logo">
					<h1><a href="{{ url('/') }}"><span>{{$namawebsite[0]}}</span> {{$namawebsite[1]}}<i>  {{$namawebsite[2]}}</i></a></h1>
				</div>
				<div class="agile_social_icons_banner">
					<ul class="agileits_social_list">
            <li><a href="https://www.facebook.com/<?=$sosmed[0]?>" target="_blank" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/<?=$sosmed[1]?>" target="_blank" class="agile_twitter"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://www.twitter.com/<?=$sosmed[2]?>" target="_blank" class="w3_agile_dribble"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            @guest
            @else
            <?php $user = auth()->user(); if($user->level=='member') { ?>
            <li><a href="{{ url('/transaksi/keranjang') }}" class="" style="position: relative;"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
              <span id="jmlkeranjang" style="display: block;position: absolute;top: -3px;right: -11px;background: red;padding: 3px;width: 15px;height: 15px;border-radius: 200px;font-size: 8px;"><?=count($user->getkeranjang())?></span>
              </a>
            </li>
          <?php } ?>
            @endguest
					</ul>
				</div>
				<div class="agileits_w3layouts_menu">
					<div class="shy-menu">
						<a class="shy-menu-hamburger">
							<span class="layer top"></span>
							<span class="layer mid"></span>
							<span class="layer btm"></span>
						</a>
						<div class="shy-menu-panel">
							<nav class="menu menu--horatio link-effect-8" id="link-effect-8">
								<ul class="w3layouts_menu__list">
                  <li><a href="{{ url('/data') }}">Pricelist</a></li>
									<li><a href="{{ url('/contactus') }}">Contact Us</a></li>
                  @guest
                      <li>
                          <a href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <?php /* <li>
                              <a href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li> */ ?>
                      @endif
                  @else
                      <li><a href="{{ url('/backend') }}">{{ __('Backend') }}</a></li><li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                      </li>
                  @endguest
								</ul>
							</nav>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
      <?php if (Route::getCurrentRoute()->uri() == '/') { ?>
			<div class="w3_banner_info">
				<div class="w3_banner_info_grid">
					<h3 class="test">Pusat informasi harga sayur-mayur,buah-buahan dan rempah-rempah nusantara</h3>
					<p>SunBiz Porong Pusat Grosir Buah dan sayur dengan kualitas terbaik berasal dari ladang
					   petani khusus untuk konsumen</p>
					<ul>
						<li><a href="{{ url('/contactus') }}" class="w3l_contact">Contact Us</a></li>
						<li><a href="{{ url('/data') }}" class="w3ls_more">Daftar Harga</a></li>
					</ul>
				</div>
			</div>
			<div class="thim-click-to-bottom">
				<a href="#banner_bottom" class="scroll">
					<i class="fa  fa-chevron-down"></i>
				</a>
			</div>
      <?php } ?>
		</div>
	</div>
<!-- banner -->

    @yield('content')

    <!-- footer -->
    	<div class="footer">
    		<div class="container">
    			<div class="w3agile_footer_grids">
    				<div class="col-md-3 agileinfo_footer_grid">
    					<div class="agileits_w3layouts_footer_logo">
    						<h2><a href="{{ url('/') }}"><span>{{$namawebsite[0]}}</span> {{$namawebsite[1]}}<i>{{$namawebsite[2]}}</i></a></h2>
                <p style="margin-top: 30px;white-space: pre-line">{{$detailwebsite}}</p>
    					</div>
    				</div>
    				<div class="col-md-4 agileinfo_footer_grid">
    					<h3>Contact Info</h3>
    					<p style="white-space: pre-line"><?=$contact?></p>
    					<p style="white-space: pre-line"><?=$address?></p>
    					<ul class="agileits_social_list">
    						<li><a href="https://www.facebook.com/<?=$sosmed[0]?>" target="_blank" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    						<li><a href="https://www.instagram.com/<?=$sosmed[1]?>" target="_blank" class="agile_twitter"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
    						<li><a href="https://www.twitter.com/<?=$sosmed[2]?>" target="_blank" class="w3_agile_dribble"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
    					</ul>
    				</div>
    				<div class="col-md-2 agileinfo_footer_grid agileinfo_footer_grid1">
    					<h3>Menu</h3>
    					<ul class="w3layouts_footer_nav">
    						<li><a href="{{ url('/') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Home</a></li>
    						<li><a href="{{ url('/data') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Pricelist</a></li>
    						<li><a href="{{ url('/contactus') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Contact Us</a></li>
    					</ul>
    				</div>
    				<div class="col-md-3 agileinfo_footer_grid">
    					<?php /*<h3>Blog Posts</h3>
    					<div class="agileinfo_footer_grid_left">
    						<a href="#" data-toggle="modal" data-target="#myModal"><img src="{{ url('/template/mygarden/images/6.jpg') }}" alt=" " class="img-responsive" /></a>
    					</div>
    					<div class="agileinfo_footer_grid_left">
    						<a href="#" data-toggle="modal" data-target="#myModal"><img src="{{ url('/template/mygarden/images/2.jpg') }}" alt=" " class="img-responsive" /></a>
    					</div>
    					<div class="agileinfo_footer_grid_left">
    						<a href="#" data-toggle="modal" data-target="#myModal"><img src="{{ url('/template/mygarden/images/5.jpg') }}" alt=" " class="img-responsive" /></a>
    					</div>
    					<div class="agileinfo_footer_grid_left">
    						<a href="#" data-toggle="modal" data-target="#myModal"><img src="{{ url('/template/mygarden/images/3.jpg') }}" alt=" " class="img-responsive" /></a>
    					</div>
              */ ?>
    					<div class="clearfix"> </div>
    				</div>
    				<div class="clearfix"> </div>
    			</div>
    		</div>
    		<div class="w3_agileits_footer_copy">
    			<div class="container">
    				<p>© {{ date('Y') }} {{ config('app.name', 'PT. Indraco') }}. All rights reserved.</p>
    			</div>
    		</div>
    	</div>
    <!-- //footer -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d89a2fec22bdd393bb7754c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
<!-- Scripts -->
<?php /*
<script type="text/javascript" src="{{ url('/') }}/js/loader.js"></script>
<script type="text/javascript">google.charts.load('current', {packages: ['corechart']}); google.charts.setOnLoadCallback(drawChart);</script>
*/ ?>
<!-- stats -->
<script type="text/javascript" src="{{ url('/template/mygarden/js/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ url('/template/mygarden/js/jquery.countup.js') }}"></script>
		<script>
			$('.counter').countUp();
		</script>
<!-- //stats -->
<!-- mislider -->
	<script src="{{ url('/template/mygarden/js/mislider.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
        jQuery(function ($) {
            var slider = $('.mis-stage').miSlider({
                //  The height of the stage in px. Options: false or positive integer. false = height is calculated using maximum slide heights. Default: false
                stageHeight: 380,
                //  Number of slides visible at one time. Options: false or positive integer. false = Fit as many as possible.  Default: 1
                slidesOnStage: false,
                //  The location of the current slide on the stage. Options: 'left', 'right', 'center'. Defualt: 'left'
                slidePosition: 'center',
                //  The slide to start on. Options: 'beg', 'mid', 'end' or slide number starting at 1 - '1','2','3', etc. Defualt: 'beg'
                slideStart: 'mid',
                //  The relative percentage scaling factor of the current slide - other slides are scaled down. Options: positive number 100 or higher. 100 = No scaling. Defualt: 100
                slideScaling: 150,
                //  The vertical offset of the slide center as a percentage of slide height. Options:  positive or negative number. Neg value = up. Pos value = down. 0 = No offset. Default: 0
                offsetV: -5,
                //  Center slide contents vertically - Boolean. Default: false
                centerV: true,
                //  Opacity of the prev and next button navigation when not transitioning. Options: Number between 0 and 1. 0 (transparent) - 1 (opaque). Default: .5
                navButtonsOpacity: 1,
            });
        });
    </script>
<!-- //mislider -->

<!-- text-effect -->
    <script type="text/javascript" src="{{ url('/template/mygarden/js/jquery.transit.js') }}"></script>
    <script type="text/javascript" src="{{ url('/template/mygarden/js/jquery.textFx.js') }}"></script>
		<script type="text/javascript">
			$(document).ready(function() {
					$('.test').textFx({
						type: 'fadeIn',
						iChar: 100,
						iAnim: 1000
					});
			});
		</script>
<!-- //text-effect -->

<!-- menu -->
	<script>
		$(function() {

			initDropDowns($("div.shy-menu"));

		});

		function initDropDowns(allMenus) {

			allMenus.children(".shy-menu-hamburger").on("click", function() {

				var thisTrigger = jQuery(this),
					thisMenu = thisTrigger.parent(),
					thisPanel = thisTrigger.next();

				if (thisMenu.hasClass("is-open")) {

					thisMenu.removeClass("is-open");

				} else {

					allMenus.removeClass("is-open");
					thisMenu.addClass("is-open");
					thisPanel.on("click", function(e) {
						e.stopPropagation();
					});
				}

				return false;
			});
		}
	</script>
<!-- //menu -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{ url('/template/mygarden/js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ url('/template/mygarden/js/easing.js') }}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->

<script src="{{ url('/template/mygarden/js/bootstrap.js') }}"></script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});


function sortTable(id_table,kolom=0,type='text',sortby='ASC') {
  var table, rows, switching, i, x, y, varx, vary, shouldSwitch;
  table = document.getElementById(id_table);
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[kolom];
      y = rows[i + 1].getElementsByTagName("TD")[kolom];
      //check if the two rows should switch place:
      varx = (type!='number')?x.getAttribute("value"):parseFloat(x.getAttribute("value"));
      vary = (type!='number')?y.getAttribute("value"):parseFloat(y.getAttribute("value"));
      if (varx > vary && sortby=='ASC') {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      } else if (varx < vary && sortby=='DESC') {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}
function imageExists(image_url){
    var http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status != 404;
}

	</script>

<!-- //here ends scrolling icon -->
</html>
