<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Sisko') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- DataTables -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/css/jquery.fancybox.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ url('/') }}/template/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="{{ url('/') }}/css/main.css">
</head>
<body class="hold-transition skin-green sidebar-mini" style="background-color:#222d32">
<?php
$path=explode("/", \Request::path());
$menudipilih=isset($path[1])?$path[1]:'';
?>


<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">{{ config('app.name', 'Sisko') }}</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{ config('app.name', 'Sisko') }}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
					<?php if (Auth::user()->level=='contributor') { ?>
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-building"></i> Kotaku
            </a>
            <ul class="dropdown-menu">
              <li class="header">List Kota</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
									<?php foreach($kotaku as $key => $kota) {
										echo '<li>
					                    <a href="'.url('/').'/backend/komoditas?id_kota='.$kota->id.'">
					                      <div class="pull-left">
					                        <i class="fa fa-building"></i>
					                      </div>
					                      <h4>
					                        '.$kota->nama.'
					                      </h4>
					                    </a>
					                  </li>';
					                  if ($key>=4) break;
									} ?>
                </ul>
              </li>
              <li class="footer"><a href="{{ url('/') }}/pasar">Lihat Semua Pasar</a></li>
            </ul>
          </li>
				<?php } ?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <li class="user-body">
			             <p>{{ Auth::user()->name }} </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <a class="btn btn-primary" style="background:#3c8dbc;color:white" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Utama</li>
        <li<?=($menudipilih=='')?' class="active"':''?>>
          <a href="{{ url('/backend') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
				<?php if (Auth::user()->level=='admin') { ?>
        <li<?=($menudipilih=='kota')?' class="active"':''?>>
          <a href="{{ url('/admin/kota') }}">
            <i class="fa fa-home"></i> <span>Kota</span>
          </a>
        </li>
				<li<?=($menudipilih=='member')?' class="active"':''?>>
          <a href="{{ url('/admin/member') }}">
            <i class="fa fa-user"></i> <span>Member</span>
          </a>
        </li>
				<li<?=($menudipilih=='komoditas')?' class="active"':''?>>
          <a href="{{ url('/admin/komoditas') }}">
            <i class="fa fa-database"></i> <span>Komoditas</span>
          </a>
        </li>
				<li<?=($menudipilih=='transaksi')?' class="active"':''?>>
          <a href="{{ url('/admin/transaksi') }}">
            <i class="fa fa-money"></i> <span>Transaksi</span>
          </a>
        </li>
				<li<?=($menudipilih=='artikel')?' class="active"':''?>>
          <a href="{{ url('/admin/artikel') }}">
            <i class="fa fa-file"></i> <span>Artikel</span>
          </a>
        </li>
				<li<?=($menudipilih=='pengaturan')?' class="active"':''?>>
          <a href="{{ url('/admin/pengaturan') }}">
            <i class="fa fa-gear"></i> <span>Pengaturan</span>
          </a>
        </li>
			<?php } else if (Auth::user()->level=='contributor') { ?>
				<li<?=($menudipilih=='komoditas')?' class="active"':''?>>
          <a href="{{ url('/backend/komoditas') }}">
            <i class="fa fa-database"></i> <span>Data Komoditas</span>
          </a>
        </li>
			<?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?=ucfirst($menudipilih)?></h1>
      </section>

      <!-- Main content -->
      <section class="content">
      @yield('content')
    </section>
    <p class="footer" style="padding:5px 15px;margin:0">&copy; <?=date('Y')?> PT INDRACO All Right Reserved.</p>
  </div>

    <!-- jQuery 3 -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/raphael/raphael.min.js"></script>
    <?=(Route::getCurrentRoute()->uri()=='/')?'<script src="'.url('/').'/template/adminlte/bower_components/morris.js/morris.min.js"></script>':'';?>
    <!-- Sparkline -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="{{ url('/') }}/template/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{ url('/') }}/template/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('/') }}/template/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ url('/') }}/template/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- DataTables -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/template/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{ url('/') }}/template/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('/') }}/template/adminlte/dist/js/adminlte.min.js"></script>
    <script src="{{ url('/') }}/js/jquery.fancybox.min.js"></script>
		<script src="{{ url('/') }}/js/he.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('/') }}/template/adminlte/dist/js/demo.js"></script>
		<script src="{{ url('/') }}/js/main.js"></script>

</body>
</html>
