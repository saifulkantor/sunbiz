@extends('layouts.home')

@section('content')
<!-- bootstrap-pop-up -->
<?php
$namawebsite = explode("||",$pengaturan[1]->data);
?>
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Germinate
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<section>
					<div class="modal-body">
						<img src="{{ url('/template/mygarden/images/4.jpg') }}" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum
							exercitationem ullam corporis suscipit laboriosam,
							nisi ut aliquid ex ea commodi consequatur? Quis autem
							vel eum iure reprehenderit qui in ea voluptate velit
							esse quam nihil molestiae consequatur, vel illum qui
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit
								esse quam nihil molestiae consequatur.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- banner-bottom -->
	<div id="banner_bottom" class="banner-bottom">
		<div class="col-md-4 agileits_banner_bottom_left">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid" onclick="location.href='{{ url('/data?kom=1') }}'" style="cursor:pointer">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-pagelines" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">
						<h4>Daftar Harga Sayur</h4>
						<p>Dapatkan Harga sayuran segar berkualitas dari berbagai kota penghasil komoditas sayur-mayur</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 agileits_banner_bottom_left1">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid" onclick="location.href='{{ url('/data?kom=78') }}'" style="cursor:pointer">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-certificate" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">
						<h4>Daftar Harga Buah</h4>
						<p>Dapatkan Informasi harga buah-buahan secara kompetitif dari beberapa kontributor kami.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 agileits_banner_bottom_left2">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-yelp" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">
						<h4>Kemitraan</h4>
						<p>Jadilah bagian dari keluarga besar kami.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- //banner-bottom -->
<!-- welcome -->
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Komoditas andalan kami <span>Sayur Dan Buah</span></h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive" />
			</div>
			<p class="agile_para">Kesegaran sayur dan buah adalah prioritas kami agar sampai ke tangan konsumen.</p>
		</div>
		<div class="mis-stage w3_agileits_welcome_grids">
			<!-- The element to select and apply miSlider to - the class is optional -->
			<ol class="mis-slider">
				<?php
				$listkomoditas = explode(',',$pengaturan[0]->data);
				foreach ($listkomoditas as $key => $value) {
						$idx=array_search($value, array_column($komoditas, 'id'));
						if(file_exists(public_path('images/komoditas/'.$value.'.jpg'))){
						    $image = url('/images/komoditas/'.$value.'.jpg');
						}else{
						    $image = url('/images/komoditas/default.jpg');
						}
						echo '<li class="mis-slide">
							<figure onclick="location.href=\''.url('/data?kom='.$value).'\'">
								<img src="'.$image.'" alt=" " class="img-responsive" />
								<figcaption>'.$komoditas[$idx]->nama.'</figcaption>
							</figure>
						</li>';
				}
				?>
			</ol>
		</div>
		<p class="agile_para"><button class="btn" onclick="location.href='{{ url('/data') }}'">Lihat Lainnya</button></p>
	</div>
<!-- //welcome -->
<!-- welcome-bottom -->
	<div id="welcome_bottom" class="welcome-bottom">
		<div class="col-md-6 wthree_welcome_bottom_left">
      <?php foreach ($komoditasutamajml as $key => $value) {
				if(file_exists(public_path('images/komoditas/'.$value->id.'.jpg'))){
						$image = url('/images/komoditas/'.$value->id.'.jpg');
				}else{
						$image = url('/images/komoditas/default.jpg');
				}
				echo '<div class="col-xs-6 col-md-3" style="cursor:pointer;" onclick="location.href=\''.url('/data?kom='.$value->id).'\'">
				<h5 style="color:white">'.$value->nama.'</h5>
				<div>
					<img src="'.$image.'" alt=" " class="img-responsive" style="width:100%">
				</div>
				<div class="clearfix">
				</div>
				<h5 class="counter pull-left" style="color:white">'.$value->jumlahanak.'</h5><h5 style="color:white"> &nbsp; Komoditas</h5>
			</div>';
      } ?>
			<div class="clearfix"> </div>
		</div>
		<div class="col-md-6 wthree_welcome_bottom_right">
			<div class="agileinfo_grid">
				<figure class="agileits_effect_moses">
					<img src="{{ url('/images/homepage/petani.jpg') }}" alt=" " class="img-responsive" />
					<figcaption>
						<h4>Petani <span>Sejahtera</span></h4>
						<p>Cintai Produk buah dan sayur dari petani kita sendiri</p>
					</figcaption>
				</figure>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- //welcome-bottom -->
<!-- news -->
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Artikel Terbaru <span><?=$namawebsite[0]?> <?=$namawebsite[1]?></span></h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
			</div>
			<p class="agile_para"></p>
			<div class="w3ls_news_grids">
				<?php foreach ($artikel as $key => $value) {
					if(file_exists(public_path('images/artikel/'.$value->id.'.jpg'))){
							$image = url('/images/artikel/'.$value->id.'.jpg');
					}else{
							$image = url('/images/artikel/default.jpg');
					}
					echo '<div class="col-md-4 w3ls_news_grid">
						<div class="w3layouts_news_grid">
							<img src="'.$image.'" alt=" " class="img-responsive" />
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>'.$value->judul.'</h3></div>
							</div>
						</div>
						<div class="agileits_w3layouts_news_grid">
							<ul>
								<li><i class="fa fa-calendar" aria-hidden="true"></i>'.date("d F Y",strtotime($value->updated_at)).'</li>
								<li><i class="fa fa-user" aria-hidden="true"></i><a href="#">'.$value->kategori.'</a></li>
							</ul>
							<h4><a href="'.url('/artikel/'.$value->slug).'">'.$value->judul.'</a></h4>
							<p>'.$value->overview.'</p>
						</div>
					</div>';
				}
?>
				<div class="clearfix"> </div>
			</div>
			<p class="agile_para"><button class="btn" onclick="location.href='{{ url('/artikel') }}'">Lihat Lainnya</button></p>
		</div>
	</div>
<!-- //news -->
<?php /*
<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<h3 class="agileits_w3layouts_head agileinfo_head"><span>Subscribe</span> to our newsletter</h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/12.png') }}" alt=" " class="img-responsive">
			</div>
			<p class="agile_para agileits_para">Morbi viverra lacus commodo felis semper, eu iaculis lectus nulla at sapien blandit sollicitudin.</p>
			<div class="w3ls_news_grids w3ls_newsletter_grids">
				<form action="#" method="post">
					<input name="Your Name" placeholder="Your Name" type="text" required="">
					<input name="Your Email" placeholder="Your Email" type="email" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div>
		</div>
	</div>
<!-- //newsletter -->
*/ ?>
<?php /*
<div class="container" style="display:none">
  @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
  @endif
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Map</div>
                <div class="card-body">

                </div>
            </div>
        </div>
        <div class="col-md-4">

        </div>
        <div style="clear:both;">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">Chart</div>
              <div class="card-body">
                <div id="chartContainer" style="width: 100%; height:400px;text-align:center"><img src="{{ url('/images/loader.gif') }}" height="100%" /></div>
              </div>
          </div>
      </div>
    </div>


</div>
*/ ?>
<script type="text/javascript">
<?php /*
var datakomoditas;
function resetdatakomoditas() {
  datakomoditas=<?=json_encode($datakomoditas);?>;
}
function getavgkomoditas_dataperprov() {
  $.ajax({
      url: "{{ url('/api/getavgkomoditas_dataperprov') }}",
      type: 'POST',
      data: {
        'tanggal':'<?=date('Y-m-d')?>',
      },
      dataType: 'JSON',
      success: function (data) {
        resetdatakomoditas(); var jml=0; var totalharga=0;
        $.each(data, function( index, value ) {
          jml++; totalharga+=value['harga'];
          datakomoditas[value['id']]=[value['nama'],parseFloat(value['harga']),0];
          $('#avgkomoditas_dataperprov .avgkd'+value['id']).html('Rp. '+parseFloat(value['harga']).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        });
        if (jml>0 && totalharga>0) {
          $.each(datakomoditas, function( index, value ) {
            if (index!=0) datakomoditas[index][2] = totalharga/jml;
          });
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  resetdatakomoditas();
  getavgkomoditas_dataperprov();
});

function drawChart() {
  // Define the chart to be drawn.
  var data = google.visualization.arrayToDataTable(datakomoditas);

  // Set chart options
  var options = {
     title : 'Harga Rata2 Tiap Provinsi',
     vAxis: {title: 'Provinsi'},
     hAxis: {title: 'Harga', slantedTextAngle: 75},
     width: '100%',
     height: 400,
     legend: { position: 'none', textStyle:{ fontSize:4 }, },
     chartArea:{left:60,top:10,right:10, bottom:170},
     position: "top",
     seriesType: 'bars',
     series: {1: {type: 'line'}}

  };

  // Instantiate and draw the chart.
  var chart = new google.visualization.ComboChart(document.getElementById('chartContainer'));
  chart.draw(data, options);
}


// CanvasJS

function drawChart() {
  var chart = new CanvasJS.Chart("chartContainer", {
  	animationEnabled: true,
  	theme: "light2", // "light1", "light2", "dark1", "dark2"
  	exportEnabled: true,
  	title:{
  		text: "Grafik Rata Rata Harga Komoditas Per Provinsi Hari ini"
  	},
  	subtitles: [{
  		text: "Semua Harga dalam Rupiah"
  	}],
  	axisX: {
  		valueFormatString: "MMM"
  	},
  	axisY: {
  		includeZero:false,
  		prefix: "$",
  		title: "Harga"
  	},
  	axisY2: {
  		prefix: "$",
  		suffix: "bn",
  		title: "Rata-Rata",
  		tickLength: 0
  	},
  	toolTip: {
  		shared: true
  	},
  	legend: {
  		reversed: true,
  		cursor: "pointer",
  		itemclick: toggleDataSeries
  	},
  	data: [{
  		type: "column",
  		showInLegend: true,
  		name: "Stock Price",
  		dataPoints: datakomoditas
  	},
  	{
  		type: "line",
  		showInLegend: true,
  		name: "Net Income",
  		axisYType: "secondary",
  		dataPoints: datakomoditas
  	}]
  });
  chart.render();
}
function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}
*/ ?>
</script>
@endsection
