@extends('layouts.home')

@section('content')
<!-- news -->
<?php $id_komoditas_utama = isset($_GET['kom'])?(int)htmlspecialchars($_GET['kom']):'';
$pilihkomoditas=''; ?>
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Daftar Harga <span>Komoditas</span></h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
			</div>
			<p class="agile_para"></p>
			<div id="filterdatakomoditas" class="row">
				<div class="form-group col-sm-3">
					<label for="id_komoditas_utama">Grup Komoditas</label>
					<select id="id_komoditas_utama" name="id_komoditas_utama" class="form-control" onchange="loadkomoditas()">
						<?php foreach ($komoditasall as $key => $komoditas) {
							if ($komoditas->id_parent==NULL) {
								echo '<option value="'.$komoditas->id.'">'.$komoditas->nama.'</option>';
								$pilihkomoditas=($id_komoditas_utama==$komoditas->id)?' setTimeout(function(){$(\'#id_komoditas_utama\').val('.$komoditas->id.').trigger(\'change\');}, 800);':$pilihkomoditas;
							} else {
								$pilihkomoditas=($id_komoditas_utama==$komoditas->id)?' $(\'#id_komoditas_utama\').val('.$komoditas->id_parent.').trigger(\'change\'); setTimeout(function(){$(\'#id_komoditas\').val('.$komoditas->id.').trigger(\'change\');}, 800);':$pilihkomoditas;
							}
						} ?>
					</select>
				</div>
				<?php /*
				<div class="form-group col-sm-3">
					<label for="id_komoditas">Komoditas</label>
					<select id="id_komoditas" name="id_komoditas" class="form-control" onchange="getkomoditas_data();">
						<option value="">-- Semua Komoditas --</option>
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="urutkan">Urutkan</label>
					<select id="urutkan" name="urutkan" class="form-control" onchange="urutkan();">
						<option value="">-- Default --</option>
						<option value="kolom1asc">Harga Termurah</option>
						<option value="kolom1desc">Harga Termahal</option>
						<option value="kolom0asc">Nama Komoditas A-Z</option>
						<option value="kolom0desc">Nama Komoditas Z-A</option>
					</select>
				</div>
				*/ ?>
				<div class="form-group col-sm-9 pull-right">
					<label for="urutkan">Cari Komoditas</label>
					<div class="input-group" style="margin:0">
            <input class="form-control" placeholder="Cari Komoditas Di Sini ..." id="filtercari">
            <span class="input-group-addon" style="cursor:pointer" onclick="filterkomoditas()"><i class="fa fa-search"></i></span>
          </div>
				</div>
			</div>
			<table id="tabledatakomoditas" class="table table-condensed table-striped table-bordered">
				<thead>
					<tr><th id="isijudultabel">Komoditas</th><th>Harga</th><th style="width: 100px;">Satuan</th></tr>
				</thead>
				<tbody style="text-align:center">
				</tbody>
				<!-- <tfoot> -->
					<!-- <tr><th>RATA - RATA</th><th id="isihargarata" style="text-align:right">Rp. 0</th><th></th></tr> -->
				<!-- </tfoot> -->
			</table>
		</div>
	</div>
<script type="text/javascript">
var datakomoditas = <?=json_encode($komoditasall)?>; var komoditasdipilih = new Array();
function filterkomoditas() {
  page=1;
	getkomoditas_data();
}
function pilihper(elemen) {
	$('.active').removeClass('active');
	$(elemen).addClass('active');
	getkomoditas_data();
}
function urutkan() {
	switch ($('#urutkan').val()) {
	  case 'kolom0asc':
	    sortTable('tabledatakomoditas',0,'text','ASC');
	    break;
		case 'kolom0desc':
	    sortTable('tabledatakomoditas',0,'text','DESC');
	    break;
		case 'kolom1asc':
	    sortTable('tabledatakomoditas',1,'number','ASC');
	    break;
		case 'kolom1desc':
	    sortTable('tabledatakomoditas',1,'number','DESC');
	    break;
	  default:
			getkomoditas_data();
	}
}
function resetdatakomoditas() {
  $('#tabledatakomoditas tbody').html('<tr><td colspan="5"><img src="{{ url('/images/loader.gif') }}" height="100%" /></td></tr>');
}
function loadkomoditas() {
  var id_komoditas_utama=$('#id_komoditas_utama').val();
  var komoditasoption='<option value="">-- Semua Komoditas --</option>';
	komoditasdipilih = new Array();
  $.each(datakomoditas,function(index, value){
    if (value['id_parent']==id_komoditas_utama) {
			komoditasdipilih.push({'id':value['id'],'nama':value['nama'],'satuan':value['satuan']});
      komoditasoption+='<option value="'+value['id']+'">'+value['nama']+'</option>';
    }
  });
  $('#id_komoditas').html(komoditasoption);
	getkomoditas_data();
}
function getkomoditas_data() {
	resetdatakomoditas();
	$('#urutkan').val('');
  $.ajax({
      url: "{{ url('/api/getkomoditas_dataavg') }}",
      type: 'POST',
      data: {
				'nama':$('#filtercari').val(),
				'tanggal':'{{ date("Y-m-d") }}',
				'id_komoditas':$('#id_komoditas').val(),
				'id_parent':$('#id_komoditas_utama').val(),
      },
      dataType: 'JSON',
      success: function (data) {
				// console.log(data);
        var jml=0; var totalharga=0;
				var isitabel = '';
					$('#isijudultabel').html('Komoditas');
					$.each(data, function( index, value ) {
						isitabel += '<tr id="rowdata'+value['id']+'"><td style="text-align:left;padding:5px!important;" value="'+value['nama'].toLowerCase()+'">'+value['nama']+'</td><td id="isiharga'+value['id']+'" style="padding:5px!important;text-align:right" value="'+parseFloat(value['harga'])+'">Rp. '+parseFloat(value['harga']).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td><td style="padding:5px!important;">'+value['satuan']+'</td></tr>';
		        jml++; totalharga+=parseFloat(value['harga']);
	        });
					isitabel=(isitabel=='')?'<tr><td colspan="9">Data Tidak Ditemukan</td></tr>':isitabel;
					$('#tabledatakomoditas tbody').html(isitabel);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
	loadkomoditas();
  resetdatakomoditas();
	getkomoditas_data();
	$('#filtercari').keyup(function(e){ if(e.keyCode == 13) { filterkomoditas();} });
	<?=$pilihkomoditas?>
});
<?php /*
function drawChart() {
  // Define the chart to be drawn.
  var data = google.visualization.arrayToDataTable(datakomoditas);

  // Set chart options
  var options = {
     title : 'Harga Rata2 Tiap Provinsi',
     vAxis: {title: 'Provinsi'},
     hAxis: {title: 'Harga', slantedTextAngle: 75},
     width: '100%',
     height: 400,
     legend: { position: 'none', textStyle:{ fontSize:4 }, },
     chartArea:{left:60,top:10,right:10, bottom:170},
     position: "top",
     seriesType: 'bars',
     series: {1: {type: 'line'}}

  };

  // Instantiate and draw the chart.
  var chart = new google.visualization.ComboChart(document.getElementById('chartContainer'));
  chart.draw(data, options);
}


// CanvasJS

function drawChart() {
  var chart = new CanvasJS.Chart("chartContainer", {
  	animationEnabled: true,
  	theme: "light2", // "light1", "light2", "dark1", "dark2"
  	exportEnabled: true,
  	title:{
  		text: "Grafik Rata Rata Harga Komoditas Per Provinsi Hari ini"
  	},
  	subtitles: [{
  		text: "Semua Harga dalam Rupiah"
  	}],
  	axisX: {
  		valueFormatString: "MMM"
  	},
  	axisY: {
  		includeZero:false,
  		prefix: "$",
  		title: "Harga"
  	},
  	axisY2: {
  		prefix: "$",
  		suffix: "bn",
  		title: "Rata-Rata",
  		tickLength: 0
  	},
  	toolTip: {
  		shared: true
  	},
  	legend: {
  		reversed: true,
  		cursor: "pointer",
  		itemclick: toggleDataSeries
  	},
  	data: [{
  		type: "column",
  		showInLegend: true,
  		name: "Stock Price",
  		dataPoints: datakomoditas
  	},
  	{
  		type: "line",
  		showInLegend: true,
  		name: "Net Income",
  		axisYType: "secondary",
  		dataPoints: datakomoditas
  	}]
  });
  chart.render();
}
function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}
*/ ?>
</script>
@endsection
