@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/transaksi') }}">Transaksi</a><span>/</span></li>
				<li><i class="fa fa-database" aria-hidden="true"></i>Keranjang</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Keranjang</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div class="container">
    <h3 class="agileits_w3layouts_head">Keranjang <span>Belanja</span></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>
			<?php $subtotal = 0;
			$user = auth()->user();
			$keranjang = $user->getkeranjang();
			$alamat = $user->semuawithalamat(['id_user'=>$user->id]);
			if (count($keranjang)>0) {
			echo '<div class="col-sm-7 col-md-8">
				<input type="hidden" name="id_user" class="form-control" value="'.$user->id.'" />';
			foreach ($keranjang as $key => $value) {
				if(file_exists(public_path('images/komoditas/'.$value->id_komoditas.'.jpg'))){
						$image = url('/images/komoditas/'.$value->id_komoditas.'.jpg');
				}else{
						$image = url('/images/komoditas/default.jpg');
				}
		echo '<div class="row" style="margin-bottom:15px"><div class="col-xs-3 col-sm-2"><img src="'.$image.'" alt=" " class="img-responsive" style="padding: 5px 0px;"></div><div class="col-xs-9 col-sm-10"><span class="pull-right" style="cursor:pointer" onclick="hapuskeranjang('.$value->id.')"><i class="fa fa-close"></i></span>
				<h3 class="namaproduk" style="padding-top:5px">'.$value->nama.'</h3>
				<span>Sisa Stok : <b id="sisastok'.$value->id.'">'.$value->stok.'</b></span>
				<div class="form-inline">
								<span style="vertical-align: middle;">Jumlah : </span>
								<div class="input-group" style="width: 150px;margin:0;">
									<input type="hidden" class="form-control" id="id_komoditas'.$value->id.'" name="id_komoditas[]" value="'.$value->id.'" />
									<input type="number" class="form-control" id="jumlah'.$value->id.'" name="jumlah['.$value->id.']" placeholder="Jumlah" min="1" value="'.$value->jumlah.'" onkeyup="hitungsemua()">
									<span class="input-group-addon">'.$value->satuan.'</span>
								</div><span style="vertical-align: middle;"> @ Rp. '.$value->harga.' </span><h3 style="" class="pull-right" id="total'.$value->id.'"> Rp. '.($value->harga*$value->jumlah).' </h3>
							</div>
			</div></div>';
		}
			?>
			<div style="margin-bottom:15px">
				<h3>Catatan</h3>
				<textarea class="form-control" name="catatan" rows="5" placeholder="Tulis Catatan Anda untuk Penjual."></textarea>
			</div>
		</div>
		<div class="col-sm-5 col-md-4">
				<div id="alamatpengiriman"><h3>Alamat Pengiriman</h3><div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
					<?php
					$id_alm = 0; $jdl = ''; $alm='';
					foreach ($alamat as $key => $value) {
						if ($value->utama==1) {
							$id_alm=$value->id;
							$jdl=$value->judul;
							$alm=$value->alamat;
							break;
						}
					}
					echo '<input type="hidden" class="form-control" name="alamat" id="alamat" value="'.$jdl.' '.$alm.'" />
					<h4 id="jdl">'.$jdl.'</h4>
					<p id="alm">'.$alm.'</p>';
					 ?>
							<h5 style="cursor:pointer" onclick="gantialamat()">Ganti Alamat</h5>
				</div>
			</div>
			<div id="metodepembayaran">
				<h3>Metode Pembayaran</h3>
				<div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
					<input type="hidden" class="form-control" name="metode_pembayaran" id="metode_pembayaran" value="transferbank" />
					<h4>Transfer Bank</h4>
				</div>
			</div>
			<h3>Rincian Biaya</h3><input type="hidden" name="diskon" class="form-control" value="0" />
			<div class="row" style="padding-bottom:20px;margin-top: 10px;">
				<div class="col-xs-6"><h4><b>SUBTOTAL </b></h4></div>
				<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="subtotal">Rp. 0 </h3></div>
				<div class="col-xs-12"><h5 class="pull-right">(Harga belum termasuk Ongkir)<h5></div>
				<?php /*<div class="col-xs-6"><h4><b> ONGKIR </b></h4></div>
				<div class="col-xs-6"><input type="hidden" name="ongkir" class="form-control" value="0" /><h3 style="font-weight:bold" class="pull-right">Rp. 0 </h3></td></div>
				<div class="col-xs-6"><h4><b> TOTAL </b></h4></div>
				<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="totalsemua">Rp. 0 </h3></div> */ ?>
			</div>
			<button type="button" class="btn btn-primary pull-right" onclick="pesankomoditas()">Pesan</button>
		</div>
	<?php } else {
		echo '<h3>Keranjang Belanja Kosong</h3><a href="'.url('/transaksi/komoditas').'">Belanja Sekarang</a>';
	} ?>
		<div style="clear:both"></div>
  </div>
</div>

<!-- Modal -->
<div class="modal modal-default fade" id="popupgantialamat">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Ganti Alamat</h4>
			</div>
			<div class="modal-body" style="padding: 20px 10px">
					<table id="listalamat" class="table table-bordered table-condensed table-striped table-hover">
						<thead><tr><th>Judul</th><th>Alamat</th></tr></thead>
						<tbody>
						<?php foreach ($alamat as $key => $value) {
							echo '<tr style="cursor:pointer" onclick="pilihalamat('.$key.')"><td>'.$value->judul.'</td><td>'.$value->alamat.'</td></tr>';
						} ?>
						</tbody>
					</table>
					<div style="clear:both"></div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
var keranjang = <?=json_encode($keranjang)?>;
var alamat = <?=json_encode($alamat)?>;
function gantialamat(){
	$('#popupgantialamat').modal();
}
function pilihalamat(id_alamat) {
	$('#alamat').val(alamat[id_alamat]['judul']+' '+alamat[id_alamat]['alamat']);
	$('#jdl').html(alamat[id_alamat]['judul']);
	$('#alm').html(alamat[id_alamat]['alamat']);
	$('#popupgantialamat').modal('hide');
}
function hitungsemua() {
	var subtotal=0;
	$.each(keranjang,function(index,value){
		if ($('#jumlah'+value['id']).val()>(value['jumlah']+value['stok'])){
			alert('Jumlah Melebihi Stok');
			$('#jumlah'+value['id']).val(value['jumlah']+value['stok']);
		}
		var totalitem = $('#jumlah'+value['id']).val()*value['harga'];
		$('#total'+value['id']).html(' Rp. '+totalitem);
		$('#sisastok'+value['id']).html(value['stok']-($('#jumlah'+value['id']).val()-value['jumlah']));
		subtotal+=totalitem;
	});
	$('#subtotal').html('Rp. '+subtotal);
	$('#totalsemua').html('Rp. '+subtotal);
}
function hapuskeranjang(id_keranjang) {
	if (confirm('Apakah Kamu Yakin Ingin Menghapus Komoditas Ini ?')) {
		$.ajax({
	      url: "{{ url('/api/hapuskeranjang') }}",
	      type: 'POST',
	      data: {
					'id':id_keranjang,
	        'id_user':{{ auth()->user()->id }},
	      },
	      dataType: 'JSON',
	      success: function (data) {
	        alert('Komoditas Berhasil Dihapus dari Keranjang');
					location.reload();
	      },
	      error: function(XMLHttpRequest, textStatus, errorThrown) {
	        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
	      }
	  });
	}
}

function pesankomoditas() {
	$.ajax({
      url: "{{ url('/api/buattransaksi') }}",
      type: 'POST',
      data: $('.form-control').serialize(),
      dataType: 'JSON',
      success: function (data) {
				if (data=='gagal') {
					alert('Terjadi Kesalahan Silahkan Coba Lagi');
					location.reload();
				} else {
					alert('Pesanan Berhasil Di Proses, Terimakasih');
					location.href="{{ url('/transaksi/detail') }}/"+data;
				}
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
hitungsemua();
});
</script>
@endsection
