@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i>Transaksi</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Transaksi</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div class="container">
    <h3 class="agileits_w3layouts_head">Transaki <span>Member</span></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>
    <div class="row">

    <div class="col-md-6 w3_agile_services_grid">
				<div class="col-xs-4 w3_agile_services_grid_left">
					<div class="agile_services_grid_left1 hvr-radial-out">
						<img src="{{ url('/images/komoditas/default.jpg') }}" alt=" " class="img-responsive">
					</div>
				</div>
				<div class="col-xs-8 w3_agile_services_grid_right">
					<h3>Komoditas</h3>
					<p>List Komoditas Terbaik Untuk Anda</p>
					<a href="{{ url('/transaksi/komoditas') }}" class="btn btn-primary">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
				</div>
				<div class="clearfix"> </div>
			</div>

      <div class="col-md-6 w3_agile_services_grid">
					<div class="col-xs-4 w3_agile_services_grid_left">
						<div class="agile_services_grid_left1 hvr-radial-out">
							<img src="{{ url('/images/transaksi/listtransaksi.jpg') }}" alt=" " class="img-responsive">
						</div>
					</div>
					<div class="col-xs-8 w3_agile_services_grid_right">
						<h3>List Transaksi</h3>
						<p>List Transaksi Anda</p>
						<a href="{{ url('/transaksi/list') }}" class="btn btn-primary">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
					</div>
					<div class="clearfix"> </div>
				</div>

    </div>
  </div>
</div>
@endsection
