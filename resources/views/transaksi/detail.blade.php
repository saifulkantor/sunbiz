@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/transaksi') }}">Transaksi</a><span>/</span></li>
				<li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/transaksi/list') }}">List</a><span>/</span></li>
				<li><i class="fa fa-money" aria-hidden="true"></i>Detail</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Detail Transaksi</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div class="container">
    <h3 class="agileits_w3layouts_head">Detail <span>Transaksi</span></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>
		<?=$transaksimodel->getlabelstatus($transaksi->status)?>
		<h3>#<?=$transaksi->kode?></h3>
		<p><?=date('d F Y h:m',strtotime($transaksi->tanggal))?></p>
		<h3>Detail Transaksi</h3>
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<?php $total = 0;
				foreach ($transaksidetail as $key => $value) {
					if(file_exists(public_path('images/komoditas/'.$value->id_komoditas.'.jpg'))){
							$image = url('/images/komoditas/'.$value->id_komoditas.'.jpg');
					}else{
							$image = url('/images/komoditas/default.jpg');
					}
			echo '<div class="row" style="margin-bottom:15px"><div class="col-xs-3 col-sm-2"><img src="'.$image.'" alt=" " class="img-responsive" style="padding: 5px 0px;"></div><div class="col-xs-9 col-sm-10">
					<h3 class="namaproduk" style="padding-top:5px">'.$value->nama.'</h3>
					<p class="pull-left">Jumlah : '.$value->jumlah.' '.$value->satuan.' @ Rp. '.$value->harga.'</p><h3 style="" class="pull-right" id="total'.$value->id.'"> Rp. '.($value->harga*$value->jumlah).' </h3>
				</div></div>';
				$total+=($value->harga*$value->jumlah);
				} ?>
				<div style="margin-bottom:15px">
					<h3>Catatan</h3>
					<p><?=($transaksi->catatan!='')?$transaksi->catatan:''?></p>
				</div>
			</div>
			<div class="col-sm-5 col-md-4">
				<div id="alamatpengiriman"><h3>Alamat Pengiriman</h3><div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
					<?php
					echo '<p id="alm">'.$transaksi->alamat.'</p>';
					 ?>
					</div>
				</div>
				<div id="metodepembayaran">
					<h3>Metode Pembayaran</h3>
					<div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
						<input type="hidden" class="form-control" name="metode_pembayaran" id="metode_pembayaran" value="transferbank">
						<h4><?=ucfirst($transaksi->metode_pembayaran)?></h4>
					</div>
				</div>
				<h3>Rincian Biaya</h3><input type="hidden" name="diskon" class="form-control" value="0" />
				<div class="row" style="padding-bottom:20px;margin-top: 10px;">
					<div class="col-xs-6"><h4><b>SUBTOTAL </b></h4></div>
					<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="subtotal">Rp. <?=$total?> </h3></div>
					<div class="col-xs-6"><h4><b> ONGKIR </b></h4></div>
					<div class="col-xs-6"><input type="hidden" name="ongkir" class="form-control" value="0" /><h3 style="font-weight:bold" class="pull-right">Rp. <?=$transaksi->ongkir?> </h3></td></div>
					<div class="col-xs-6"><h4><b> TOTAL </b></h4></div>
					<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="totalsemua">Rp. <?=$transaksi->total?> </h3></div>
				</div>
			</div>
		</div>

  </div>
</div>
<script type="text/javascript">
function lihattransaksi(id_transaksi) {
	location.href="{{ url('/transaksi/detail') }}/"+id_transaksi;
}
</script>
@endsection
