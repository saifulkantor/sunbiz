@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/transaksi') }}">Transaksi</a><span>/</span></li>
				<li><i class="fa fa-money" aria-hidden="true"></i>List</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>List Transaksi</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div id="listtransaksi" class="container">
    <h3 class="agileits_w3layouts_head">List <span>Transaksi</span></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link pendingadmin" href="#listtransaksi" role="tab" aria-controls="pills-home" aria-selected="true" onclick="gantistatus('pendingadmin')">Pending Admin</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link pendingpayment" href="#listtransaksi" role="tab" aria-controls="pills-profile" aria-selected="false" onclick="gantistatus('pendingpayment')">Belum Dibayar</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link send" href="#listtransaksi" role="tab" aria-controls="pills-contact" aria-selected="false" onclick="gantistatus('send')">Dikirim</a>
		  </li>
			<li class="nav-item">
		    <a class="nav-link all" href="#listtransaksi" role="tab" aria-controls="pills-contact" aria-selected="false" onclick="gantistatus('all')">Semua Transaksi</a>
		  </li>
		</ul>
		<table id="listtransaksi" class="table table-bordered table-hover">
			<thead><tr><th>ID Transaksi</th><th>Tanggal</th><th>Total</th></tr></thead>
			<tbody>
			<?php foreach ($transaksi as $key => $value) {
				echo '<tr style="cursor:pointer" onclick="lihattransaksi(\''.$value->kode.'\')"><td>#'.$value->kode.'</td><td>'.date('d F Y h:m',strtotime($value->tanggal)).'</td><td>Rp. '.$value->total.'</td></tr>';
			} ?>
			</tbody>
		</table>
		<div id="pagingtransaksi" class="row pull-right">

		</div>
  </div>
</div>
<script type="text/javascript">
var page=1; var perpage=20; var status='pendingpayment';
function lihattransaksi(id_transaksi) {
	location.href="{{ url('/transaksi/detail') }}/"+id_transaksi;
}
function gantistatus(stat) {
	status = stat;
	$('.nav-link').removeClass('active');
	$('.nav-link.'+stat).addClass('active');
	getlisttransaksi();
}
function resetdatatransaksi() {
  $('#listtransaksi tbody').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#pagingtransaksi').html('');
}
function getlisttransaksi() {
	resetdatatransaksi();
	ajaxgetlisttransaksi();
}

function ajaxgetlisttransaksi() {
  $.ajax({
      url: "{{ url('/api/listtransaksi') }}",
      type: 'POST',
      data: {
				'id_user':{{ auth()->user()->id }},
        'status':status,
        'page':page,
        'perpage':perpage,
      },
      dataType: 'JSON',
      success: function (data) {
				console.log(data);
        var html = '';
        $.each(data,function(index,value) {
          html+='<tr style="cursor:pointer" onclick="lihattransaksi(\''+value['kode']+'\')"><td>#'+value['kode']+'</td><td>'+value['tanggal']+'</td><td>Rp. '+value['total']+'</td></tr>';
        });
				html =(html=='')?'<tr><td colspan="10" style="text-align: center;"> Data Tidak Ditemukan </td></tr>':html;
        $('#listtransaksi tbody').html(html);
        var htmlpaging='';
        if (page>1) htmlpaging+='<button class="btn" onclick="page-=1;getlisttransaksi()">&lt;&lt; Prev</button> &nbsp; &nbsp; ';
        if (data.length>=perpage) htmlpaging+='<button class="btn" onclick="page+=1;getlisttransaksi()"> Next &gt;&gt;</button>';
        $('#pagingtransaksi').html(htmlpaging);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  gantistatus(status);
});
</script>
@endsection
