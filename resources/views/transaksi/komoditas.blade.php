@extends('layouts.home')

@section('content')
<!-- news -->
<?php $id_komoditas_utama = isset($_GET['kom'])?(int)htmlspecialchars($_GET['kom']):'';
$pilihkomoditas=''; ?>
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/transaksi') }}">Transaksi</a><span>/</span></li>
				<li><i class="fa fa-database" aria-hidden="true"></i>Komoditas</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Komoditas</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">List <span>Komoditas</span></h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
			</div>
			<p class="agile_para"></p>
			<div id="filterdatakomoditas" class="row">
				<div class="form-group col-sm-3">
					<label for="id_komoditas_utama">Grup Komoditas</label>
					<select id="id_komoditas_utama" name="id_komoditas_utama" class="form-control" onchange="filterkomoditas()">
						<?php foreach ($komoditasparent as $key => $komoditas) {
								echo '<option value="'.$komoditas->id.'">'.$komoditas->nama.'</option>';
								$pilihkomoditas=($id_komoditas_utama==$komoditas->id)?' $(\'#id_komoditas_utama\').val('.$komoditas->id.').trigger(\'change\');':$pilihkomoditas;
						} ?>
					</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="urutkan">Urutkan</label>
					<select id="urutkan" name="urutkan" class="form-control" onchange="filterkomoditas();">
						<option value="">-- Default --</option>
						<option value="harga asc">Harga Termurah</option>
						<option value="harga desc">Harga Termahal</option>
						<option value="nama asc">Nama Komoditas A-Z</option>
						<option value="nama desc">Nama Komoditas Z-A</option>
					</select>
				</div>
        <div class="form-group col-sm-6">
					<label for="urutkan">Cari Komoditas</label>
					<div class="input-group" style="margin:0">
            <input class="form-control" placeholder="Cari Komoditas Di Sini ..." id="filtercari" />
            <span class="input-group-addon" style="cursor:pointer" onclick="filterkomoditas()"><i class="fa fa-search"></i></span>
          </div>
				</div>
			</div>
			<div id="listkomoditas" class="row">

			</div>
      <div id="pagingkomoditas" class="row pull-right">

			</div>
		</div>
	</div>

  <!-- Modal -->
  <div class="modal modal-default fade" id="popupaddtochart">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Tambah ke Keranjang</h4>
          </div>
          <div class="modal-body" style="padding: 20px 10px">
              <div class="col-xs-12 col-sm-6">
                <img class="image" src="" style="width:100%;max-height: 300px;cursor:pointer;" />
              </div>
              <div class="col-xs-12 col-sm-6">
                <input type="hidden" class="form-control id_komoditas" name="id_komoditas">
                <div class="form-group">
                  <h3 class="nama namaproduk">Harga</h3>
                  <label class="harga hargaproduk">Harga</label>
                </div>
                <div class="form-group">
                  <label class="stok">Stok</label>
                </div>
                <div class="form-inline">
                  <input type="number" value="1" min="1" class="form-control jumlah" style="width: 80px;margin-right: 20px;">
                  <button type="button" class="btn btn-outline" onclick="pesankomoditas()">Tambah</button>
                </div>
              </div>
              <div style="clear:both"></div>
          </div>
          <div class="modal-body-loading" style="padding: 20px 10px">
            <img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
<script type="text/javascript">
var page=1; var perpage=12;
function filterkomoditas() {
  page=1;
	getlistkomoditas();
}
function popupaddtochart(id,stok) {
  $('#popupaddtochart .id_komoditas').val(id);
  $('#popupaddtochart .nama').html($('#komnama'+id).html());
  $('#popupaddtochart .harga').html($('#komharga'+id).html());
  $('#popupaddtochart .stok').html('Stok : '+stok);
  $('#popupaddtochart .image').attr('src',$('#komimage'+id).attr('src'));
  $('#popupaddtochart .modal-body').show();
  $('#popupaddtochart .modal-body-loading').hide();
  $('#popupaddtochart').modal();
}
function resetdatakomoditas() {
  $('#listkomoditas').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#pagingkomoditas').html('');
}
function getlistkomoditas() {
  $([document.documentElement, document.body]).animate({
        scrollTop: $(".welcome .container").offset().top
    }, 1000);
	resetdatakomoditas();
  setTimeout(ajaxgetlistkomoditas,1000);
}
function ajaxgetlistkomoditas() {
  $.ajax({
      url: "{{ url('/api/listkomoditas') }}",
      type: 'POST',
      data: {
				'urutkan':$('#urutkan').val(),
				'tanggal':'{{ date("Y-m-d") }}',
				'id_parent':$('#id_komoditas_utama').val(),
        'nama':$('#filtercari').val(),
        'page':page,
        'perpage':perpage,
      },
      dataType: 'JSON',
      success: function (data) {
				console.log(data);
        var html = '';
        $.each(data,function(index,value) {
          var linkimage = '{{url("images/komoditas") }}/'+value["id"]+'.jpg';
          if (!imageExists(linkimage)) linkimage='{{url("images/komoditas") }}/default.jpg';
          html+='<div class="col-xs-6 col-sm-4 col-md-3"><div onclick="popupaddtochart('+value["id"]+',\''+value["stok"]+' '+value["satuan"]+'\');" style="margin-bottom: 15px;border: 1px solid #e0e0e0;cursor:pointer">'+
  								'<img id="komimage'+value["id"]+'" src="'+linkimage+'" alt=" " class="img-responsive" style="width:100%">'+
  								'<p id="komnama'+value["id"]+'" class="namaproduk">'+value["nama"]+'</p>'+
                  '<p id="komharga'+value["id"]+'" class="hargaproduk">Rp. '+value["harga"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'/'+value["satuan"]+'</p>'+
  							'</div></div>';
        });
				html=(html=='')?'<p style="text-align: center;padding-top: 30px;">Komoditas Tidak Ditemukan</p>':html;
        $('#listkomoditas').html(html);
        var htmlpaging='';
        if (page>1) htmlpaging+='<button class="btn" onclick="page-=1;getlistkomoditas()">&lt;&lt; Prev</button> &nbsp; &nbsp; ';
        if (data.length>=perpage) htmlpaging+='<button class="btn" onclick="page+=1;getlistkomoditas()"> Next &gt;&gt;</button>';
        $('#pagingkomoditas').html(htmlpaging);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}

function pesankomoditas() {
  $('#popupaddtochart .modal-body-loading').show();
  $('#popupaddtochart .modal-body').hide();
  $.ajax({
      url: "{{ url('/api/tambahkeranjang') }}",
      type: 'POST',
      data: {
				'id':$('#popupaddtochart .id_komoditas').val(),
        'jumlah':$('#popupaddtochart .jumlah').val(),
        'id_user':{{ auth()->user()->id }},
      },
      dataType: 'JSON',
      success: function (data) {
        data = parseInt(data);
        if (Number.isInteger(data)){
          alert('Komoditas Berhasil Ditambahkan ke Keranjang');
          $('#jmlkeranjang').html(data);
          getlistkomoditas();
        } else {
          alert(data);
        }
        $('#popupaddtochart .modal-body').show();
        $('#popupaddtochart .modal-body-loading').hide();
        $('#popupaddtochart').modal('hide');
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
        $('#popupaddtochart .modal-body').show();
        $('#popupaddtochart .modal-body-loading').hide();
        $('#popupaddtochart').modal('hide');
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  resetdatakomoditas();
  getlistkomoditas();
  $('#filtercari').keyup(function(e){ if(e.keyCode == 13) { filterkomoditas();} });
	<?=$pilihkomoditas?>
});
</script>
@endsection
