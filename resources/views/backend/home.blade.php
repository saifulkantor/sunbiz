@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?= count($kotaku) ?></h3>

          <p>Kota</p>
        </div>
        <div class="icon">
          <i class="fa fa-database"></i>
        </div>
        <a href="{{ url('/backend/komoditas') }}" class="small-box-footer">Lihat Semua <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
</div>
@endsection
