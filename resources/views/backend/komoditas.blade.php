@extends('layouts.app')

@section('content')
<?php $id_kota=isset($_GET['id_kota'])?$_GET['id_kota']:0; $pilihkota=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkotabox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Kota</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="form-group row">
                    <label for="tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                    <div class="col-sm-8">
                      <input type="date" class="tanggal form-control datepicker" name="tanggal" value="{{ date('Y-m-d') }}" required>
                    </div>
                  </div>
                  <table id="tabelkota" class="table">
                    <thead>
                      <tr><th>No.</th><th>Nama Kota / Kabupaten</th><th>Provinsi</th></tr>
                    </thead>
                  <?php foreach ($kotaku as $key=>$kt) {
                    $pilihkota=($id_kota==$kt->id)?'pilihkota('.$kt->id.',\''.$kt->nama.'\');':$pilihkota;
                    echo '<tr><td>'.($key+1).'</td><td><p class="btn btn-link" onclick="pilihkota('.$kt->id.',\''.$kt->nama.'\')">'.$kt->jenis.' '.$kt->nama.'</p></td><td>'.$kt->nama_provinsi.'</td></tr>';
                  } ?>
                  </table>
                </div>

              </div>

              <!-- List Komoditas -->
              <div id="tabelkomoditasbox" class="box box-default" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">List Komoditas</h3>

                  <div class="box-tools pull-right" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="bukakota()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group row">
                      <label for="tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                      <div class="col-sm-8">
                        <input type="date" class="tanggal form-control datepicker" name="tanggal" value="{{ date('Y-m-d') }}" required>
                      </div>
                    </div>
                    <table id="tabelkomoditas" class="table table-condensed table-striped">
                      <input type="hidden" class="id_kota" name="id_kota" value="0" />
                      <input type="hidden" class="id_user" name="id_user" value="{{ Auth::user()->id }}" />
                      <thead>
                        <tr><th>Nama Komoditas</th><th colspan="2">Harga</th></tr>
                      </thead>
                    <tbody>
                    <?php $kom = $komoditas;
                    foreach ($komoditas as $k) {
                        $anak = array_filter($kom, function ($e) use ($k) { return $e->id_parent == $k->id; } );
                        $input = ($anak==null)?'Rp.</td><td><input id="hargakomoditas'.$k->id.'" class="datatambah form-control" type="number" name="hargakomoditas['.$k->id.']" value="0" style="max-width: 120px;" min="0" />':'</td><td>';
                        $no = ($k->id_parent==null)?'':'&nbsp; &nbsp;';
                        $no .= ($anak!=null)?$k->urutan.'. ':'&nbsp; &nbsp; ';
                        $nama = ($anak!=null)?'<b onclick="togglekomoditas(\'kom'.$k->id.'\')" style="cursor:pointer"> '.$no.$k->nama.' <i class="fa fa-caret-down"></i></b>':$no.$k->nama;
                        echo '<tr class="kom'.$k->id_parent.'"><td>'.$nama.'</td><td>'.$input.'</td></tr>';
                      } ?>
                    </tbody>
                    </table>

                  </div>

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" onclick="simpandata()"><i class="fa fa-save"></i> Simpan </button>
                  </div>
                </div>



            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => { <?=$pilihkota?> });
function togglekomoditas(kelas){
  $('.'+kelas).toggle();
}
function bukakota() {
  $('#tabelkomoditasbox').hide();
  $('#tabelkotabox').show();
}
function clearform() {
  $('#tabelkomoditas input.form-control').val(0);
}
function pilihkota(id_kota,nama_kota) {
  clearform();
  $('#tabelkotabox').hide();
  $('#tabelkomoditasbox').show();
  $('#tabelkomoditasbox .id_kota').val(id_kota);
  $('#tabelkomoditasbox .tanggal').val($('#tabelkotabox .tanggal').val());
  $('#tabelkomoditasbox .box-title').html('List Komoditas '+nama_kota);
  getdata();
}
function simpandata() {
  $.ajax({
      url: "{{ url('/api/komoditas_data') }}",
      type: 'POST',
      data: $('#tabelkomoditasbox input').serialize(),
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkomoditas_data') }}",
      type: 'POST',
      data: {
        'tanggal':$('#tabelkomoditasbox .tanggal').val(),
        'id_kota':$('#tabelkomoditasbox .id_kota').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#hargakomoditas'+value['id_komoditas']).val(value['harga']); $('#stokkomoditas'+value['id_komoditas']).val(value['stok']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
