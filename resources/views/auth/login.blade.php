@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
				<li><i class="fa fa-lock" aria-hidden="true"></i>Login</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Login</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>

<div class="welcome">
  <div class="container">
      <div class="col-sm-6">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
										</div>
										<?php /*
                        @if (Route::has('password.request'))
												<div class="col-md-8">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
												</div>
                        @endif
											*/	?>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
