@extends('layouts.home')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
        <li><i class="fa fa-money" aria-hidden="true"></i><a href="{{ url('/artikel') }}">Artikel</a><span>/</span></li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2></h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div class="container">
    <h3 class="agileits_w3layouts_head"><?=$artikel->judul?></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>
		<p><?=date('d F Y h:m',strtotime($artikel->updated_at))?></p>
		<div id="detailartikel"></div>
  </div>
</div>
<script type="text/javascript">
var komoditas = <?=json_encode($komoditas)?>;
window.addEventListener('DOMContentLoaded', (event) => {
	dataajaxartikel(<?=$artikel->id?>);
});
function dataajaxartikel(id_artikel) {
  $('#detailartikel').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $.ajax({
      url: "{{ url('/api/getartikel') }}",
      type: 'POST',
      data: {
        'id' : id_artikel,
      },
      dataType: 'JSON',
      success: function (data) {
				$.each(komoditas,function( index, value ) {
			    // var datalama=new RegExp('&#123;&#123;produk-'+value['id']+'&#125;&#125;','g');
					var datalama=new RegExp('\{\{produk-'+value['id']+'\}\}','g');
					if (data.match(datalama)) {
						data=data.replace(datalama,tampilanlistkomoditas(value));
					}
				});
        $('#detailartikel').html(data);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function tampilanlistkomoditas(value) {
	var linkimage = '{{url("images/komoditas") }}/'+value["id"]+'.jpg';
	if (!imageExists(linkimage)) linkimage='{{url("images/komoditas") }}/default.jpg';
	return '<div class="col-xs-6 col-sm-4 col-md-3"><div style="margin-bottom: 15px;border: 1px solid #e0e0e0;cursor:pointer">'+
					'<img id="komimage'+value["id"]+'" src="'+linkimage+'" alt=" " class="img-responsive" style="width:100%">'+
					'<p id="komnama'+value["id"]+'" class="namaproduk">'+value["nama"]+'</p>'+
					'<p id="komharga'+value["id"]+'" class="hargaproduk">Rp. '+value["harga"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'/'+value["satuan"]+'</p>'+
				'</div></div>';
}
</script>
@endsection
