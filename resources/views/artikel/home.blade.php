@extends('layouts.home')

@section('content')
<!-- news -->
<?php $id_kategori = isset($_GET['kom'])?(int)htmlspecialchars($_GET['kom']):'';
$pilihartikel=''; ?>
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
				<li><i class="fa fa-database" aria-hidden="true"></i>Artikel</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Artikel</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">List <span>Artikel</span></h3>
			<div class="w3_agile_image">
				<img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
			</div>
			<p class="agile_para"></p>
			<div id="filterdataartikel" class="row">
				<div class="form-group col-sm-3">
					<label for="id_kategori">Kategori Artikel</label>
					<select id="id_kategori" name="id_kategori" class="form-control" onchange="filterartikel()">
						<?php foreach ($artikelkategori as $key => $artikel) {
								echo '<option value="'.$artikel->id.'">'.$artikel->nama.'</option>';
								$pilihartikel=($id_kategori==$artikel->id)?' $(\'#id_kategori\').val('.$artikel->id.').trigger(\'change\');':$pilihartikel;
						} ?>
					</select>
				</div>
        <div class="form-group col-sm-3">
					<label for="urutkan">Cari Artikel</label>
					<div class="input-group" style="margin:0">
            <input class="form-control" placeholder="Cari Artikel Di Sini ..." id="filtercari" />
            <span class="input-group-addon" style="cursor:pointer" onclick="filterartikel()"><i class="fa fa-search"></i></span>
          </div>
				</div>
			</div>
			<div id="listartikel" class="row">

			</div>
      <div id="pagingartikel" class="row pull-right">

			</div>
		</div>
	</div>

  <!-- Modal -->
  <div class="modal modal-default fade" id="popupaddtochart">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Tambah ke Keranjang</h4>
          </div>
          <div class="modal-body" style="padding: 20px 10px">
              <div class="col-xs-12 col-sm-6">
                <img class="image" src="" style="width:100%;max-height: 300px;cursor:pointer;" />
              </div>
              <div class="col-xs-12 col-sm-6">
                <input type="hidden" class="form-control id_artikel" name="id_artikel">
                <div class="form-group">
                  <h3 class="nama namaproduk">Harga</h3>
                  <label class="harga hargaproduk">Harga</label>
                </div>
                <div class="form-group">
                  <label class="stok">Stok</label>
                </div>
                <div class="form-inline">
                  <input type="number" value="1" min="1" class="form-control jumlah" style="width: 80px;margin-right: 20px;">
                  <button type="button" class="btn btn-outline" onclick="pesanartikel()">Tambah</button>
                </div>
              </div>
              <div style="clear:both"></div>
          </div>
          <div class="modal-body-loading" style="padding: 20px 10px">
            <img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
<script type="text/javascript">
var page=1; var perpage=12;
function filterartikel() {
  page=1;
	getlistartikel();
}
function popupaddtochart(id,stok) {
  $('#popupaddtochart .id_artikel').val(id);
  $('#popupaddtochart .nama').html($('#komnama'+id).html());
  $('#popupaddtochart .harga').html($('#komharga'+id).html());
  $('#popupaddtochart .stok').html('Stok : '+stok);
  $('#popupaddtochart .image').attr('src',$('#komimage'+id).attr('src'));
  $('#popupaddtochart .modal-body').show();
  $('#popupaddtochart .modal-body-loading').hide();
  $('#popupaddtochart').modal();
}
function resetdataartikel() {
  $('#listartikel').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#pagingartikel').html('');
}
function getlistartikel() {
  $([document.documentElement, document.body]).animate({
        scrollTop: $(".welcome .container").offset().top
    }, 1000);
	resetdataartikel();
  setTimeout(ajaxgetlistartikel,1000);
}
function ajaxgetlistartikel() {
  $.ajax({
      url: "{{ url('/api/listartikel') }}",
      type: 'POST',
      data: {
				'id_kategori':$('#id_kategori').val(),
        'judul':$('#filtercari').val(),
        'page':page,
        'perpage':perpage,
      },
      dataType: 'JSON',
      success: function (data) {
				console.log(data);
        var html = '';
        $.each(data,function(index,value) {
          var linkimage = '{{url("images/artikel") }}/'+value["id"]+'.jpg';
          if (!imageExists(linkimage)) linkimage='{{url("images/artikel") }}/default.jpg';
					const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
					var tanggal = new Date(value["updated_at"]);
					tanggal = tanggal.getDate() + " " + months[tanggal.getMonth()] + " " + tanggal.getFullYear();
          html+='<div class="col-md-4 w3ls_news_grid">'+
					'<div class="w3layouts_news_grid">'+
						'<img src="'+linkimage+'" alt=" " class="img-responsive" />'+
							'<div class="w3layouts_news_grid_pos">'+
								'<div class="wthree_text"><h3>'+value["judul"]+'</h3></div>'+
							'</div>'+
						'</div>'+
						'<div class="agileits_w3layouts_news_grid">'+
							'<ul>'+
								'<li><i class="fa fa-calendar" aria-hidden="true"></i>'+tanggal+'</li>'+
								'<li><i class="fa fa-user" aria-hidden="true"></i><a href="#">'+value["kategori"]+'</a></li>'+
							'</ul>'+
							'<h4><a href="<?=url('/artikel')?>/'+value["slug"]+'" target="_blank">'+value["judul"]+'</a></h4>'+
							'<p>'+value["overview"]+'</p>'+
						'</div>'+
					'</div>';
        });
				html=(html=='')?'<p style="text-align: center;padding-top: 30px;">Artikel Tidak Ditemukan</p>':html;
        $('#listartikel').html(html);
        var htmlpaging='';
        if (page>1) htmlpaging+='<button class="btn" onclick="page-=1;getlistartikel()">&lt;&lt; Prev</button> &nbsp; &nbsp; ';
        if (data.length>=perpage) htmlpaging+='<button class="btn" onclick="page+=1;getlistartikel()"> Next &gt;&gt;</button>';
        $('#pagingartikel').html(htmlpaging);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}

window.addEventListener('DOMContentLoaded', (event) => {
  resetdataartikel();
  getlistartikel();
  $('#filtercari').keyup(function(e){ if(e.keyCode == 13) { filterartikel();} });
	<?=$pilihartikel?>
});
</script>
@endsection
