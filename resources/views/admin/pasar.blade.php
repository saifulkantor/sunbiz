@extends('layouts.app')

@section('content')
<?php $id_pasar=isset($_GET['id_pasar'])?$_GET['id_pasar']:0; $pilihpasar=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelpasarbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Pasar</h3>
                </div>
                <div class="box-body">
                  <button type="button" class="btn btn-primary" onclick="tambah()"><i class="fa fa-save"></i> Tambah </button>
                  <table id="tabelpasar" class="table">
                    <thead>
                      <tr><th>No.</th><th>Nama Pasar</th><th>Kota</th></tr>
                    </thead>
                  <?php foreach ($pasar as $key=>$psr) {
                    echo '<tr><td>'.($key+1).'</td><td><p class="btn btn-link" onclick="pilihpasar('.$psr->id.',\''.$psr->nama.'\')">'.$psr->nama.'</p></td><td>'.$psr->nama_kota.'</td></tr>';
                  } ?>
                  </table>
                </div>

              </div>

              <!-- List pasar -->
              <div id="isipasarbox" class="box box-default" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Pasar</h3>

                  <div class="box-tools pull-right" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="bukapasar()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-6">
                      <input type="hidden" class="form-control" id="id_pasar" name="id_pasar" />
                      <div class="form-group">
                        <label for="name">Nama Pasar</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Pasar" />
                      </div>
                      <div class="form-group">
                        <label for="id_provinsi">Provinsi</label>
                        <select id="id_provinsi" class="form-control" name="id_provinsi" onclick="loadkota()">
                          <?php foreach ($provinsi as $key => $value) {
                            echo '<option value="'.$value->id.'">'.$value->nama.'</option>';
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="id_kota">Kota</label>
                        <select id="id_kota" class="form-control" name="id_kota">

                        </select>
                      </div>

                    </div>
                  </div>

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" onclick="simpandata()"><i class="fa fa-save"></i> Simpan </button>
                  </div>
                </div>

            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
var kota = <?=json_encode($kota)?>;
window.addEventListener('DOMContentLoaded', (event) => { <?=$pilihpasar?> });
function loadkota() {
  var id_provinsi=$('#id_provinsi').val();
  var kotaoption='';
  $.each(kota,function(index, value){
    if (value['id_provinsi']==id_provinsi) {
      kotaoption+='<option value="'+value['id']+'">'+value['jenis']+' '+value['nama']+'</option>';
    }
  });
  $('#id_kota').html(kotaoption);
}
function bukapasar() {
  $('#isipasarbox').hide();
  $('#tabelpasarbox').show();
}
function clearform() {
  $('#isipasarbox input.form-control').val('');
}
function tambah() {
  clearform();
  $('#tabelpasarbox').hide();
  $('#isipasarbox').show();
  $('#isipasarbox .box-title').html('Data Pasar ');
  loadkota();
}
function pilihpasar(id_pasar,nama_pasar) {
  clearform();
  $('#tabelpasarbox').hide();
  $('#isipasarbox').show();
  $('#isipasarbox #id_pasar').val(id_pasar);
  $('#isipasarbox .box-title').html('Data Pasar '+nama_pasar);
  getdata();
}
function simpandata() {
  $.ajax({
      url: "{{ url('/api/pasar') }}",
      type: 'POST',
      data: $('#isipasarbox .form-control').serialize(),
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getpasar') }}",
      type: 'POST',
      data: {
        'id_pasar':$('#isipasarbox #id_pasar').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#nama').val(value['nama']); $('#id_provinsi').val(value['id_provinsi']); loadkota(); $('#id_kota').val(value['id_kota']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
