@extends('layouts.app')

@section('content')
<?php $id_contributor=isset($_GET['id_contributor'])?$_GET['id_contributor']:0; $pilihcontributor=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelcontributorbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Contributor</h3>
                </div>
                <div class="box-body">
                  <button type="button" class="btn btn-primary" onclick="tambah()"><i class="fa fa-save"></i> Tambah </button>
                  <table id="tabelcontributor" class="table">
                    <thead>
                      <tr><th>No.</th><th>Nama Contributor</th><th>Kota</th></tr>
                    </thead>
                  <?php foreach ($contributor as $key=>$con) {
                    echo '<tr><td>'.($key+1).'</td><td><p class="btn btn-link" onclick="pilihcontributor('.$con->id.',\''.$con->name.'\')">'.$con->name.'</p></td><td>'.$con->nama_kota.'</td></tr>';
                  } ?>
                  </table>
                </div>

              </div>

              <!-- List contributor -->
              <div id="isicontributorbox" class="box box-default" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Contributor</h3>

                  <div class="box-tools pull-right" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="bukacontributor()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-6">
                      <input type="hidden" class="form-control" id="id_contributor" name="id" />
                      <input type="hidden" class="form-control" id="level" name="level" value="contributor" />
                      <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Isi Nama Contributor" />
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Isi Email Contributor" />
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Isi Password Contributor" />
                      </div>
                      <div class="form-group">
                        <label for="ulangipassword">Ulangi Password</label>
                        <input type="password" class="form-control" id="ulangipassword" name="ulangipassword" placeholder="Ulangi Password" />
                      </div>
                      <table id="pilihkota" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Provinsi</th>
                            <th>Kota</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr><td><select id="id_provinsi1" class="form-control" name="id_provinsi[]" onclick="loadkota(1)">
                            <?php foreach ($provinsi as $key => $value) {
                              echo '<option value="'.$value->id.'">'.$value->nama.'</option>';
                            }
                            ?>
                          </select></td><td>
                            <select id="id_kota1" class="form-control" name="id_kota[]">
                            </select></td><td></td></tr>
                        </tbody>
                        <tfoot><tr><td><p style="color:blue;cursor:pointer" onclick="tambahkota()"> <i class="fa fa-plus"></i> Tambah Kota</p></td><td></td><td></td></tr></tfoot>
                    </table>

                    </div>
                  </div>

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" onclick="simpandata()"><i class="fa fa-save"></i> Simpan </button>
                  </div>
                </div>

            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
var kota = <?=json_encode($kota)?>;
window.addEventListener('DOMContentLoaded', (event) => { <?=$pilihcontributor?> });
var counter=1;
function tambahkota() {
  counter++;
  var clone = $('#id_provinsi1').clone().wrap("<select />").parent().prop('data-count', counter );
  var htmlclone = clone.html().replace('id="id_provinsi1"','id="id_provinsi'+counter+'"').replace('onclick="loadkota(1)"','onclick="loadkota('+counter+')"');
  $('#pilihkota tbody').append('<tr class="kotatambahan kotake'+counter+'"><td>'+htmlclone+'</td><td><select id="id_kota'+counter+'" class="form-control" name="id_kota[]"></select></td><td><i class="fa fa-trash btn" style="color:blue;cursor:pointer;" onclick="$(\'.kotake'+counter+'\').remove()"></i></td></tr>');
  loadkota(counter);
}
function loadkota(id_counter) {
  var id_provinsi=$('#id_provinsi'+id_counter).val();
  var kotaoption='';
  $.each(kota,function(index, value){
    if (value['id_provinsi']==id_provinsi) {
      kotaoption+='<option value="'+value['id']+'">'+value['jenis']+' '+value['nama']+'</option>';
    }
  });
  $('#id_kota'+id_counter).html(kotaoption);
}
function bukacontributor() {
  $('#isicontributorbox').hide();
  $('#tabelcontributorbox').show();
}
function clearform() {
  $('#tabelcontributor input.form-control').val('');
  $('.kotatambahan').remove();
  counter=1;
  loadkota(1);
}
function tambah() {
  clearform();
  $('#tabelcontributorbox').hide();
  $('#isicontributorbox').show();
  $('#isicontributorbox .box-title').html('Data Contributor ');
}
function pilihcontributor(id_contributor,nama_contributor) {
  clearform();
  $('#tabelcontributorbox').hide();
  $('#isicontributorbox').show();
  $('#isicontributorbox #id_contributor').val(id_contributor);
  $('#isicontributorbox .box-title').html('Data Contributor '+nama_contributor);
  getdata();
}
function simpandata() {
  if ($('#password').val()==$('#ulangipassword').val()) {
    $.ajax({
        url: "{{ url('/api/contributor') }}",
        type: 'POST',
        data: $('#isicontributorbox .form-control').serialize(),
        dataType: 'JSON',
        success: function (data) { location.reload(); },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
        }
    });
  } else {
    alert('Password dan Ulangi Password Tidak Sama');
  }
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getcontributor') }}",
      type: 'POST',
      data: {
        'id':$('#isicontributorbox #id_contributor').val()
      },
      dataType: 'JSON',
      success: function (datas) {
        clearform();
        console.log(datas);
        $.each(datas['data'], function( index, value ) { $('#name').val(value['name']); $('#email').val(value['email']); });
        $.each(datas['kota'], function( index, value ) { if(index>0) tambahkota(); $('#id_provinsi'+(index+1)).val(value['id_provinsi']); loadkota(index+1); $('#id_kota'+(index+1)).val(value['id']); });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
