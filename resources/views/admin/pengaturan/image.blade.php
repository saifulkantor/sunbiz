@extends('layouts.app')

@section('content')
<?php $id_komoditas=isset($_GET['id_komoditas'])?$_GET['id_komoditas']:0; $pilihkomoditas=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Pengaturan Image</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="location.href='{{ url('/admin/pengaturan') }}'"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="alert alert-success alert-dismissible">
                    Klik pada Image untuk mengubahnya.
                  </div>
                  <form method="post" action="" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="name">Homepage Banner (size : 1600px x 770px)</label>
                      <input type="file" class="inputimage" style="display:none" data-name="imgbb" name="banner-besar" id="inputimgbb" accept="image/*">
                      <img id="imgeditbanner" class="previewimageimgbb" src="{{ url('/images/homepage/banner-besar.jpg') }}" style="width:100%;cursor:pointer;" onclick="$('#inputimgbb').click()" />
                    </div>
                    <div class="form-group">
                      <label for="name">Image Petani (size : 800px x 535px)</label>
                      <input type="file" class="inputimage" style="display:none" data-name="img1" name="petani" id="inputimg1" accept="image/*">
                      <img id="imgeditimg1" class="previewimageimg1" src="{{ url('/images/homepage/petani.jpg') }}" style="width:100%;cursor:pointer;" onclick="$('#inputimg1').click()" />
                    </div>
                    <div class="form-group">
                      <label for="name">Image Paralax Newsletter (size : 1280px x 851px)</label>
                      <input type="file" class="inputimage" style="display:none" data-name="imgpn" name="paralax-newsletter" id="inputimgpn" accept="image/*">
                      <img id="imgeditparalaxn" class="previewimageimgpn" src="{{ url('/images/homepage/paralax-newsletter.jpg') }}" style="width:100%;cursor:pointer;" onclick="$('#inputimgpn').click()" />
                    </div>
                    <div class="form-group">
                      <label for="name">Image Paralax Footer (size : 1280px x 851px)</label>
                      <input type="file" class="inputimage" style="display:none" data-name="imgpf" name="paralax-footer" id="inputimgpf" accept="image/*">
                      <img id="imgeditparalaxf" class="previewimageimgpf" src="{{ url('/images/homepage/paralax-footer.jpg') }}" style="width:100%;cursor:pointer;" onclick="$('#inputimgpf').click()" />
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </form>
                </div>

              </div>


            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {

  $(".row_position").sortable
        ({
            scroll: false,
            sort: function (event, ui) {
                ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
            },
            helper: 'clone',
            axis: 'y',
            update: function () {
              var selectedData = new Array();
              $('.row_position>tr').each(function() {
                  selectedData.push($(this).attr("urutan"));
              });
              updateOrder(selectedData);
            }
        }).disableSelection();
  <?=$pilihkomoditas?>
});

function bukaeditkomoditas(id_komoditas,nama_komoditas,satuan) {
  $('#imgeditkomoditas').attr('src',$('#imgkom'+id_komoditas).attr('src'));
  $('#id').val(id_komoditas);
  $('#nama').val(nama_komoditas);
  $('#satuan').val(satuan);
  $('#editkomoditas').modal();
}
function updateOrder(data) {
    console.log(data);
}
function togglekomoditas(kelas){
  $('.'+kelas).toggle();
}
function bukakomoditas() {
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
}
function clearform() {
  $('#tabelkomoditas input.form-control').val(0);
}
function pilihkomoditas(id_komoditas,nama_komoditas) {
  clearform();
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
  $('#tabelkomoditasbox .id_komoditas').val(id_komoditas);
  $('#tabelkomoditasbox .tanggal').val($('#tabelkomoditasbox .tanggal').val());
  $('#tabelkomoditasbox .box-title').html('List Komoditas '+nama_komoditas);
  getdata();
}
function simpandata() {
  var formData = new FormData(document.getElementById('formdokumenkomoditas'));
  $.ajax({
      url: "{{ url('/api/komoditas') }}",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkomoditas_data') }}",
      type: 'POST',
      data: {
        'tanggal':$('#tabelkomoditasbox .tanggal').val(),
        'id_komoditas':$('#tabelkomoditasbox .id_komoditas').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#hargakomoditas'+value['id_komoditas']).val(value['harga']); $('#stokkomoditas'+value['id_komoditas']).val(value['stok']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
