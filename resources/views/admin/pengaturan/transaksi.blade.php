@extends('layouts.app')

@section('content')
<?php
$kodetransaksi = $pengaturan[7]->data;
?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Pengaturan Transaksi</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="location.href='{{ url('/admin/pengaturan') }}'"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="">
                    @csrf
                    <div class="form-group col-md-6">
                      <label for="kodetransaksi">Kode Transaksi</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-code"></i>
                        </div>
                        <input id="kodetransaksi" name="kodetransaksi" class="form-control" value="<?=$kodetransaksi?>" onkeyup="prevkodetransaksi()" />
                      </div>
                      <p class="help-block"><span>Kode : </span>
                        &nbsp; <span style="color:blue;cursor:pointer" onclick="tambahkode('tahun')">[[tahun]]</span>
                        &nbsp; <span style="color:blue;cursor:pointer" onclick="tambahkode('bulan')">[[bulan]]</span>
                        &nbsp; <span style="color:blue;cursor:pointer" onclick="tambahkode('tanggal')">[[tanggal]]</span>
                        &nbsp; <span style="color:blue;cursor:pointer" onclick="tambahkode('id_user')">[[id_user]]</span>
                        &nbsp; <span style="color:blue;cursor:pointer" onclick="tambahkode('id_transaksi')">[[id_transaksi]]</span>
                      </p>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="prevkodetransaksi">Preview</label>
                        <h1 id="prevkodetransaksi" style="margin:0px"></h1>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="kurir">Kurir Pengiriman</label>
                      <input type="hidden" class="form-control" id="hapuskurir" name="hapuskurir" value="" />
                      <table id="listkurir" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Nama Kurir</th>
                            <th>Kapasitas</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <p style="color:blue;cursor:pointer" onclick="tambahkurir()"> <i class="fa fa-plus"></i> Tambah Kurir</p>
                    </div>
                    <div class="form-group col-md-6">

                    </div>
                    <div style="clear:both"></div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </form>
                </div>
              </div>


            </div>
        </div>
    </div>

    <div class="modal modal-success fade" id="popupkurir">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Edit Kurir</h4>
            </div>
            <div class="modal-body">
                <div id="isikurir" class="table-striped">
                  <input type="hidden" class="form-control" id="id" name="id" value="">
                  <input type="hidden" class="form-control" id="utama" name="utama" value="0">
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Nama Kurir</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Kurir" />
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Kapasitas</label>
                    <input type="number" class="form-control" id="kapasitas" name="kapasitas" placeholder="0 box" min="1" value="1" />
                  </div>
                  <div style="clear:both"></div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="simpankurir()"><i class="fa fa-save"></i> Simpan </button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {
    prevkodetransaksi();
    <?php foreach ($kurir as $key => $value) {
      echo 'loadkurir('.json_encode($value).'); ';
    }
    ?>
});
function tambahkode(kode) {
  var kodetransaksi = $('#kodetransaksi');
  var prefixStr = kodetransaksi.val().substring(0, kodetransaksi.prop("selectionStart"));
  var sufixStr = kodetransaksi.val().substring(kodetransaksi.prop("selectionEnd"), kodetransaksi.val().length);
  $('#kodetransaksi').val(prefixStr+'[['+kode+']]'+sufixStr);
  prevkodetransaksi();
}
function prevkodetransaksi() {
  $('#prevkodetransaksi').html(kodereplace($('#kodetransaksi').val(),{'id_user':5,'id_transaksi':7}));
}
var counter=0;
function tambahkurir() {
  $('#popupkurir').modal();
  $('#popupkurir input.form-control,#popupkurir textarea.form-control').val('');
  if (counter<=0) $('#utama').val(1); else $('#utama').val(0);
}
function ubahkurir(id) {
  tambahkurir();
  $('#trkurir'+id+' input').map(function(){
    $('#popupkurir .form-control#'+$(this).attr('data-key')).val($(this).val()).trigger('click');
  });
  $('#popupkurir .form-control#id').val(id);
}
function hapuskurir(id) {
  if (confirm('Apakah kamu yakin ingin menghapus "'+$('#jdl'+id).html()+'" ?')){
    if (typeof $('#id'+id).val() !== "undefined") {
      var hapuskurir=($('#hapuskurir').val()=='')?$('#id'+id).val():$('#hapuskurir').val()+','+$('#id'+id).val();
      $('#hapuskurir').val(hapuskurir);
    }
    $('#trkurir'+id).remove();
  }
}

function simpankurir() {
  if ($('#nama').val()!=''&&$('#kapasitas').val()!='') {
    $('#popupkurir').modal('hide');
    masukkandata();
  } else {
    alert('Mohon masukkan semua data dengan benar');
  }
}

function masukkandata() {
  var values = {};
  $("#popupkurir .form-control").map(function(){
    values[$(this).attr('name')]=$(this).val();
  });
  loadkurir(values);
}

function loadkurir(datas) {
  var htmlhid='';
  if (datas['id']=='' || typeof $('#nama'+datas['id']).val() == 'undefined') {
    counter++;
    datas['id']=counter;
    $.each(datas, function (key, value){
      htmlhid+='<input type="hidden" id="'+key+counter+'" class="form-control" data-key="'+key+'" name="'+key+'['+counter+']" value="'+value+'" />';
    });
    var btn = (counter>1)?'<i class="fa fa-pencil" style="cursor:pointer;" onclick="ubahkurir('+counter+')"></i> &nbsp; <i class="fa fa-trash" style="cursor:pointer;" onclick="hapuskurir('+counter+')"></i> ':'<i class="fa fa-pencil" style="cursor:pointer;" onclick="ubahkurir('+counter+')"></i>';
    $('#listkurir tbody').append('<tr id="trkurir'+counter+'"><td>'+htmlhid+'<span id="namateks'+counter+'">'+datas['nama']+'</span></td><td id="kapasitasnamateks'+counter+'">'+datas['kapasitas']+'</td><td>'+btn+'</td></tr>');
  } else {
    console.log(datas);
    $.each(datas, function (key, value){
      $('#'+key+datas['id']).val(value);
    });
    $('#namateks'+datas['id']).html(datas['nama']);
    $('#kapasitasnamateks'+datas['id']).html(datas['kapasitas']);
  }
}
</script>
@endsection
