@extends('layouts.app')

@section('content')
<?php $id_komoditas=isset($_GET['id_komoditas'])?$_GET['id_komoditas']:0; $pilihkomoditas=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Pengaturan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="tabelkomoditas" class="table table-condensed table-striped">
                    <input type="hidden" class="id_kota" name="id_kota" value="0" />
                    <input type="hidden" class="id_user" name="id_user" value="{{ Auth::user()->id }}" />
                  <tbody class="row_position">
                    <tr><td><a href="{{ url('/admin/pengaturan/identitas') }}"><i class="fa fa-globe"></i> Identitas Situs</a></td></tr>
                    <tr><td><a href="{{ url('/admin/pengaturan/image') }}"><i class="fa fa-image"></i> Pengaturan Image</a></td></tr>
                    <tr><td><a href="{{ url('/admin/pengaturan/transaksi') }}"><i class="fa fa-money"></i> Pengaturan Transaksi</a></td></tr>
                    <tr><td><a href="{{ url('/admin/pengaturan/dokumen') }}"><i class="fa fa-file"></i> Kelola Dokumen</a></td></tr>
                    <tr><td><a href="{{ url('/admin/pengaturan/andalan') }}"><i class="fa fa-database"></i> Komoditas Andalan</a></td></tr>
                  </tbody>
                  </table>
                </div>

              </div>

              <!-- Modal -->
              <div class="modal modal-success fade" id="editkomoditas">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Edit Komoditas</h4>
                      </div>
                      <div class="modal-body">
                        <form id="formdokumenkomoditas" method="post" enctype="multipart/form-data"  action="">
                          <div class="col-xs-12 col-sm-6">
                            <input type="file" class="inputimage" style="display:none" data-name="imgk" name="inputimgk" id="inputimgk" accept="image/*">
                            <img id="imgeditkomoditas" class="previewimageimgk" src="" style="width:100%;max-height: 300px;cursor:pointer;" onclick="$('#inputimgk').click()" />
                          </div>
                          <div class="col-xs-12 col-sm-6">
                            <input type="hidden" class="form-control" id="id" name="id" />
                            <div class="form-group">
                              <label for="name">Nama Komoditas</label>
                              <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Komoditas" />
                            </div>
                            <div class="form-group">
                              <label for="name">Satuan</label>
                              <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" />
                            </div>
                          </div>
                          <div style="clear:both"></div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline" onclick="simpandata()">Simpan</button>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>


            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {

  $(".row_position").sortable
        ({
            scroll: false,
            sort: function (event, ui) {
                ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
            },
            helper: 'clone',
            axis: 'y',
            update: function () {
              var selectedData = new Array();
              $('.row_position>tr').each(function() {
                  selectedData.push($(this).attr("urutan"));
              });
              updateOrder(selectedData);
            }
        }).disableSelection();
  <?=$pilihkomoditas?>
});

function bukaeditkomoditas(id_komoditas,nama_komoditas,satuan) {
  $('#imgeditkomoditas').attr('src',$('#imgkom'+id_komoditas).attr('src'));
  $('#id').val(id_komoditas);
  $('#nama').val(nama_komoditas);
  $('#satuan').val(satuan);
  $('#editkomoditas').modal();
}
function updateOrder(data) {
    console.log(data);
}
function togglekomoditas(kelas){
  $('.'+kelas).toggle();
}
function bukakomoditas() {
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
}
function clearform() {
  $('#tabelkomoditas input.form-control').val(0);
}
function pilihkomoditas(id_komoditas,nama_komoditas) {
  clearform();
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
  $('#tabelkomoditasbox .id_komoditas').val(id_komoditas);
  $('#tabelkomoditasbox .tanggal').val($('#tabelkomoditasbox .tanggal').val());
  $('#tabelkomoditasbox .box-title').html('List Komoditas '+nama_komoditas);
  getdata();
}
function simpandata() {
  var formData = new FormData(document.getElementById('formdokumenkomoditas'));
  $.ajax({
      url: "{{ url('/api/komoditas') }}",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkomoditas_data') }}",
      type: 'POST',
      data: {
        'tanggal':$('#tabelkomoditasbox .tanggal').val(),
        'id_komoditas':$('#tabelkomoditasbox .id_komoditas').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#hargakomoditas'+value['id_komoditas']).val(value['harga']); $('#stokkomoditas'+value['id_komoditas']).val(value['stok']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
