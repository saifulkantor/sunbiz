@extends('layouts.app')

@section('content')
<?php $id_dokumen=isset($_GET['id_dokumen'])?$_GET['id_dokumen']:0; $pilihdokumen=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabeldokumenbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Kelola Dokumen</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="tutupbox()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div id="boxindex" class="box-body">
                  <button type="button" class="btn btn-primary" onclick="ubahdokumen(0)"> Tambah Dokumen </button>
                  <table id="tabeltemplate" class="tabeltemplate table table-bordered table-striped">
                    <thead>
                      <tr><th>Nama Dokumen</th><th>Kategori</th><th></th></tr>
                    </thead>
                    <?php foreach ($dokumen as $key => $value) {
                        echo '<tr><td class="nama'.$value->id.'">'.$value->nama.'</td><td>'.$dokumenmdl->listkategori()[$value->kategori].'<span style="display:none" class="kategori'.$value->id.'">'.$value->kategori.'</span></td><td><i class="fa fa-pencil" onclick="ubahdokumen('.$value->id.')" style="cursor:pointer"></i> &nbsp; <i class="fa fa-trash" onclick="hapusdokumen('.$value->id.')" style="cursor:pointer"></i></td></tr>';
                    } ?>
                  </table>

                </div>

                <div id="boxdetail" class="box-body row" style="display:none">
                  <form id="formdokumen" method="post" action="" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id" name="id" value="0" />
                    <div class="form-group col-xs-6">
                      <label>Nama Dokumen</label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="fa fa-book"></i>
                        </div>
                        <input type="text" id="nama" name="nama" value="" max="100" required placeholder="Tulis Nama Dokumen" class="form-control" />
                      </div>
                    </div>
                    <div class="form-group col-xs-6">
                      <label>Kategori</label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="fa fa-book"></i>
                        </div>
                        <select id="kategori" name="kategori" class="form-control">
                          <?php foreach ($dokumenmdl->listkategori() as $key => $kategori) {
                            echo '<option value="'.$key.'">'.$kategori.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Isi Dokumen</label>
                      <div class="input-group" style="width: 100%;">
                        <textarea id="textareasemua" rows="10" placeholder="Tulis Template Anda di Sini" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                      </div>
                    </div>
                    <textarea name="template" id="template" style="display:none"></textarea>
                    <button type="button" class="btn btn-primary" onclick="simpandokumen()"> Simpan </button>

                  </form>
                </div>

              </div>


            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {

  var templatealignment = {
        textAlign: function(context) {
          return "<li><div class='btn-group'>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
              "<span class='glyphicon glyphicon-align-left'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
              "<span class='glyphicon glyphicon-align-center'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
              "<span class='glyphicon glyphicon-align-right'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='JustifyFull' data-wysihtml5-command-value='&JustifyFull;' title= 'Align text Justify'>" +
              "<span class='glyphicon glyphicon-align-justify'></span></a>" +
              "</li>";
        },
        pilihShortcode: function(context) {
          return "<li>" +
          "<button type='button' class='btn btn-primary' onclick=\"$('#modalpilihShortcode').modal();\" >Shortcode</button>" +
          "<div class=\"modal fade\" id=\"modalpilihShortcode\">" +
            "<div class=\"modal-dialog modal-md\">" +
              "<div class=\"modal-content\">" +
                "<div class=\"modal-header\">" +
                    "<a class=\"close\" data-dismiss=\"modal\">&times;</a>" +
                  "<h3>Tambah Shortcode</h3>" +
                "</div>" +
                "<div class=\"modal-body\">" +
                "<?php $listshortcode = $dokumenmdl->listkode('invoice');
                //data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='&hellip;'
                foreach ($listshortcode as $key => $value) {
                  if ($value==''){
                      echo "<h4>".$key."</h4>";
                  } else {
                    echo "<a data-wysihtml5-command='insertHTML' class='btn btn-link' data-wysihtml5-command-value='&#123;&#123;".$key."&#125;&#125;' data-dismiss='modal'>".$value."</a>";
                  }
                }
                ?><div style=\"clear:both\"></div>"+
                "</div>" +
                "<div class=\"modal-footer\">" +
                  "<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Tutup</button>" +
                "</div>" +
              "</div>" +
            "</div>" +
          "</div>" +
          "</li>";
        }
      };
  $('#textareasemua').wysihtml5({
    classes: {
      "wysiwyg-text-align-center": 1,
      "wysiwyg-text-align-justify": 1,
      "wysiwyg-text-align-left": 1,
      "wysiwyg-text-align-right": 1
    },
    "toolbar": {
      "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
      "emphasis": true, //Italics, bold, etc. Default true
      "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
      "html": false, //Button which allows you to edit the generated HTML. Default false
      "link": true, //Button to insert a link. Default true
      "image": true, //Button to insert an image. Default true,
      "color": true, //Button to change color of font
      "blockquote": false,
      'textAlign': true, // custom defined buttons to align text see myCustomTemplates variable above
      'pilihShortcode': true
    },
    stylesheets: ["<?=url('/')?>/css/dokumen.css"],
    customTemplates: templatealignment
  });

  <?=$pilihdokumen?>
});

function tutupbox() {
  if ($('#boxindex').is(':visible')){
    location.href='{{ url('/admin/pengaturan') }}';
  } else {
    $('#boxdetail').hide();
    $('#boxindex').show();
  }
}

function simpandokumen() {
  $('#template').val($('iframe').contents().find('.wysihtml5-editor').html());
  $('#formdokumen').submit();
}

function ubahdokumen(id_dokumen){
  $('#boxindex').hide();
  $('#boxdetail').show();
  dataajaxdokumen(id_dokumen);
}

function dataajaxdokumen(id_dokumen) {
  $('iframe').contents().find('.wysihtml5-editor').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#id').val(id_dokumen);
  $('#nama').val($('.nama'+id_dokumen).html());
  $('#kategori').val($('.kategori'+id_dokumen).html());
  $.ajax({
      url: "{{ url('/api/getdokumen') }}",
      type: 'POST',
      data: {
        'id' : id_dokumen,
      },
      dataType: 'JSON',
      success: function (data) {
        console.log(data);
        id_dokumendipilih=id_dokumen;
        $('iframe').contents().find('.wysihtml5-editor').html(data);
        $('#boxdetail').show();$('#boxindex').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}

function hapusdokumen(id_dokumen) {
  if (confirm('Apakah kamu yakin ingin menghapus dokumen ini ?')) {
    $.ajax({
        url: "{{ url('/api/hapusdokumen') }}",
        type: 'POST',
        data: {
          'id' : id_dokumen,
        },
        dataType: 'JSON',
        success: function (data) {
          if (data=='sukses') {
            location.reload();
          } else {
            alert(data);
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
        }
    });
  }
}

</script>
