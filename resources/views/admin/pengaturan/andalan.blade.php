@extends('layouts.app')

@section('content')
<?php
$listkomoditas = explode(",",$pengaturan[0]->data);
?>
<style type="text/css">
.dipilih{
  cursor: pointer;
  background: white;
  color: #00a65a;
}
</style>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Komoditas Andalan</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="location.href='{{ url('/admin/pengaturan') }}'"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="">
                    @csrf
                    <div class="form-group">
                      <?php for ($i=0;$i<6;$i++) {
                        $idkom=isset($listkomoditas[$i])?$listkomoditas[$i]:0;
                        $namakom=($idkom!=0)?$komoditas[array_search($idkom, array_column($komoditas, 'id'))]->nama:'-- BELUM DIPILIH --';
                        if(file_exists(public_path('images/komoditas/'.$idkom.'.jpg'))){
            						    $image = url('/images/komoditas/'.$idkom.'.jpg');
            						}else{
            						    $image = url('/images/komoditas/default.jpg');
            						}
                        echo '<input type="hidden" name="komoditas['.$i.']" id="idkomoditas'.$i.'" value="'.$idkom.'" />
                        <div class="col col-sm-4 col-lg-2" style="cursor:pointer" onclick="bukalistkomoditas('.$i.')"><img id="imgkomoditas'.$i.'" src="'.$image.'" alt=" " class="img-responsive" />
        								<p id="namakomoditas'.$i.'">'.$namakom.'</p></div>';
                      } ?>

                      <div style="clear:both"></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </form>
                </div>

              </div>

              <div class="modal modal-success fade" id="pilihkomoditas">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Pilih Komoditas</h4>
                      </div>
                      <div class="modal-body">
                        <p>Yang dipilih : <b class="pilihan"></b></p>
                        <div class="col-xs-6" style="height:300px;overflow-y:auto;border:1px solid #fff;padding: 10px;">
                          <?php foreach ($komoditas as $key => $kom) {
                            if ($kom->id_parent==null){
                              echo '<p id="komparent'.$kom->id.'" onclick="pilihkomoditas('.$kom->id.')" style="cursor:pointer">'.$kom->nama.'</p>';
                            }
                          } ?>
                        </div>
                        <div class="col-xs-6" id="anakkomoditas" style="height:300px;overflow-y:auto;border:1px solid #fff;padding: 10px;">
                        </div>
                        <div style="clear:both"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline" onclick="simpandata()">Simpan</button>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>


            </div>
        </div>
    </div>
    <script type="text/javascript">
    var urutandipilih=0; var komoditasdipilih=0;
    var komoditas = <?=json_encode($komoditas)?>;
    var listkomoditas = <?=json_encode($listkomoditas)?>;
    function bukalistkomoditas(id_urutan) {
      urutandipilih=id_urutan;
      $('#pilihkomoditas').modal();
      var id_komoditas = listkomoditas[id_urutan];
      var komdipilih = komoditas.filter(function (kom) { return kom.id == id_komoditas });
      komoditasdipilih=komdipilih[0]['id'];
      $('.pilihan').html(komdipilih[0]['nama']);
      $('#pilihkomoditas p').removeClass('dipilih');
    }
    function pilihkomoditas(id_komoditas) {
      var anakkomoditastml='';
      $.each(komoditas,function(index,value){
        if (value['id_parent']==id_komoditas) anakkomoditastml+='<p id="anakkom'+index+'" style="cursor:pointer" onclick="gantipilihan('+index+')">'+value['nama']+'</p>';
      });
      $('#anakkomoditas').html(anakkomoditastml);
      $('#pilihkomoditas p').removeClass('dipilih');
      $('#komparent'+id_komoditas).addClass('dipilih');
    }
    function gantipilihan(index) {
      komoditasdipilih = komoditas[index]['id'];
      $('.pilihan').html(komoditas[index]['nama']);
      $('#anakkomoditas p').removeClass('dipilih');
      $('#anakkom'+index).addClass('dipilih');
    }
    function simpandata() {
      var image ='';
      if(imageExists('<?=url('/')?>/images/komoditas/'+komoditasdipilih+'.jpg')) {
          image = '<?=url('/')?>/images/komoditas/'+komoditasdipilih+'.jpg';
      }else{
          image = '<?=url('/')?>/images/komoditas/default.jpg';
      }
      $('#namakomoditas'+urutandipilih).html($('.pilihan').html());
      $('#idkomoditas'+urutandipilih).val(komoditasdipilih);
      $('#imgkomoditas'+urutandipilih).prop('src',image);
      $('#pilihkomoditas').modal('hide');
    }
    </script>
@endsection
