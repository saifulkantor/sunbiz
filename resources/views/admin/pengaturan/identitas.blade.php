@extends('layouts.app')

@section('content')
<?php
$namawebsite = explode("||",$pengaturan[1]->data);
$detailwebsite = $pengaturan[2]->data;
$contact = $pengaturan[3]->data;
$email = $pengaturan[4]->data;
$address = $pengaturan[5]->data;
$sosmed = explode("||",$pengaturan[6]->data);
?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Pengaturan Identitas</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="location.href='{{ url('/admin/pengaturan') }}'"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="">
                    @csrf
                    <div class="form-group">
                      <label for="namawebsite">Nama Website</label>
                      <div class="row">
                        <div class="col-sm-3"><input type="text" id="namawebsite1" name="namawebsite1" class="form-control" value="<?=$namawebsite[0]?>" onkeyup="$('#prevnamawebsite1').html($(this).val())" /></div>
                        <div class="col-sm-3"><input type="text" id="namawebsite2" name="namawebsite2" class="form-control" value="<?=$namawebsite[1]?>" onkeyup="$('#prevnamawebsite2').html(' '+$(this).val())" /></div>
                        <div class="col-sm-3"><input type="text" id="namawebsite3" name="namawebsite3" class="form-control" value="<?=$namawebsite[2]?>" onkeyup="$('#prevnamawebsite3').html(' '+$(this).val())" /></div>
                      </div>
                      <p style="margin-top:10px">Preview</p>
                      <div class="w3_agile_logo" style="float: left;">
              					<h1 style="margin:0px"><span style="padding: .2em .5em;background: #a4dd25;display: inline-block;color: #212121;" id="prevnamawebsite1"><?=$namawebsite[0]?></span> <span id="prevnamawebsite2"><?=$namawebsite[1]?></span>
                          <i style="display: block;font-size: 0.3em;text-align: right;letter-spacing: 2.5px;text-transform: capitalize;" id="prevnamawebsite3">  <?=$namawebsite[2]?></i></h1>
              				</div>
                      <div style="clear:both"></div>
                    </div>
                    <div class="row">
                      <div class="form-group col-sm-6 col-md-3">
                        <label for="name">Detail Website</label>
                        <textarea name="detailwebsite" class="form-control" rows="5" placeholder="Tulis Penjelasan singkat mengenai Website atau Perusahaan"><?=$detailwebsite?></textarea>
                      </div>
                      <div class="form-group col-sm-6 col-md-3">
                        <label for="name">Alamat</label>
                        <textarea name="address" class="form-control" rows="5" placeholder="Alamat Lengkap Perusahaan"><?=$address?></textarea>
                      </div>
                      <div class="form-group col-sm-6 col-md-3">
                        <label for="name">Contact Info</label>
                        <textarea name="contact" class="form-control" rows="5" placeholder="Contoh: Phone/SMS : +62 832 3223 3232"><?=$contact?></textarea>
                      </div>
                      <div class="form-group col-sm-6 col-md-3">
                        <label for="name">Email</label>
                        <textarea name="email" class="form-control" rows="5" placeholder="Contoh: Office : officesunbiz@gmail.com"><?=$email?></textarea>
                      </div>
                    </div>
                    <label for="namawebsite">Social Media</label>
                    <div class="row">
                      <div class="form-group col-sm-6 col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-facebook"></i>
                          </div>
                          <input name="sosmed1" class="form-control" value="<?=$sosmed[0]?>" />
                        </div>
                      </div>
                      <div class="form-group col-sm-6 col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-instagram"></i>
                          </div>
                          <input name="sosmed2" class="form-control" value="<?=$sosmed[1]?>" />
                        </div>
                      </div>
                      <div class="form-group col-sm-6 col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-twitter"></i>
                          </div>
                          <input name="sosmed3" class="form-control" value="<?=$sosmed[2]?>" />
                        </div>
                      </div>
                      <div style="clear:both"></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </form>
                </div>
              </div>


            </div>
        </div>
    </div>
<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {

  $(".row_position").sortable
        ({
            scroll: false,
            sort: function (event, ui) {
                ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
            },
            helper: 'clone',
            axis: 'y',
            update: function () {
              var selectedData = new Array();
              $('.row_position>tr').each(function() {
                  selectedData.push($(this).attr("urutan"));
              });
              updateOrder(selectedData);
            }
        }).disableSelection();

});

function bukaeditkomoditas(id_komoditas,nama_komoditas,satuan) {
  $('#imgeditkomoditas').attr('src',$('#imgkom'+id_komoditas).attr('src'));
  $('#id').val(id_komoditas);
  $('#nama').val(nama_komoditas);
  $('#satuan').val(satuan);
  $('#editkomoditas').modal();
}
function updateOrder(data) {
    console.log(data);
}
function togglekomoditas(kelas){
  $('.'+kelas).toggle();
}
function bukakomoditas() {
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
}
function clearform() {
  $('#tabelkomoditas input.form-control').val(0);
}
function pilihkomoditas(id_komoditas,nama_komoditas) {
  clearform();
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
  $('#tabelkomoditasbox .id_komoditas').val(id_komoditas);
  $('#tabelkomoditasbox .tanggal').val($('#tabelkomoditasbox .tanggal').val());
  $('#tabelkomoditasbox .box-title').html('List Komoditas '+nama_komoditas);
  getdata();
}
function simpandata() {
  var formData = new FormData(document.getElementById('formdokumenkomoditas'));
  $.ajax({
      url: "{{ url('/api/komoditas') }}",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkomoditas_data') }}",
      type: 'POST',
      data: {
        'tanggal':$('#tabelkomoditasbox .tanggal').val(),
        'id_komoditas':$('#tabelkomoditasbox .id_komoditas').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#hargakomoditas'+value['id_komoditas']).val(value['harga']); $('#stokkomoditas'+value['id_komoditas']).val(value['stok']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
@endsection
