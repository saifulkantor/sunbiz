@extends('layouts.app')

@section('content')
<?php
$nama_website = str_replace("||"," ",$pengaturan[1]->data);
$detail_website = $pengaturan[2]->data;
$contact_website = $pengaturan[3]->data;
$email_website = $pengaturan[4]->data;
$alamat_website = $pengaturan[5]->data;
$sosmed_website = explode("||",$pengaturan[6]->data);
?>
<div class="row justify-content-center">
	<div class="col-xs-12">
		<div id="tabeltransaksibox" class="box box-default">
			<div class="box-header with-border">
				<?=$transaksimodel->getlabelstatus($transaksi->status)?>
				<h3 class="box-title"><a href="{{ url('/admin/transaksi') }}" class="btn-sm btn-default">&lt;&lt;</a> Detail Transaksi</h3>
			</div>
			<div class="box-body">
		<div class="pull-left">
			<h3 style="margin-top:0"><strong><?=$transaksi->nama_pembeli?></strong></h3>
			<h3>#<?=$transaksi->kode_transaksi?></h3>
			<p><?=date('d F Y h:m',strtotime($transaksi->tanggal_transaksi))?></p>
		</div>
		<?php if ($transaksi->status=='pendingadmin') { ?>
		<div class="pull-right action">
			<button class="btn btn-primary" onclick="gantistatus('pendingpayment')">Terima Pesanan</button>
			<button class="btn btn-danger" onclick="gantistatus('rejected')">Tolak Pesanan</button>
		</div>
	<?php } else if ($transaksi->status=='pendingpayment') { ?>
	<div class="pull-right action">
		<button class="btn btn-primary" onclick="buatinvoice()">Buat Invoice</button>
		<button class="btn btn-primary" onclick="gantistatus('send')">Proses Pengiriman</button>
	</div>
	<?php } ?>
		<div style="clear:both"></div>
		<h3>Detail Transaksi</h3>
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<input type="hidden" class="form-control" id="status" name="status" value="<?=$transaksi->status?>" />
				<?php $total = 0;
				foreach ($transaksidetail as $key => $value) {
					if(file_exists(public_path('images/komoditas/'.$value->id_komoditas.'.jpg'))){
							$image = url('/images/komoditas/'.$value->id_komoditas.'.jpg');
					}else{
							$image = url('/images/komoditas/default.jpg');
					}
				echo '<div class="row" style="margin-bottom:15px"><div class="col-xs-3 col-sm-2"><img src="'.$image.'" alt=" " class="img-responsive" style="padding: 5px 0px;"></div><div class="col-xs-9 col-sm-10">
						<h3 style="" class="pull-right" id="total'.$value->id.'"> Rp. '.number_format(($value->harga*$value->jumlah),0,',','.').' </h3>
						<h3 class="namaproduk" style="margin-top:0">'.$value->nama.'</h3>
						<p class="pull-left" style="margin:0">Jumlah : <span id="jml'.$value->id.'">'.$value->jumlah.'</span> '.$value->satuan.' @ Rp. '.number_format($value->harga,0,',','.').'</p>';
					if ($transaksi->status=='pendingadmin') {
				echo '<div style="clear:both">
							<p id="ubahjumlah'.$value->id.'" style="color:blue;cursor:pointer" onclick="ubahjumlah('.$value->id.')">Ubah Jumlah</p>
							<div class="input-group jumlah'.$value->id.'" style="width:150px;display:none">
								<input id="jumlah'.$value->id.'" name="jumlah['.$value->id.']" type="number" class="form-control" value="'.$value->jumlah.'" min="1" max="'.number_format(($value->stok+$value->jumlah),0,',','.').'" onkeyup="hitungsemua()" />
								<span class="input-group-addon" style="cursor:pointer" onclick="simpanjumlah('.$value->id.')"><i class="fa fa-check"></i></span>
							</div>
							<sub class="jumlah'.$value->id.'" style="display:none">Sisa Stok : <span id="sisastok'.$value->id.'">'.$value->stok.'</span> </sub>
							</div>';
						}
				echo '</div></div>';
				$total+=($value->harga*$value->jumlah);
				} ?>
				<div class="col-xs-12" style="margin-bottom:15px">
					<h3>Catatan</h3>
					<p><?=($transaksi->catatan!='')?$transaksi->catatan:''?></p>
				</div>
			</div>
			<div class="col-sm-5 col-md-4">
				<div id="alamatpengiriman"><h3 style="margin-top:0">Alamat Pengiriman</h3><div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
					<?php
					echo '<p id="alm">'.$transaksi->alamat_pembeli.'</p>';
					 ?>
					</div>
				</div>
				<div id="metodepembayaran">
					<h3>Metode Pembayaran</h3>
					<div class="w3_agileits_mail_right_grid" style="margin: 5px 0px;padding:20px">
						<input type="hidden" class="form-control" name="metode_pembayaran" id="metode_pembayaran" value="transferbank">
						<h4><?=ucfirst($transaksi->metode_pembayaran)?></h4>
					</div>
				</div>
				<h3>Rincian Biaya</h3><input type="hidden" name="diskon" class="form-control" value="0" />
				<div class="row" style="padding-bottom:20px;margin-top: 10px;">
					<div class="col-xs-6"><h4><b>SUBTOTAL </b></h4></div>
					<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="subtotal">Rp. <?=number_format($total,0,',','.')?> </h3></div>
					<div class="col-xs-6"><h4><b> ONGKIR </b></h4></div>
					<div class="col-xs-6">
						<h3 style="font-weight:bold" class="pull-right" id="ongkir">Rp. <?=number_format($transaksi->ongkir,0,',','.')?> </h3>
						<?php if ($transaksi->status=='pendingadmin') { ?>
						<p id="ubahongkir" style="color:blue;cursor:pointer;clear:both;margin:0" onclick="ubahongkir()" class="pull-right">Ubah Ongkir</p>
						<div class="input-group ongkir" style="width: 150px;display:none">
							<input id="ongkirinput" name="ongkir" type="number" class="form-control" value="<?=$transaksi->ongkir?>" min="0" onkeyup="hitungsemua()">
							<span class="input-group-addon" style="cursor:pointer" onclick="simpanongkir()"><i class="fa fa-check"></i></span>
						</div>
					<?php } ?>
					</div>
					<div class="col-xs-6"><h4><b> TOTAL </b></h4></div>
					<div class="col-xs-6"><h3 style="font-weight:bold" class="pull-right" id="totalsemua">Rp. <?=number_format($transaksi->total,0,',','.')?> </h3></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if ($transaksi->status=='pendingpayment') { ?>
<div id="dokumeninvoice" class="box box-default" style="display:none">
	<div class="box-header with-border">
		<?=$transaksimodel->getlabelstatus($transaksi->status)?>
		<h3 class="box-title"><span onclick="hideinvoice()" class="btn-sm btn-default" style="cursor:pointer">&lt;&lt;</span> Invoice</h3>
	</div>
	<div class="box-body">
		<h3 style="margin-top:0"><strong><?=$transaksi->nama_pembeli?></strong></h3>
		<h3>#<?=$transaksi->kode_transaksi?></h3>
		<p><?=date('d F Y h:m',strtotime($transaksi->tanggal_transaksi))?></p>
		<div class="form-group">
			<div class="input-group" style="width: 100%;">
				<textarea id="textareasemua" rows="10" placeholder="Tulis Template Anda di Sini" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
			</div>
		</div>
	</div>
</div>
<?php } ?>
  </div>
</div>
<?php if ($transaksi->status=='pendingpayment') { ?>
<div class="modal modal-default fade" id="popupinvoice">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Pilih Dokumen</h4>
			</div>
			<div class="modal-body">
				<?php foreach ($dokumen as $key => $value) {
					echo '<div class="small-box bg-green btn col-sm-6" onclick="bukainvoice('.$value->id.')">
			      <div class="inner">
			        <h3 style="white-space: pre-line;min-height:70px">'.$value->nama.'</h3>
			      </div>
			      <div class="icon">
			        <i class="fa fa-file"></i>
			      </div>
			    </div>
					';
				} ?>
				<div style="clear:both"></div>
			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>

<div class="modal modal-default fade" id="popupkiriminvoice">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Kirim Invoice</h4>
			</div>
			<div class="modal-body">
				<div class="form-group col-md-6">
					<label for="email">Kirim ke Email</label>
					<input type="email" class="form-control" id="email" name="email" requeired placeholder="Email Penerima" value="<?=$transaksi->email?>" />
				</div>
				<div style="clear:both"></div>
			</div>
			<div class="modal-footer">
				<button type='button' class='btn btn-primary' onclick='kiriminvoice()'> <i class='fa fa-envelope'></i> Kirim </button>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script type="text/javascript">
var detailtransaksi=<?=json_encode($transaksidetail)?>;
function lihattransaksi(id_transaksi) {
	location.href="{{ url('/transaksi/detail') }}/"+id_transaksi;
}
function ubahjumlah(id) {
	$('#ubahjumlah'+id).hide();
	$('.jumlah'+id).show();
}
function simpanjumlah(id) {
	$('#ubahjumlah'+id).show();
	$('.jumlah'+id).hide();
}
function ubahongkir() {
	$('#ubahongkir').hide();
	$('.ongkir').show();
}
function simpanongkir() {
	$('#ubahongkir').show();
	$('.ongkir').hide();
}
function buatinvoice() {
	$('#popupinvoice').modal();
}

function bukainvoice(id_dokumen) {
	$('#dokumeninvoice').show();
	$('#popupinvoice').modal('hide');
	$('#tabeltransaksibox').hide();
	dataajaxdokumen(id_dokumen);
}

function hideinvoice() {
	$('#tabeltransaksibox').show();
	$('#dokumeninvoice').hide();
}

function hitungsemua() {
	var subtotal=0;
	$.each(detailtransaksi,function(index,value){
		if ($('#jumlah'+value['id']).val()>(value['jumlah']+value['stok'])){
			alert('Jumlah Melebihi Stok');
			$('#jumlah'+value['id']).val(value['jumlah']+value['stok']);
		}
		$('#jml'+value['id']).html($('#jumlah'+value['id']).val());
		var totalitem = $('#jumlah'+value['id']).val()*value['harga'];
		$('#total'+value['id']).html(' Rp. '+totalitem.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
		$('#sisastok'+value['id']).html(value['stok']-($('#jumlah'+value['id']).val()-value['jumlah']));
		subtotal+=totalitem;
	});
	$('#subtotal').html('Rp. '+subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
	$('#ongkir').html('Rp. '+parseInt($('#ongkirinput').val()).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
	var total = subtotal+parseInt($('#ongkirinput').val());
	$('#totalsemua').html('Rp. '+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
}

function gantistatus(stat='') {
		var teks='Apakah kamu yakin mengubah status transaksi ini ?';
		switch (stat) {
			case 'rejected':
				teks='Apakah Anda yakin ingin menolak pesanan ini ?';
				break;
			case 'paymentpending':
				teks='Apakah Anda yakin ingin menerima pesanan ini ?';
				break;
			case 'send':
				teks='Apakah Anda yakin ingin mengubah status transaksi ini menjadi Dikirim ?';
				break;
			default:
		}
		if (confirm(teks)) {
			$('#status').val(stat);
		  $.ajax({
		      url: "<?=url('/api/transaksi/ubahdetail/'.$transaksi->kode_transaksi.'')?>",
		      type: 'POST',
		      data: $('#tabeltransaksibox .form-control').serialize(),
		      dataType: 'JSON',
		      success: function (data) { if (data=='sukses') { alert('Data Berhasil Diubah'); } else { alert(data); } location.reload(); },
		      error: function(XMLHttpRequest, textStatus, errorThrown) {
		        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
		      }
		  });
	}
}

function kiriminvoice() {
	$.ajax({
			url: "<?=url('/api/transaksi/kiriminvoice/'.$transaksi->kode_transaksi.'')?>",
			type: 'POST',
			data: {
				'email' : $('#email').val(),
				'template': $('iframe').contents().find('.wysihtml5-editor').html(),
			},
			dataType: 'JSON',
			success: function (data) { if (data=='sukses') { alert('Invoice Berhasil Dikirim'); } else { alert(data); } location.reload(); },
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
			}
	});
}

function dataajaxdokumen(id_dokumen) {
	var date = new Date();
	var koder = {
    'tahun':date.getFullYear(),
    'bulan':date.getMonth()+1,
    'tanggal':date.getDate(),
		'nama_website':'<?=$nama_website?>',
		'alamat_website':<?=json_encode($alamat_website)?>,
		'telp_website':<?=json_encode($contact_website)?>,
		'tabel_detail_transaksi':<?=json_encode($transaksimodel->tabel_detail_transaksi($transaksidetail,$transaksi))?>,
  };
  $('iframe').contents().find('.wysihtml5-editor').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#kategori').val($('.kategori'+id_dokumen).html());
  $.ajax({
      url: "{{ url('/api/getdokumen') }}",
      type: 'POST',
      data: {
        'id' : id_dokumen,
      },
      dataType: 'JSON',
      success: function (data) {
        id_dokumendipilih=id_dokumen;
				data=replacevariabeldokumen(data,<?=json_encode($transaksi)?>);
				data=replacevariabeldokumen(data,koder);
        $('iframe').contents().find('.wysihtml5-editor').html(data);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}

<?php if ($transaksi->status=='pendingpayment') { ?>
window.addEventListener('DOMContentLoaded', (event) => {

  var templatealignment = {
    textAlign: function(context) {
      return "<li><div class='btn-group'>" +
          "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
          "<span class='glyphicon glyphicon-align-left'></span></a>" +
          "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
          "<span class='glyphicon glyphicon-align-center'></span></a>" +
          "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
          "<span class='glyphicon glyphicon-align-right'></span></a>" +
          "<a class='btn btn-default' data-wysihtml5-command='JustifyFull' data-wysihtml5-command-value='&JustifyFull;' title= 'Align text Justify'>" +
          "<span class='glyphicon glyphicon-align-justify'></span></a>" +
          "</div></li>";
    },
		cetakInvoice: function(context) {
			return "<li class='pull-right'><button type='button' class='btn btn-primary' onclick='$(\"#popupkiriminvoice\").modal()'> <i class='fa fa-envelope'></i> Kirim </button> &nbsp; "+
			"<button type='button' class='btn btn-primary' onclick='cetakinvoice()'> <i class='fa fa-print'></i> Cetak </button></li>";
		}
  };
  $('#textareasemua').wysihtml5({
    classes: {
      "wysiwyg-text-align-center": 1,
      "wysiwyg-text-align-justify": 1,
      "wysiwyg-text-align-left": 1,
      "wysiwyg-text-align-right": 1
    },
    "toolbar": {
      "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
      "emphasis": true, //Italics, bold, etc. Default true
      "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
      "html": false, //Button which allows you to edit the generated HTML. Default false
      "link": true, //Button to insert a link. Default true
      "image": true, //Button to insert an image. Default true,
      "color": true, //Button to change color of font
      "blockquote": false,
      'textAlign': true,
			'cetakInvoice': true
    },
    stylesheets: ["<?=url('/')?>/css/dokumen.css"],
    customTemplates: templatealignment
  });

});
function cetakinvoice() {
  var html = $('iframe').contents().find('html').html();
  var w = window.open('','printwindow');
	w.document.write("<html><head><title>Invoice</title><link rel=\"stylesheet\" href=\"<?=url('/')?>/css/dokumen.css\" type=\"text/css\" /></head><body>" );
	w.document.write(html);
	w.document.write("</body></html>");
	w.document.close();
	w.focus();
	setTimeout(function() {
      w.print();
      w.close();
  }, 100);
}
<?php } ?>
</script>
@endsection
