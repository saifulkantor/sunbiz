@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
	<div class="col-xs-12">
		<div id="tabelcontributorbox" class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">List Transaksi</h3>
			</div>
			<div class="box-body">
				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link pendingadmin" href="#" role="tab" aria-controls="pills-home" aria-selected="true" onclick="gantistatus('pendingadmin')">Pending Admin</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link pendingpayment" href="#" role="tab" aria-controls="pills-profile" aria-selected="false" onclick="gantistatus('pendingpayment')">Belum Dibayar</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link send" href="#" role="tab" aria-controls="pills-contact" aria-selected="false" onclick="gantistatus('send')">Dikirim</a>
				  </li>
					<li class="nav-item">
				    <a class="nav-link all" href="#" role="tab" aria-controls="pills-contact" aria-selected="false" onclick="gantistatus('all')">Semua Transaksi</a>
				  </li>
				</ul>
				<table id="listtransaksi" class="table table-bordered table-hover" style="margin-bottom:20px">
					<thead><tr><th>ID Transaksi</th><th>Tanggal</th><th>Total</th><th>Pembeli</th><th>Status</th></tr></thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagingtransaksi" class="pull-left">

				</div>
			</div>
		</div>
  </div>
</div>
<script type="text/javascript">
var page=1; var perpage=20; var status='pendingadmin';
function lihattransaksi(id_transaksi) {
	location.href="{{ url('/admin/transaksi/detail') }}/"+id_transaksi;
}
function gantistatus(stat) {
	status = stat;
	$('.nav-link').removeClass('active');
	$('.nav-link.'+stat).addClass('active');
	getlisttransaksi();
}
function resetdatatransaksi() {
  $('#listtransaksi tbody').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#pagingtransaksi').html('');
}
function getlisttransaksi() {
	resetdatatransaksi();
	ajaxgetlisttransaksi();
}

function ajaxgetlisttransaksi() {
  $.ajax({
      url: "{{ url('/api/listtransaksi') }}",
      type: 'POST',
      data: {
				//'id_user':{{ auth()->user()->id }},
        'status':status,
        'page':page,
        'perpage':perpage,
      },
      dataType: 'JSON',
      success: function (data) {
				console.log(data);
        var html = '';
        $.each(data,function(index,value) {
          html+='<tr style="cursor:pointer" onclick="lihattransaksi(\''+value['kode']+'\')"><td>#'+value['kode']+'</td><td>'+value['tanggal']+'</td><td>Rp. '+value['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td><td>Rp. '+value['nama']+'</td><td>'+getlabelstatustransaksi(value['status'])+'</td></tr>';
        });
				html =(html=='')?'<tr><td colspan="10" style="text-align: center;"> Data Tidak Ditemukan </td></tr>':html;
        $('#listtransaksi tbody').html(html);
        var htmlpaging='';
        if (page>1) htmlpaging+='<button class="btn btn-primary" onclick="page-=1;getlisttransaksi()">&lt;&lt; Prev</button> &nbsp; &nbsp; ';
        if (data.length>=perpage) htmlpaging+='<button class="btn btn-primary" onclick="page+=1;getlisttransaksi()"> Next &gt;&gt;</button>';
        $('#pagingtransaksi').html(htmlpaging);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  gantistatus(status);
});
</script>
@endsection
