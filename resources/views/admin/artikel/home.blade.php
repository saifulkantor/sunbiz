@extends('layouts.app')

@section('content')
<?php $id_artikel=isset($_GET['id_artikel'])?$_GET['id_artikel']:0; $pilihartikel=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelartikelbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Kelola Artikel</h3>

                  <div class="box-tools pull-right" title="Tutup">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="tutupbox()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div id="boxindex" class="box-body">
                  <button type="button" class="btn btn-primary" onclick="ubahartikel(0)"> Tambah Artikel </button>
                  <table id="tabeltemplate" class="tabeltemplate table table-bordered table-striped">
                    <thead>
                      <tr><th>Judul Artikel</th><th>Kategori</th><th></th></tr>
                    </thead>
                    <?php foreach ($artikel as $key => $value) {
                        echo '<tr><td class="judul'.$value->id.'">'.$value->judul.'</td><td>'.$value->kategori.'<span style="display:none" class="id_kategori'.$value->id.'">'.$value->id_kategori.'</span></td><td><i class="fa fa-pencil" onclick="ubahartikel('.$value->id.')" style="cursor:pointer"></i> &nbsp; <i class="fa fa-trash" onclick="hapusartikel('.$value->id.')" style="cursor:pointer"></i></td></tr>
                        <span style="display:none" class="overview'.$value->id.'">'.$value->overview.'</span>';
                    } ?>
                  </table>

                </div>

                <div id="boxdetail" class="box-body" style="display:none">
                  <div style="display:none" class="id_kategori0">1</div>
                  <form id="formartikel" method="post" action="" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id" name="id" value="0" />
                    <input type="hidden" id="id_user" name="id_user" value="{{ auth()->user()->id }}" />
                    <div class="col-xs-4">
                      <input type="file" class="inputimage" style="display:none" data-name="imga" name="inputimga" id="inputimga" accept="image/*">
                      <img id="imgeditartikel" class="previewimageimga" src="" style="width:100%;max-height: 300px;cursor:pointer;" onclick="$('#inputimga').click()" />
                    </div>
                    <div class="col-xs-8">
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label>Judul Artikel</label>
                          <div class="input-group"><div class="input-group-addon">
                              <i class="fa fa-book"></i>
                            </div>
                            <input type="text" id="judul" name="judul" value="" max="100" required placeholder="Tulis Judul Artikel" class="form-control" />
                          </div>
                        </div>
                        <div class="form-group col-sm-6">
                          <label>Kategori</label>
                          <div class="input-group"><div class="input-group-addon">
                              <i class="fa fa-book"></i>
                            </div>
                            <select id="id_kategori" name="id_kategori" class="form-control">
                              <?php foreach ($artikelkategori as $key => $kategori) {
                                echo '<option value="'.$kategori->id.'">'.$kategori->nama.'</option>';
                              } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group col-xs-12">
                          <label>Overview</label>
                          <textarea name="overview" id="overview" class="form-control" placeholder="Tulis Deskripsi Singkat Artikel"></textarea>
                        </div>
                      </div>
                    </div>
                    <div style="clear:both;height:10px">&nbsp;</div>
                    <div class="form-group col-xs-12">
                      <label>Isi Artikel</label>
                      <div class="input-group" style="width: 100%;">
                        <textarea id="textareasemua" rows="10" placeholder="Tulis Template Anda di Sini" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                      </div>
                    </div>
                    <textarea name="template" id="template" style="display:none"></textarea>
                    <button type="button" class="btn btn-primary" onclick="simpanartikel()"> Simpan </button>

                  </form>
                </div>

              </div>


            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {

  var templatealignment = {
        textAlign: function(context) {
          return "<li><div class='btn-group'>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
              "<span class='glyphicon glyphicon-align-left'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
              "<span class='glyphicon glyphicon-align-center'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
              "<span class='glyphicon glyphicon-align-right'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='JustifyFull' data-wysihtml5-command-value='&JustifyFull;' title= 'Align text Justify'>" +
              "<span class='glyphicon glyphicon-align-justify'></span></a>" +
              "</li>";
        },
        pilihProduk: function(context) {
          return "<li>" +
          "<button type='button' class='btn btn-primary' onclick=\"modalpilihProduk()\" ><i class=\"fa fa-database\"></i></button>" +
          "<div class=\"modal fade\" id=\"modalpilihProduk\">" +
            "<div class=\"modal-dialog modal-md\">" +
              "<div class=\"modal-content\">" +
                "<div class=\"modal-header\">" +
                    "<a class=\"close\" data-dismiss=\"modal\">&times;</a>" +
                  "<h3>Tambah Produk</h3>" +
                "</div>" +
                "<div class=\"modal-body\">" +
                "<div id=\"listkomoditas\"></div>" +
                "<div style=\"clear:both\"></div>"+
                "<div id=\"pagingkomoditas\"></div>" +
                "</div>" +
                "<div class=\"modal-footer\">" +
                  "<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Tutup</button>" +
                "</div>" +
              "</div>" +
            "</div>" +
          "</div>" +
          "</li>";
        },
        pilihShortcode: function(context) {
          return "<li>" +
          "<button type='button' class='btn btn-primary' onclick=\"$('#modalpilihShortcode').modal();\" style='padding: 3px 12px;' >Shortcode</button>" +
          "<div class=\"modal fade\" id=\"modalpilihShortcode\">" +
            "<div class=\"modal-dialog modal-md\">" +
              "<div class=\"modal-content\">" +
                "<div class=\"modal-header\">" +
                    "<a class=\"close\" data-dismiss=\"modal\">&times;</a>" +
                  "<h3>Tambah Shortcode</h3>" +
                "</div>" +
                "<div class=\"modal-body\">" +
                "<?php $listshortcode = $artikelmdl->listkode();
                //data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='&hellip;'
                foreach ($listshortcode as $key => $value) {
                  if ($value==''){
                      echo "<h4>".$key."</h4>";
                  } else {
                    echo "<a data-wysihtml5-command='insertHTML' class='btn btn-link' data-wysihtml5-command-value='&#123;&#123;".$key."&#125;&#125;' data-dismiss='modal'>".$value."</a>";
                  }
                } ?><div style=\"clear:both\"></div>"+
                "</div>" +
                "<div class=\"modal-footer\">" +
                  "<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Tutup</button>" +
                "</div>" +
              "</div>" +
            "</div>" +
          "</div>" +
          "</li>";
        }
      };
  $('#textareasemua').wysihtml5({
    classes: {
      "wysiwyg-text-align-center": 1,
      "wysiwyg-text-align-justify": 1,
      "wysiwyg-text-align-left": 1,
      "wysiwyg-text-align-right": 1
    },
    "toolbar": {
      "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
      "emphasis": true, //Italics, bold, etc. Default true
      "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
      "html": false, //Button which allows you to edit the generated HTML. Default false
      "link": true, //Button to insert a link. Default true
      "image": true, //Button to insert an image. Default true,
      "color": true, //Button to change color of font
      "blockquote": false,
      'textAlign': true, // custom defined buttons to align text see myCustomTemplates variable above
      'pilihProduk': true,
      'pilihShortcode': true
    },
    stylesheets: ["<?=url('/')?>/css/bootstrap.min.css","<?=url('/')?>/css/artikel.css"],
    customTemplates: templatealignment
  });

  <?=$pilihartikel?>
});
var page=1; var perpage=12;
function modalpilihProduk() {
  $('#modalpilihProduk').modal();
  ajaxgetlistkomoditas();
}
function tutupbox() {
  if ($('#boxindex').is(':visible')){
    location.href='{{ url('/admin/pengaturan') }}';
  } else {
    $('#boxdetail').hide();
    $('#boxindex').show();
  }
}

function simpanartikel() {
  $('#template').val($('iframe').contents().find('.wysihtml5-editor').html());
  $('#formartikel').submit();
}

function ubahartikel(id_artikel){
  $('#boxindex').hide();
  $('#boxdetail').show();
  dataajaxartikel(id_artikel);
}

function dataajaxartikel(id_artikel) {
  $('iframe').contents().find('.wysihtml5-editor').html('<img style="width:100%" src="{{ url('/images/loader.gif') }}" height="100%" />');
  $('#imgeditartikel').attr('src','{{ url('/images/loader.gif') }}');
  $('#id').val(id_artikel);
  $('#judul').val($('.judul'+id_artikel).html());
  $('#id_kategori').val($('.id_kategori'+id_artikel).html());
  $('#overview').val($('.overview'+id_artikel).html());
  $.ajax({
      url: "{{ url('/api/getartikel') }}",
      type: 'POST',
      data: {
        'id' : id_artikel,
      },
      dataType: 'JSON',
      success: function (data) {
        console.log(data);
        var linkimage = '{{url("images/artikel") }}/'+id_artikel+'.jpg';
        if (!imageExists(linkimage)) linkimage='{{url("images/artikel") }}/default.jpg';
        $('#imgeditartikel').attr('src',linkimage);
        $('iframe').contents().find('.wysihtml5-editor').html(data);
        $('#boxdetail').show();$('#boxindex').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}

function hapusartikel(id_artikel) {
  if (confirm('Apakah kamu yakin ingin menghapus artikel ini ?')) {
    $.ajax({
        url: "{{ url('/api/hapusartikel') }}",
        type: 'POST',
        data: {
          'id' : id_artikel,
        },
        dataType: 'JSON',
        success: function (data) {
          if (data=='sukses') {
            location.reload();
          } else {
            alert(data);
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
        }
    });
  }
}

function ajaxgetlistkomoditas() {
  $.ajax({
      url: "{{ url('/api/listkomoditas') }}",
      type: 'POST',
      data: {
				'urutkan':$('#urutkan').val(),
				'tanggal':'{{ date("Y-m-d") }}',
				'id_parent':$('#id_komoditas_utama').val(),
        'nama':$('#filtercari').val(),
        'page':page,
        'perpage':perpage,
      },
      dataType: 'JSON',
      success: function (data) {
				console.log(data);
        var html = '';
        $.each(data,function(index,value) {
          var linkimage = '{{url("images/komoditas") }}/'+value["id"]+'.jpg';
          if (!imageExists(linkimage)) linkimage='{{url("images/komoditas") }}/default.jpg';

          var imghtml='<div class="col-xs-6 col-sm-4 col-md-3"><div [[kode]] style="margin-bottom: 15px;border: 1px solid #e0e0e0;cursor:pointer">'+
  								'<img id="komimage'+value["id"]+'" src="'+linkimage+'" alt=" " class="img-responsive" style="width:100%">'+
  								'<p id="komnama'+value["id"]+'" class="namaproduk">'+value["nama"]+'</p>'+
                  '<p id="komharga'+value["id"]+'" class="hargaproduk">Rp. '+value["harga"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'/'+value["satuan"]+'</p>'+
  							'</div></div>';
          // var imghtml2=imghtml.replace('[[kode]]','');
          // var imgcode = 'data-wysihtml5-command=\'insertHTML\' data-dismiss=\'modal\' data-wysihtml5-command-value=\'&#x3C;p&#x3E;'+he.encode(imghtml2)+'&#x3C;/p&#x3E;\'';
          var imgcode = 'data-wysihtml5-command=\'insertHTML\' data-dismiss=\'modal\' data-wysihtml5-command-value=\'&#123;&#123;produk-'+value["id"]+'&#125;&#125;\'';
          html+=imghtml.replace('[[kode]]',imgcode);
          console.log(imgcode);

        });
				html=(html=='')?'<p style="text-align: center;padding-top: 30px;">Komoditas Tidak Ditemukan</p>':html;
        $('#listkomoditas').html(html);
        var htmlpaging='';
        if (page>1) htmlpaging+='<button type="button" class="btn" onclick="page-=1;ajaxgetlistkomoditas()">&lt;&lt; Prev</button> &nbsp; &nbsp; ';
        if (data.length>=perpage) htmlpaging+='<button type="button"  class="btn" onclick="page+=1;ajaxgetlistkomoditas()"> Next &gt;&gt;</button>';
        $('#pagingkomoditas').html(htmlpaging);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
