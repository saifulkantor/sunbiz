@extends('layouts.app')

@section('content')
<?php $id_kota=isset($_GET['id_kota'])?$_GET['id_kota']:0; $pilihkota=''; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkotabox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Kota</h3>
                </div>
                <div class="box-body">
                  <button type="button" class="btn btn-primary" onclick="tambah()" style="margin-bottom:20px"><i class="fa fa-save"></i> Tambah </button>
                  <table id="tabelkota" class="table table-bordered table-striped">
                    <thead>
                      <tr><th>No.</th><th>Nama Kota</th><th>Provinsi</th></tr>
                    </thead>
                  <?php foreach ($kota as $key=>$kt) {
                    echo '<tr><td>'.($key+1).'</td><td><p class="btn btn-link" onclick="pilihkota('.$kt->id.',\''.$kt->nama.'\')">'.$kt->jenis.' '.$kt->nama.'</p></td><td>'.$kt->nama_provinsi.'</td></tr>';
                  } ?>
                  </table>
                </div>

              </div>

              <!-- List kota -->
              <div id="isikotabox" class="box box-default" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Kota</h3>

                  <div class="box-tools pull-right" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="bukakota()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-6">
                      <input type="hidden" class="form-control" id="id_kota" name="id_kota" />
                      <div class="form-group">
                        <label for="jenis">Kota / Kabupaten</label>
                        <input type="text" class="form-control" id="jenis" name="jenis" placeholder="Kota / Kabupaten" />
                      </div>
                      <div class="form-group">
                        <label for="name">Nama Kota</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Kota" />
                      </div>
                      <div class="form-group">
                        <label for="id_provinsi">Provinsi</label>
                        <select id="id_provinsi" class="form-control" name="id_provinsi">
                          <?php foreach ($provinsi as $key => $value) {
                            echo '<option value="'.$value->id.'">'.$value->nama.'</option>';
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" onclick="simpandata()"><i class="fa fa-save"></i> Simpan </button>
                  </div>
                </div>

            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => { $('#tabelkota').dataTable(); <?=$pilihkota?> });
function bukakota() {
  $('#isikotabox').hide();
  $('#tabelkotabox').show();
}
function clearform() {
  $('#isikotabox input.form-control').val('');
}
function tambah() {
  clearform();
  $('#tabelkotabox').hide();
  $('#isikotabox').show();
  $('#isikotabox .box-title').html('Data Kota ');
}
function pilihkota(id_kota,nama_kota) {
  clearform();
  $('#tabelkotabox').hide();
  $('#isikotabox').show();
  $('#isikotabox #id_kota').val(id_kota);
  $('#isikotabox .box-title').html('Data Kota '+nama_kota);
  getdata();
}
function simpandata() {
  $.ajax({
      url: "{{ url('/api/kota') }}",
      type: 'POST',
      data: $('#isikotabox .form-control').serialize(),
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkota') }}",
      type: 'POST',
      data: {
        'id_kota':$('#isikotabox #id_kota').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#jenis').val(value['jenis']); $('#nama').val(value['nama']); $('#id_provinsi').val(value['id_provinsi']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
