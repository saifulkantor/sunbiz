@extends('layouts.app')

@section('content')
<?php $id_komoditas=isset($_GET['id_komoditas'])?$_GET['id_komoditas']:0; $pilihkomoditas='';?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelkomoditasbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Komoditas</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <img id="imgkom0" style="display:none" src="<?=url('/images/komoditas/default.jpg')?>" />
                  <button class="btn btn-primary" onclick="bukaeditkomoditas(0,'','kg',0,1,null)">Tambah Grup Komoditas</button>
                  <table id="tabelkomoditas" class="table table-condensed table-striped">
                    <input type="hidden" class="id_kota" name="id_kota" value="0" />
                    <input type="hidden" class="id_user" name="id_user" value="{{ Auth::user()->id }}" />
                    <thead>
                      <tr><th>Nama Komoditas</th><th>Harga</th><th>Satuan</th><th>Stok</th></tr>
                    </thead>
                  <tbody class="row_position">
                  <?php $kom = $komoditas;
                  $komparent=array();
                  foreach ($komoditas as $index=> $k) {
                      if(file_exists(public_path('images/komoditas/'.$k->id.'.jpg'))){
          						    $image = url('/images/komoditas/'.$k->id.'.jpg');
          						}else{
          						    $image = url('/images/komoditas/default.jpg');
          						}
                      $anak = array_filter($kom, function ($e) use ($k) { return $e->id_parent == $k->id; } );
                      $no = ($k->id_parent==null)?'':'&nbsp; &nbsp;';
                      $no .= ($k->id_parent==null)?(count($komparent)+1).'. ':'&nbsp; &nbsp; ';
                      $no .= '<img onclick="bukaeditkomoditas('.$k->id.',\''.$k->nama.'\',\''.$k->satuan.'\',\''.$k->harga.'\',\''.$k->stok.'\','.$k->id_parent.')" id="imgkom'.$k->id.'" src="'.$image.'" style="width:20px;height:20px" /> &nbsp; ';
                      $nama ='<b onclick="bukaeditkomoditas('.$k->id.',\''.$k->nama.'\',\''.$k->satuan.'\',\''.$k->harga.'\',\''.$k->stok.'\','.$k->id_parent.')">'.$no.$k->nama.'</b>';
                      $nama .= ($anak!=null)?'<b onclick="togglekomoditas(\'kom'.$k->id.'\')" style="cursor:pointer"> <i class="fa fa-caret-down"></i></b>':'';
                      $hss = ($k->id_parent==null)?'<td></td><td></td><td></td>':'<td>Rp. '.number_format($k->harga,0,',','.').'</td><td>'.$k->satuan.'</td><td>'.$k->stok.'</td>';
                      if ($k->id_parent==null) {
                        $komparent[]=$k;
                        $htmlparent[$k->id]='<tr class="kom'.$k->id_parent.'" style="cursor:pointer" urutan="'.($index+1).'"><td>'.$nama.'</td>'.$hss.'</tr>';
                        $htmlparent[$k->id].='<tr class="kom'.$k->id.'" style="cursor:pointer;display:none"><td colspan="9"><b onclick="bukaeditkomoditas(0,\'\',\'kg\',0,1,'.$k->id.');" style="cursor:pointer;color:blue"> &nbsp; &nbsp;&nbsp; &nbsp; <i class="fa fa-plus"></i> Komoditas Baru</b></td></tr>';
                      } else {
                        $htmlanak[$k->id_parent]=isset($htmlanak[$k->id_parent])?$htmlanak[$k->id_parent]:'';
                        $htmlanak[$k->id_parent].='<tr class="kom'.$k->id_parent.'" style="cursor:pointer;display:none" urutan="'.($index+1).'"><td>'.$nama.'</td>'.$hss.'</tr>';
                      }
                    }
                    foreach ($komparent as $key => $value) {
                      echo $htmlparent[$value->id];
                      echo isset($htmlanak[$value->id])?$htmlanak[$value->id]:'';
                    }
                    ?>
                  </tbody>
                  </table>
                </div>

              </div>

              <!-- Modal -->
              <div class="modal modal-success fade" id="editkomoditas">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Edit Komoditas</h4>
                      </div>
                      <div class="modal-body">
                        <form id="formdokumenkomoditas" method="post" enctype="multipart/form-data"  action="">
                          <div class="col-xs-12 col-sm-6">
                            <input type="file" class="inputimage" style="display:none" data-name="imgk" name="inputimgk" id="inputimgk" accept="image/*">
                            <img id="imgeditkomoditas" class="previewimageimgk" src="" style="width:100%;max-height: 300px;cursor:pointer;" onclick="$('#inputimgk').click()" />
                          </div>
                          <div class="col-xs-12 col-sm-6">
                            <input type="hidden" name="id_user" value="{{ auth()->user()->id }}" />
                            <input type="hidden" class="form-control" id="id" name="id" />
                            <div class="form-group untukanak">
                              <label for="name">Kelompok Komoditas</label>
                              <select class="form-control" id="id_parent" name="id_parent" >
                                <?php foreach ($komparent as $key => $value) {
                                  echo '<option value="'.$value->id.'">'.$value->nama.'</option>';
                                } ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="name">Nama Komoditas</label>
                              <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Komoditas" />
                            </div>
                            <div class="form-group untukanak">
                              <label for="name">Harga</label>
                              <div class="input-group">
                                <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga" min="0" /><span id="satuantampil" class="input-group-addon">/</span>
                              </div>
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly name="tanggal" class="form-control pull-right" id="tanggal" style="cursor:pointer">
                              </div>
                            </div>
                            <div class="row untukanak">
                              <div class="form-group col-xs-6">
                                <label for="name">Stok</label>
                                <input type="number" class="form-control" id="stok" name="stok" placeholder="Stok" min="0" />
                              </div>
                              <div class="form-group col-xs-6">
                                <label for="name">Satuan</label>
                                <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" onkeyup="gantisatuan()" />
                              </div>
                            </div>
                            <button type="button" class="btn btn-outline" onclick="simpandata()">Simpan</button>
                          </div>
                          <div style="clear:both"></div>
                        </form>
                      </div>
                      <div class="modal-footer">
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>


            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {
  var tgl = $('#tanggal').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true
  });
  tgl.datepicker('setDate', new Date());
  $(".row_position").sortable
        ({
            scroll: false,
            sort: function (event, ui) {
                ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
            },
            helper: 'clone',
            axis: 'y',
            update: function () {
              var selectedData = new Array();
              $('.row_position>tr').each(function() {
                  selectedData.push($(this).attr("urutan"));
              });
              updateOrder(selectedData);
            }
        }).disableSelection();
  <?=$pilihkomoditas?>
});

function bukaeditkomoditas(id_komoditas,nama_komoditas,satuan,harga,stok,id_parent) {
  if (id_komoditas==0) $('#editkomoditas .modal-title').html('Tambah Komoditas'); else $('#editkomoditas .modal-title').html('Edit Komoditas');
  $('#imgeditkomoditas').attr('src',$('#imgkom'+id_komoditas).attr('src'));
  $('#id_parent').val(id_parent);
  $('#id').val(id_komoditas);
  $('#nama').val(nama_komoditas);
  $('#satuan').val(satuan);
  $('#harga').val(harga);
  $('#stok').val(stok);
  gantisatuan();
  if (id_parent==null) $('.untukanak').hide(); else $('.untukanak').show();
  $('#editkomoditas').modal();
}
function gantisatuan() {
  $('#satuantampil').html('/ '+$('#satuan').val());
}
function updateOrder(data) {
    console.log(data);
}
function togglekomoditas(kelas){
  $('.'+kelas).fadeToggle();
}
function bukakomoditas() {
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
}
function clearform() {
  $('#tabelkomoditas input.form-control').val(0);
}
function pilihkomoditas(id_komoditas,nama_komoditas) {
  clearform();
  $('#tabelkomoditasbox').hide();
  $('#tabelkomoditasbox').show();
  $('#tabelkomoditasbox .id_komoditas').val(id_komoditas);
  $('#tabelkomoditasbox .tanggal').val($('#tabelkomoditasbox .tanggal').val());
  $('#tabelkomoditasbox .box-title').html('List Komoditas '+nama_komoditas);
  getdata();
}
function simpandata() {
  var formData = new FormData(document.getElementById('formdokumenkomoditas'));
  $.ajax({
      url: "{{ url('/api/komoditas') }}",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'JSON',
      success: function (data) { location.reload(); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getkomoditas_data') }}",
      type: 'POST',
      data: {
        'tanggal':$('#tabelkomoditasbox .tanggal').val(),
        'id_komoditas':$('#tabelkomoditasbox .id_komoditas').val()
      },
      dataType: 'JSON',
      success: function (data) { console.log(data); $.each(data, function( index, value ) { $('#hargakomoditas'+value['id_komoditas']).val(value['harga']); $('#stokkomoditas'+value['id_komoditas']).val(value['stok']); }); },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
