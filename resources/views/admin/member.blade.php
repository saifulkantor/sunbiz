@extends('layouts.app')

@section('content')
<?php $id_member=isset($_GET['id_member'])?$_GET['id_member']:0; $pilihmember='';
$alamattersimpan=[]; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div id="tabelmemberbox" class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">List Member</h3>
                </div>
                <div class="box-body">
                  <button type="button" class="btn btn-primary" onclick="tambah()"><i class="fa fa-save"></i> Tambah </button>
                  <table id="tabelmember" class="table">
                    <thead>
                      <tr><th>No.</th><th>Nama Member</th><th>Alamat</th></tr>
                    </thead>
                  <?php
                  foreach ($member as $key=>$con) {
                    echo '<tr><td>'.($key+1).'</td><td><p class="btn btn-link" onclick="pilihmember('.$con->id_user.',\''.$con->name.'\')">'.$con->name.'</p></td><td>'.$con->alamat.'</td></tr>';
                  } ?>
                  </table>
                </div>

              </div>

              <!-- List member -->
              <div id="isimemberbox" class="box box-default" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Member</h3>

                  <div class="box-tools pull-right" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-danger btn-sm" onclick="bukamember()"><i class="fa fa-close"></i></button>
                    </div>
                  </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                      <input type="hidden" class="form-control" id="id_member" name="id_user" />
                      <input type="hidden" class="form-control" id="level" name="level" value="member" />
                      <div class="form-group col-md-6">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Isi Nama Member" />
                      </div>
                      <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Isi Email Member" />
                      </div>
                      <div class="form-group col-md-6">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Isi Password Member" />
                      </div>
                      <div class="form-group col-md-6">
                        <label for="ulangipassword">Ulangi Password</label>
                        <input type="password" class="form-control" id="ulangipassword" name="ulangipassword" placeholder="Ulangi Password" />
                      </div>
                      <div class="form-group col-md-12">
                        <label for="alamat">Alamat</label>
                        <input type="hidden" class="form-control" id="hapusalamat" name="hapusalamat" value="" />
                        <table id="listalamat" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Judul</th>
                              <th>Alamat</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                        <p style="color:blue;cursor:pointer" onclick="tambahalamat()"> <i class="fa fa-plus"></i> Tambah Alamat</p>
                      </div>
                    </div>

                    <div class="box-footer">
                      <button type="button" class="btn btn-primary" onclick="simpandata()"><i class="fa fa-save"></i> Simpan </button>
                    </div>

                  </div>
                </div>

            </div>
        </div>

    <!-- Modal -->
    <div class="modal modal-success fade" id="popupalamat">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Edit Alamat</h4>
            </div>
            <div class="modal-body">
                <div id="isialamat" class="table-striped">
                  <input type="hidden" class="form-control" id="id_alm" name="id_alm" value="">
                  <input type="hidden" class="form-control" id="utama" name="utama" value="0">
                  <div class="form-group col-xs-12">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Alamat Warehouse A / Alamat Kantor B" />
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Alamat</label>
                    <textarea class="form-control" rows="5" id="alamat" name="alamat"></textarea>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Telp</label>
                    <input type="text" class="form-control" id="telp" name="telp" placeholder="+62 ..........." />
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Provinsi</label>
                    <select id="id_provinsi" class="form-control" name="id_provinsi" onclick="loadkota()">
                      <?php foreach ($provinsi as $key => $value) {
                        echo '<option value="'.$value->id.'">'.$value->nama.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Kota</label>
                    <select id="id_kota" class="form-control" name="id_kota">
                    </select>
                  </div>
                  <div class="form-group col-xs-12 col-sm-6 col-md-4">
                    <label for="judul">Kodepos</label>
                    <input type="number" class="form-control" id="kodepos" name="kodepos" placeholder="Kode Pos" />
                  </div>
                  <div style="clear:both"></div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="simpanalamat()"><i class="fa fa-save"></i> Simpan </button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>





@endsection

<script type="text/javascript">
var kota = <?=json_encode($kota)?>;
window.addEventListener('DOMContentLoaded', (event) => { <?=$pilihmember?> });
var counter=0;
function tambahalamat() {
  $('#popupalamat').modal();
  $('#popupalamat input.form-control,#popupalamat textarea.form-control').val('');
  if (counter<=0) $('#utama').val(1); else $('#utama').val(0);
}
function ubahalamat(id) {
  tambahalamat();
  $('#tralamat'+id+' input').map(function(){
    $('#popupalamat .form-control#'+$(this).attr('data-key')).val($(this).val()).trigger('click');
  });
  $('#popupalamat .form-control#id_alm').val(id);
}
function hapusalamat(id) {
  if (confirm('Apakah kamu yakin ingin menghapus "'+$('#jdl'+id).html()+'" ?')){
    if (typeof $('#id'+id).val() !== "undefined") {
      var hapusalamat=($('#hapusalamat').val()=='')?$('#id'+id).val():$('#hapusalamat').val()+','+$('#id'+id).val();
      $('#hapusalamat').val(hapusalamat);
    }
    $('#tralamat'+id).remove();
  }
}

function simpanalamat() {
  if ($('#judul').val()!=''&&$('#alamat').val()!=''&&$('#alamat').val()!=''&&$('#telp').val()!=''&&$('#id_kota').val()!=''&&$('#kodepos').val()!='') {
    $('#popupalamat').modal('hide');
    masukkandata();
  } else {
    alert('Mohon masukkan semua data dengan benar');
  }
}

function masukkandata() {
  var values = {};
  $("#popupalamat .form-control").map(function(){
    values[$(this).attr('name')]=$(this).val();
  });
  loadalamat(values);
}

function loadalamat(datas) {
  var htmlhid='';
  if (datas['id_alm']=='') {
    counter++;
    datas['id_alm']=counter;
    $.each(datas, function (key, value){
      htmlhid+='<input type="hidden" id="'+key+counter+'" class="form-control" data-key="'+key+'" name="'+key+'['+counter+']" value="'+value+'" />';
    });
    var btn = (counter>1)?'<i class="fa fa-pencil" style="cursor:pointer;" onclick="ubahalamat('+counter+')"></i> &nbsp; <i class="fa fa-trash" style="cursor:pointer;" onclick="hapusalamat('+counter+')"></i> ':'<i class="fa fa-pencil" style="cursor:pointer;" onclick="ubahalamat('+counter+')"></i>';
    $('#listalamat tbody').append('<tr id="tralamat'+counter+'"><td>'+htmlhid+'<span id="jdl'+counter+'">'+datas['judul']+'</span></td><td id="alm'+counter+'">'+datas['alamat']+'</td><td>'+btn+'</td></tr>');
  } else {
    console.log(datas);
    $.each(datas, function (key, value){
      $('#'+key+datas['id_alm']).val(value);
    });
    $('#jdl'+datas['id_alm']).html(datas['judul']);
    $('#alm'+datas['id_alm']).html(datas['alamat']);
  }
}

function loadkota() {
  var id_provinsi=$('#id_provinsi').val();
  var kotaoption='';
  $.each(kota,function(index, value){
    if (value['id_provinsi']==id_provinsi) {
      kotaoption+='<option value="'+value['id']+'">'+value['jenis']+' '+value['nama']+'</option>';
    }
  });
  $('#id_kota').html(kotaoption);
}
function bukamember() {
  $('#isimemberbox').hide();
  $('#tabelmemberbox').show();
  $('#tabelmemberbox input.form-control,#tabelmemberbox textarea.form-control').val('');
}
function clearform() {
  $('#popupalamat input.form-control,#popupalamat textarea.form-control').val('');
  counter=0;
  loadkota();
}
function tambah() {
  clearform();
  $('#tabelmemberbox').hide();
  $('#isimemberbox').show();
  $('#isimemberbox .box-title').html('Data Member ');
}
function pilihmember(id_member,nama_member) {
  clearform();
  $('#tabelmemberbox').hide();
  $('#isimemberbox').show();
  $('#isimemberbox #id_member').val(id_member);
  $('#isimemberbox .box-title').html('Data Member '+nama_member);
  getdata();
}
function simpandata() {
  if ($('#password').val()==$('#ulangipassword').val()) {
    $.ajax({
        url: "{{ url('/api/member') }}",
        type: 'POST',
        data: $('#isimemberbox .form-control').serialize(),
        dataType: 'JSON',
        success: function (data) { if (data=='sukses') location.reload(); else alert('Terjadi Kesalahan Mohon ulangi kembali'); },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
        }
    });
  } else {
    alert('Password dan Ulangi Password Tidak Sama');
  }
}
function getdata() {
  $.ajax({
      url: "{{ url('/api/getmember') }}",
      type: 'POST',
      data: {
        'id_user':$('#isimemberbox #id_member').val()
      },
      dataType: 'JSON',
      success: function (datas) {
        clearform();
        $.each(datas, function( index, value ) {
          $('#name').val(value['name']); $('#email').val(value['email']);
          var alamat = {
            'id_alm':'',
            'id_provinsi':value['id_provinsi'],
            'id':value['id'],
            'judul':value['judul'],
            'alamat':value['alamat'],
            'kodepos':value['kodepos'],
            'telp':value['telp'],
            'utama':value['utama'],
            'id_kota':value['id_kota']
          };
          loadalamat(alamat);
        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus+ "\n" + "Error: " + errorThrown);
      }
  });
}
</script>
