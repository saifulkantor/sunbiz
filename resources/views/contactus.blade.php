@extends('layouts.home')

@section('content')
<?php
$namawebsite = explode("||",$pengaturan[1]->data);
$detailwebsite = $pengaturan[2]->data;
$contact = $pengaturan[3]->data;
$email = $pengaturan[4]->data;
$address = $pengaturan[5]->data;
$sosmed = explode("||",$pengaturan[6]->data);
?>
<div class="breadcrumbs">
	<div class="container">
		<div class="w3layouts_breadcrumbs_left">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a><span>/</span></li>
				<li><i class="fa fa-phone" aria-hidden="true"></i>Contact Us</li>
			</ul>
		</div>
		<div class="w3layouts_breadcrumbs_right">
			<h2>Contact Us</h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="welcome">
  <div id="listtransaksi" class="container">
    <h3 class="agileits_w3layouts_head">Contact <span>Us</span></h3>
    <div class="w3_agile_image">
      <img src="{{ url('/template/mygarden/images/1.png') }}" alt=" " class="img-responsive">
    </div>
    <p class="agile_para"></p>

		<div class="w3ls_news_grids">
			<div class="col-md-8 w3_agile_mail_left">
				<div class="agileits_mail_grid_right1 agile_mail_grid_right1">
					<form action="" method="post">
						@csrf
						<span>
							<i>Nama</i>
							<input type="text" name="nama" placeholder=" " required="">
						</span>
						<span>
							<i>Email</i>
							<input type="email" name="email" placeholder=" " required="">
						</span>
						<span>
							<i>Pesan</i>
							<textarea name="message" placeholder=" " required=""></textarea>
						</span>
						<div class="w3_submit">
							<input type="submit" value="Submit Now">
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4 w3_agile_mail_right">
				<div class="w3_agileits_mail_right_grid" style="margin-top:0">
					<h4>About Us</h4>
					<p style="white-space: pre-line"><?=$detailwebsite?></p>
					<h5>Follow Us On</h5>
					<ul class="agileits_social_list">
						<li><a href="https://www.facebook.com/<?=$sosmed[0]?>" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/<?=$sosmed[1]?>" class="agile_twitter"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="https://www.twitter.com/<?=$sosmed[2]?>" class="w3_agile_dribble"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
					<!-- <div class="w3_agileits_mail_right_grid_pos">
						<img src="images/12.jpg" alt=" " class="img-responsive" />
					</div> -->
				</div>
				<div class="w3_agileits_mail_right_grid_main">
					<div class="w3layouts_mail_grid_left">
						<div class="w3layouts_mail_grid_left1 hvr-radial-out">
							<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						</div>
						<div class="w3layouts_mail_grid_left2">
							<h3>Mail Us</h3>
							<p style="white-space: pre-line"><?=$email?></p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="w3layouts_mail_grid_left">
						<div class="w3layouts_mail_grid_left1 hvr-radial-out">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						</div>
						<div class="w3layouts_mail_grid_left2">
							<h3>Address</h3>
							<p style="white-space: pre-line"><?=$address?></p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="w3layouts_mail_grid_left">
						<div class="w3layouts_mail_grid_left1 hvr-radial-out">
							<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
						</div>
						<div class="w3layouts_mail_grid_left2">
							<h3>Phone</h3>
							<p style="white-space: pre-line"><?=$contact?></p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>

  </div>
</div>


@endsection
