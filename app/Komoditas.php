<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\KomoditasData;

class Komoditas extends Model
{
    protected $table = 'komoditas';
    protected $fillable = ['nama', 'satuan', 'urutan', 'id_parent'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25,$sql=false) {
      $query = 'SELECT * FROM (SELECT k.*,SUBSTRING_INDEX(GROUP_CONCAT(kd.harga ORDER BY kd.tanggal DESC),",",1) as harga,MAX(kd.tanggal) FROM komoditas k LEFT OUTER JOIN komoditas_data kd ON k.id=kd.id_komoditas';
      $urutkan='';
      if ($filters) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
        $filter=htmlspecialchars($filter);
          if ($key=='nama')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else if ($key=='tanggal')
            $query .= ' '.$key.' <= "'.$filter.'" AND';
          else if ($key=='urutkan')
            $urutkan=($filter!='')?'ORDER BY '.$filter:'';
          else
            $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      $query .= ' GROUP BY k.id, k.nama,k.satuan,k.urutan,k.id_parent,k.stok ORDER BY k.id, k.id_parent, kd.tanggal DESC) tbl '.$urutkan;
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      if ($sql){
        return $query;
      } else {
        $datas = DB::select($query);
        return $datas;
      }
    }

    function getutamajml() {
      $query = 'SELECT k.id,k.nama,k.stok, COUNT(k.id) as jumlahanak FROM komoditas k LEFT OUTER JOIN komoditas k2 ON k.id=k2.id_parent WHERE k.id_parent IS NULL GROUP BY k.id,k.nama,k.stok';
      $datas = DB::select($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          if ($key!='tanggal' && $key!='harga' && $key!='id_user') {
            $val .= $key.',';
            $query .= ($data==null)?'NULL,':'"'.$data.'",';
          }
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO komoditas '.$val.' VALUES '.$query;
        // return $query;
        //$datas = DB::select($query);
        $tanggal = $datas['tanggal']; unset($datas['tanggal']);
        $harga = $datas['harga']; unset($datas['harga']);
        $id_user= $datas['id_user']; unset($datas['id_user']);
        $id = DB::table('komoditas')->insertGetId($datas);
        $datas["id_kota"]=444;
        $datakom = new KomoditasData();
        $datakom->createone(['id_komoditas'=>$id,'harga'=>$harga,'tanggal'=>$tanggal,'id_kota'=>$datas["id_kota"],'id_user'=>$id_user]);
        return $id;
      }
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE komoditas SET ';
        foreach ($datas as $key => $data) {
          if ($key!='tanggal' && $key!='harga' && $key!='id_user' && $data!=NULL) {
            $query .= ' '.$key.'="'.$data.'",';
          }
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $key = ($key=='id_komoditas')?'id':$key;
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        if (isset($datas['tanggal'])) {
          $datas["id_kota"]=444;
          $datakom=KomoditasData::where(['tanggal'=>$datas['tanggal'],'id_komoditas'=>$wheres['id']])->first();
          if ($datakom) {
            $datakom = new KomoditasData();
            $datakom->createone(['id_komoditas'=>$wheres["id"],'harga'=>$datas["harga"],'tanggal'=>$datas["tanggal"],'id_kota'=>$datas["id_kota"],'id_user'=>$datas["id_user"]]);
          } else if ($datas["harga"]!='') {
            $datakom = new KomoditasData();
            $datakom->createone(['id_komoditas'=>$wheres["id"],'harga'=>$datas["harga"],'tanggal'=>$datas["tanggal"],'id_kota'=>$datas["id_kota"],'id_user'=>$datas["id_user"]]);
          }
        }

        // return $query;
        $datas = DB::update($query);
        return $datas;
      } else {
        return '';
      }
    }

}
