<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'komoditas';
    protected $fillable = ['id_user', 'id_komoditas', 'jumlah', 'expired_date'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT * FROM keranjang';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      // $query = 'SELECT * FROM komoditas ORDER BY urutan';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE keranjang SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::update($query);
        return $datas;
      } else {
        return '';
      }
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO keranjang '.$val.' VALUES '.$query;
        // return $query;
        $datas = DB::insert($query);
        return $datas;
      }
    }

    function deleteone($wheres=null) {
      if ($wheres!=null) {
        $query = 'DELETE FROM keranjang WHERE ';
        foreach ($wheres as $key => $where) {
          $query .= ' '.$key.'="'.$where.'" AND';
        }
        $query=substr($query, 0, -3);
        $query.=';';
        // return $query;
        $datas = DB::delete($query);
        return $datas;
      }
    }

}
