<?php
namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contactus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $request;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(request $request,$subject)
    {
        $this->request = $request;
        $this->subject=$subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/contactus')->with([
                        'request' => $this->request,
                    ])->from($this->request->email,'Sistem')->replyTo($this->request->email,$this->request->nama)->subject($this->subject);
    }
}
