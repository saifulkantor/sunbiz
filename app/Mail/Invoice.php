<?php
namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $pembeli;
    public $emailconf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pembeli,$emailconf)
    {
        $this->pembeli = $pembeli;
        $this->emailconf = $emailconf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/invoice')->with([
                        'template' => $this->emailconf['template'],
                    ])->from($this->emailconf['email'],'Admin')->replyTo($this->emailconf['email'],'Admin')->subject($this->emailconf['subject']);
    }
}
