<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{
    protected $table = 'alamat';
    protected $fillable = ['judul','alamat','id_kota','kodepos','telp','id_user','utama'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT a.*,u.name as nama_user,u.email FROM alamat a LEFT OUTER JOIN user u ON a.id_user=u.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_alamat')?'k.id':$key;
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function hapusdata($wheres=null) {
      $query = 'DELETE FROM alamat';
      if ($wheres!=null) {
        $query .= ' WHERE ';
        foreach ($wheres as $key => $where) {
          $key = ($key=='id_alamat')?'id':$key;
          $query .= ' '.$key.'="'.$where.'" AND';
        }
        $query=substr($query, 0, -3); $query.=';';
      }
      $datas = DB::delete($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO alamat '.$val.' VALUES '.$query;
        // return $query;
        $datas = DB::insert($query);
        return $datas;
      }
    }

}
