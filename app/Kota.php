<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'kota';
    protected $fillable = ['id', 'jenis', 'nama', 'id_provinsi', 'kodepos'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT k.*,p.nama as nama_provinsi FROM kota k LEFT OUTER JOIN provinsi p ON k.id_provinsi=p.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_kota')?'k.id':$key;
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $key = ($key=='id_kota')?'id':$key;
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO kota '.$val.' VALUES '.$query;
        // return $query;
        $datas = DB::select($query);
        return $datas;
      }
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE kota SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $key = ($key=='id_kota')?'id':$key;
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::select($query);
        return $datas;
      } else {
        return '';
      }
    }

}
