<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['id_user','kode','tanggal','alamat','metode_pembayaran','ongkir','diskon','total','status','catatan'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT t.*,u.name as nama,u.email FROM transaksi t LEFT OUTER JOIN users u ON t.id_user=u.id';
      if ($filters!=null) {
        $query .= ' WHERE 1=1 AND';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_transaksi')?'id':$key;
          if ($key=='nama')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else if ($key=='status')
            $query .= ($filter=='all')?'':' '.$key.'="'.$filter.'" AND';
          else
            $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      // $query = 'SELECT * FROM transaksi ORDER BY urutan';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function lengkap($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT t.id as id_transaksi, t.kode as kode_transaksi,t.id_user,t.tanggal as tanggal_transaksi,u.email, t.alamat as alamat_pembeli,t.metode_pembayaran,t.ongkir,t.diskon,t.total,t.status,t.catatan,
      u.name as nama_pembeli FROM transaksi t LEFT OUTER JOIN users u ON t.id_user=u.id';
      if ($filters!=null) {
        $query .= ' WHERE 1=1 AND';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_transaksi')?'id':$key;
          if ($key=='nama')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else if ($key=='status')
            $query .= ($filter=='all')?'':' '.$key.'="'.$filter.'" AND';
          else
            $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      // $query = 'SELECT * FROM transaksi ORDER BY urutan';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE transaksi SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $key = ($key=='id_transaksi')?'id':$key;
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::update($query);
        return $datas;
      } else {
        return '';
      }
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO transaksi '.$val.' VALUES '.$query;
        // return $query;
        //$datas = DB::select($query);
        $datas = DB::table('transaksi')->insertGetId($datas);
        return $datas;
      }
    }

    public function getlabelstatus($status){
      $html='';
      switch ($status) {
        case 'pendingadmin':
          $html.='<span class="label label-lg label-warning pull-right">Pending Admin</span>';
          break;
        case 'rejected':
          $html.='<span class="label label-lg label-warning pull-right">Ditolak</span>';
          break;
        default:
          $html.='<span class="label label-lg label-success pull-right">'.$status.'</span>';
          break;
      }
      return $html;
    }

    public function kodereplace($format,$datatambahan){
      $koder = [
        'tahun'=>date('Y'),
        'bulan'=>date('m'),
        'tanggal'=>date('d'),
      ];
      foreach ($koder as $key => $value) {
        $format=str_replace('[['.$key.']]',$value,$format);
      }
      foreach ($datatambahan as $key => $value) {
        $format=str_replace('[['.$key.']]',$value,$format);
      }
      return $format;
    }

    public function tabel_detail_transaksi($datas,$transaksi) {
      $teks = '<table class="table table-condensed table-striped table-bordered">
      <thead><tr><th>No.</th><th>Nama Komoditas</th><th>Harga</th><th>Jumlah</th><th>Total</th></tr></thead>
      <tbody>';
      $total=0;
      foreach ($datas as $key => $value) {
        $teks .='<tr><td style="text-align:center">'.($key+1).'</td><td>'.$value->nama.'</td><td style="text-align:right">Rp. '.number_format($value->harga,0,',','.').'</td><td style="text-align:center">'.$value->jumlah.'</td><td style="text-align:right">Rp. '.number_format(($value->harga*$value->jumlah),0,',','.').'</td></tr>';
        $total+=($value->harga*$value->jumlah);
      }
      $teks .='<tr><td colspan="4" style="text-align:right">SUBTOTAL</td><td style="text-align:right">Rp. '.number_format($total,0,',','.').'</td></tr>
      <tr><td colspan="4" style="text-align:right">ONGKIR</td><td style="text-align:right">Rp. '.number_format($transaksi->ongkir,0,',','.').'</td></tr>
      <tr><td colspan="4" style="text-align:right">TOTAL</td><td style="text-align:right">Rp. '.number_format($transaksi->total,0,',','.').'</td></tr>';
      $teks .= '</tbody></table>';
      return $teks;
    }

}
