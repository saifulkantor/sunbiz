<?php

namespace App;

use App\Komoditas;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function getkeranjang() {
      $komoditas=new Komoditas();
      $query = 'SELECT * FROM keranjang k LEFT OUTER JOIN ('.$komoditas->semua(null,true,0,0,true).') kd ON k.id_komoditas=kd.id WHERE k.id_user = ?';
      $datas = DB::select($query, [$this->id]);
      return $datas;
    }

    function kota($lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT k.*,p.nama as nama_provinsi FROM
                  user_kota uk LEFT OUTER JOIN
                  kota k ON uk.id_kota=k.id LEFT OUTER JOIN
                  provinsi p ON k.id_provinsi=p.id
                WHERE uk.id_user = ?';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query, [$this->id]);
      return $datas;
    }

    function pasar($lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT CONCAT(k.jenis, \' \', k.nama) as nama_kota, p.* FROM
                  user_kota uk LEFT OUTER JOIN
                  kota k ON uk.id_kota=k.id LEFT OUTER JOIN
                  pasar p ON p.id_kota=k.id
                WHERE uk.id_user = ?';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query, [$this->id]);
      return $datas;
    }

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT * FROM users';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function semuawithkota($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT u.id,u.name,u.username,GROUP_CONCAT(k.nama) as nama_kota FROM users u LEFT OUTER JOIN user_kota uk ON u.id=uk.id_user LEFT OUTER JOIN kota k ON uk.id_kota=k.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      $query.=' GROUP BY u.id,u.name,u.username';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function semuawithalamat($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT u.*,a.*,k.id_provinsi FROM users u LEFT OUTER JOIN alamat a ON u.id=a.id_user LEFT OUTER JOIN kota k ON a.id_kota=k.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      $query.=' ORDER BY a.utama DESC';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    // function semuawithalamat($filters=null,$lengkap=true,$page=1,$limit=25) {
    //   $query = 'SELECT u.id,u.email,u.name,u.username,a.alamat,a.judul,a.utama FROM users u LEFT OUTER JOIN alamat a ON u.id=a.id_user';
    //   if ($filters!=null) {
    //     $query .= ' WHERE ';
    //     foreach ($filters as $key => $filter) {
    //       $query .= ' '.$key.'="'.$filter.'" AND';
    //     }
    //     $query=substr($query, 0, -3);
    //   }
    //   $query.=' GROUP BY u.id,u.email,u.name,u.username,a.alamat,a.judul,a.utama ORDER BY a.utama DESC';
    //   if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
    //   $datas = DB::select($query);
    //   return $datas;
    // }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO users '.$val.' VALUES '.$query;
        // return $query;
        //$datas = DB::select($query);
        $datas = DB::table('users')->insertGetId($datas);
        return $datas;
      }
    }

    function updateone($data) {
      $password = ($data['password']=='')?'':', password="'.$data['password'].'"';
      $query = 'UPDATE users SET name="'.$data['name'].'", username="'.$data['username'].'", email="'.$data['email'].'"'.$password.' WHERE id='.$data['id'];
      // return $query;
      $datas = DB::select($query);
      return $datas;
    }

    function updateonecontoh($data) {
      $password = ($data['password']=='')?'':', password="'.$data['password'].'"';
      $query = 'UPDATE users SET name="'.$data['name'].'", username="'.$data['username'].'", email="'.$data['email'].'"'.$password.' WHERE id='.$data['id'];
      return $query;
    }

}
