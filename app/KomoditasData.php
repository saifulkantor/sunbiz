<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class KomoditasData extends Model
{
  protected $table = 'komoditas_data';
  protected $fillable = ['harga','id_user','id_komoditas','tanggal','id_kota'];
  public $createstring='';

  function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
    $query = 'SELECT * FROM komoditas_data kd LEFT OUTER JOIN komoditas k ON kd.id_komoditas=k.id LEFT OUTER JOIN kota kt ON kd.id_kota=kt.id';
    if ($filters!=null) {
      $query .= ' WHERE ';
      foreach ($filters as $key => $filter) {
        if ($filter!='') $query .= ' '.$key.'="'.$filter.'" AND';
      }
      $query=substr($query, 0, -3);
    }
    if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
    $datas = DB::select($query);
    return $datas;
  }

  function semuaavg($filters=null,$lengkap=true,$page=1,$limit=25) {

    $query = 'SELECT MAX(kd.id_komoditas) as id_komoditas, AVG(kd.harga) as harga, MAX(kd.id_kota) as id_kota FROM komoditas_data kd LEFT OUTER JOIN komoditas k ON kd.id_komoditas=k.id LEFT OUTER JOIN kota kt ON kd.id_kota=kt.id';
    if ($filters!=null) {
      $query .= ' WHERE ';
      $groupby = '';
      foreach ($filters as $key => $filter) {
        if ($key=='group') $groupby = ' GROUP BY '.$filter; else if ($filter!='') $query .= ' '.$key.'="'.$filter.'" AND';
      }
      $query=substr($query, 0, -3).$groupby;
    }
    if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
    $datas = DB::select($query);
    return $datas;
  }

  function getavgperprovinsi($filters=null) {
    $query = 'SELECT p.*, AVG(kd.harga) as harga FROM provinsi p LEFT OUTER JOIN
            kota k ON p.id=k.id_provinsi LEFT OUTER JOIN
            komoditas_data kd ON k.id=kd.id_kota LEFT OUTER JOIN
            komoditas ko ON kd.id_komoditas=ko.id';
    if ($filters!=null) {
      $query .= ' WHERE ';
      foreach ($filters as $key => $filter) {
        $query .= ' '.$key.'="'.$filter.'" AND';
      }
      $query=substr($query, 0, -3);
    }
    $query.=' GROUP BY p.id, p.nama';
    $datas = DB::select($query);
    return $datas;
  }

  function pushcreatebulk($data) {
    $this->createstring.=' ('.$data["id_komoditas"].','.$data["harga"].',"'.$data["tanggal"].'",'.$data["id_kota"].','.$data["id_user"].'),';
  }

  function createbulk() {
    if ($this->createstring!='') {
      $query = 'INSERT INTO komoditas_data VALUES '.$this->createstring;
      $query=substr($query, 0, -1); $query.=';';
      // return $query;
      $datas = DB::select($query);
      return $datas;
    }
  }

  function createone($data) {
    $this->createstring='';
    $this->pushcreatebulk($data);
    $this->createbulk();
  }

  function updateone($data) {
    $query = 'UPDATE komoditas_data SET harga='.$data["harga"].', id_user='.$data["id_user"].' WHERE id_komoditas='.$data["id_komoditas"].' AND tanggal="'.$data["tanggal"].'" AND id_kota='.$data["id_kota"].';';
    // return $query;
    $datas = DB::select($query);
    return $datas;
  }
}
