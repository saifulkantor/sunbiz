<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Userkota extends Model
{
    protected $table = 'user_kota';
    protected $fillable = ['id_user', 'id_kota'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT k.*,p.nama as nama_provinsi FROM kota k LEFT OUTER JOIN provinsi p ON k.id_provinsi=p.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_kota')?'k.id':$key;
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function hapusdata($wheres=null) {
      $query = 'DELETE FROM user_kota';
      if ($wheres!=null) {
        $query .= ' WHERE ';
        foreach ($wheres as $key => $where) {
          $query .= ' '.$key.'="'.$where.'" AND';
        }
        $query=substr($query, 0, -3); $query.=';';
      }
      $datas = DB::delete($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO user_kota '.$val.' VALUES '.$query;
        // return $query;
        $datas = DB::insert($query);
        return $datas;
      }
    }

}
