<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class TransaksiDetail extends Model
{
    protected $table = 'transaksi_detail';
    protected $fillable = ['id_transaksi','id_komoditas','jumlah','harga'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT td.*,k.nama,k.satuan,k.stok FROM transaksi_detail td LEFT OUTER JOIN komoditas k ON td.id_komoditas=k.id ';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_transaksi_detail')?'id':$key;
          if ($key=='nama')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else
            $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      // $query = 'SELECT * FROM transaksi ORDER BY urutan';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE transaksi_detail SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $key = ($key=='id_transaksi_detail')?'id':$key;
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::update($query);
        return $datas;
      } else {
        return '';
      }
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO transaksi_detail '.$val.' VALUES '.$query;
        // return $query;
        //$datas = DB::select($query);
        $datas = DB::table('transaksi_detail')->insertGetId($datas);
        return $datas;
      }
    }

}
