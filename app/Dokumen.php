<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    protected $table = 'dokumen';
    protected $fillable = ['nama', 'kategori'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT * FROm dokumen';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_kurir')?'id':$key;
          $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO dokumen '.$val.' VALUES '.$query;
        // return $query;
        // $datas = DB::select($query);
        $datas = DB::table('dokumen')->insertGetId($datas);
        return $datas;
      }
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE dokumen SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::select($query);
        return $datas;
      } else {
        return '';
      }
    }
    function listkategori() {
      $kode =  [
        'invoice'=>'Invoice'
      ];
      return $kode;
    }

    function listkode($kategori){
      switch ($kategori) {
        case 'invoice':
          $kode =  [
            'Waktu Sekarang'=>'',
            'tanggal'=>'{{tanggal}}',
            'bulan'=>'{{bulan}}',
            'tahun'=>'{{tahun}}',
            'Pembeli'=>'',
            'id_pembeli'=>'{{id_pembeli}}',
            'nama_pembeli'=>'{{nama_pembeli}}',
            'alamat_pembeli'=>'{{alamat_pembeli}}',
            'Transaksi'=>'',
            'id_transaksi'=>'{{id_transaksi}}',
            'kode_transaksi'=>'{{kode_transaksi}}',
            'tanggal_transaksi'=>'{{tanggal_transaksi}}',
            'catatan'=>'{{catatan}}',
            'ongkir'=>'{{ongkir}}',
            'total'=>'{{total}}',
            'Website'=>'',
            'nama_website'=>'{{nama_website}}',
            'alamat_website'=>'{{alamat_website}}',
            'telp_website'=>'{{telp_website}}',
            'email_website'=>'{{email_website}}',
            'sosmed_website'=>'{{sosmed_website}}',
            'Tabel'=>'',
            'tabel_detail_transaksi'=>'{{tabel_detail_transaksi}}',
          ];
          break;

        default:
          // code...
          break;
      }
      return $kode;

    }

}
