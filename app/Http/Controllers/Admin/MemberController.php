<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Provinsi;
use App\Kota;

class MemberController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $kota = new Kota(); $kota = $kota->semua();
      $member = new User(); $member = $member->semuawithalamat(['level'=>'member','utama'=>1]);
      return view('admin/member',compact('member','provinsi','kota'));
    }

    public function getalamat($id_member) {
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $kota = new Kota(); $kota = $kota->semua();
      $member = new User(); $member = $member->semuawithalamat(['level'=>'member','id_user'=>$id_member]);
      return view('admin/member',compact('member','provinsi','kota'));
    }


}
