<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Komoditas;
use App\Pasar;
use App\Pengaturan;
use App\Kurir;
use App\Dokumen;

class PengaturanController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      $pasar = new Pasar(); $pasar = $pasar->semua();
      return view('admin/pengaturan/home',compact('pasar','komoditas'));
    }

    public function image() {
      return view('admin/pengaturan/image');
    }

    public function imagesave(request $request) {
      $files = $request->allFiles();
      if ($files!=null) {
        foreach ($files as $key => $file) {
            $file->storeAs('images/homepage/',$key.'.jpg');
            // $target_path = public_path().'/images/komoditas/'.$request->id.'/.jpg';
            // move_uploaded_file($file['tmp_name'], $target_path);
            // $target_path = $file['tmp_name'];
        }
      }
      return redirect('admin/pengaturan/image');
    }
    public function identitas() {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      return view('admin/pengaturan/identitas',compact('pengaturan'));
    }
    public function identitassave(request $request) {
      $namawebsite=$request->namawebsite1.'||'.$request->namawebsite2.'||'.$request->namawebsite3;
      $sosmed=$request->sosmed1.'||'.$request->sosmed2.'||'.$request->sosmed3;
      $pengaturan = new Pengaturan();
      $pengaturan->updateone(['data'=>$namawebsite],['id'=>2]);
      $pengaturan->updateone(['data'=>$request->detailwebsite],['id'=>3]);
      $pengaturan->updateone(['data'=>$request->contact],['id'=>4]);
      $pengaturan->updateone(['data'=>$request->email],['id'=>5]);
      $pengaturan->updateone(['data'=>$request->address],['id'=>6]);
      $pengaturan->updateone(['data'=>$sosmed],['id'=>7]);
      return redirect('admin/pengaturan/identitas');
    }

    public function transaksi() {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $kurir = new Kurir(); $kurir = $kurir->semua();
      return view('admin/pengaturan/transaksi',compact('pengaturan','kurir'));
    }
    public function transaksisave(request $request) {
      $pengaturan = new Pengaturan();
      $pengaturan->updateone(['data'=>$request->kodetransaksi],['id'=>8]);
      $kurirmdl = new Kurir();
      foreach ($request->nama as $key => $kurir) {
        $cekkurir = Kurir::where('id', $request->id[$key])->first();
        if ($cekkurir) {
          $cekkurir->kapasitas=$request->kapasitas[$key];
          $cekkurir->nama=$request->nama[$key];
          $cekkurir->timestamps = false;
          $cekkurir->save();
        } else {
            $kurirmdl->createone(['nama'=>$request->nama[$key],'kapasitas'=>$request->kapasitas[$key]]);
        }
      }
      return redirect('admin/pengaturan/transaksi');
    }

    public function dokumen() {
      $dokumenmdl = new Dokumen(); $dokumen = $dokumenmdl->semua();
      return view('admin/pengaturan/dokumen',compact('dokumen','dokumenmdl'));
    }
    public function dokumensave(request $request) {
      $dokumenmdl = new Dokumen();
      $cekdokumen = Dokumen::where('id', $request->id)->first();
      if ($cekdokumen) {
        $cekdokumen->kategori=$request->kategori;
        $cekdokumen->nama=$request->nama;
        $cekdokumen->timestamps = false;
        $cekdokumen->save();
        $id=$cekdokumen->id;
      } else {
          $id=$dokumenmdl->createone(['nama'=>$request->nama,'kategori'=>$request->kategori]);
      }
      $template = file_put_contents(public_path().'/dokumen/'.$id.'.txt',$request->template);
      return redirect('admin/pengaturan/dokumen');
    }

    public function andalan() {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      return view('admin/pengaturan/andalan',compact('pengaturan','komoditas'));
    }
    public function andalansave(request $request) {
      $pengaturan = new Pengaturan(); $dataandalan='';
      foreach ($request->komoditas as $key => $value) {
        if ($value!=0) {
          $dataandalan.=$value.',';
        }
      }
      $dataandalan =($dataandalan=='')?$pengaturan->semua()[0]->data:substr($dataandalan, 0, -1);
      $pengaturan->updateone(['data'=>$dataandalan],['id'=>1]);
      return redirect('admin/pengaturan/andalan');
    }

}
