<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use App\Pengaturan;
use App\Artikel;
use App\ArtikelKategori;

class ArtikelController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $artikelmdl = new Artikel(); $artikel = $artikelmdl->semua();
      $artikelkategori = new ArtikelKategori(); $artikelkategori = $artikelkategori->semua();
      return view('admin/artikel/home',compact('artikel','artikelmdl','artikelkategori'));
    }
    public function artikelsave(request $request) {
      $artikelmdl = new Artikel();
      $cekartikel = Artikel::where('id', $request->id)->first();
      if (!$cekartikel) {
        $cekartikel = new Artikel();
      }
      $cekartikel->id_kategori=$request->id_kategori;
      $cekartikel->judul=$request->judul;
      $cekartikel->overview=$request->overview;
      $cekartikel->slug = Str::slug($cekartikel->judul, '-');
      $cekartikel->id_user=$request->id_user;
      $cekartikel->save();
      $id=$cekartikel->id;
      $template = file_put_contents(public_path().'/artikel/'.$id.'.txt',$request->template);
      $files = $request->allFiles();
      if ($files!=null) {
        foreach ($files as $key => $file) {
            $file->storeAs('images/artikel/',$id.'.jpg');
            // $target_path = public_path().'/images/komoditas/'.$request->id.'/.jpg';
            // move_uploaded_file($file['tmp_name'], $target_path);
            // $target_path = $file['tmp_name'];
        }
      }
      return redirect('admin/artikel');
    }

}
