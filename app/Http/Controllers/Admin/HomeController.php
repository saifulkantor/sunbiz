<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Kota;
use App\User;

class HomeController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $kota = new Kota(); $kota = $kota->semua();
      $contributor = new User(); $contributor = $contributor->semua(['level'=>'contributor']);
      return view('admin/home',compact('kota','contributor'));
    }
}
