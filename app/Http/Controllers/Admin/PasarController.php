<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Pasar;
use App\Provinsi;
use App\Kota;

class PasarController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $kota = new Kota(); $kota = $kota->semua();
      $pasar = new Pasar(); $pasar = $pasar->semua();
      return view('admin/pasar',compact('pasar','provinsi','kota'));
    }


}
