<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Provinsi;
use App\Kota;

class KotaController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $kota = new Kota(); $kota = $kota->semua();
      return view('admin/kota',compact('provinsi','kota'));
    }


}
