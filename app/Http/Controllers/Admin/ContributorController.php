<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Provinsi;
use App\Kota;

class ContributorController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $kota = new Kota(); $kota = $kota->semua();
      $contributor = new User(); $contributor = $contributor->semuawithkota(['level'=>'contributor']);
      return view('admin/contributor',compact('contributor','provinsi','kota'));
    }


}
