<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Provinsi;
use App\Kota;
use App\Komoditas;
use App\Transaksi;
use App\TransaksiDetail;
use App\Pengaturan;
use App\Dokumen;

class TransaksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function list()
    {
      $transaksi2 = new Transaksi(); $transaksi = $transaksi2->semua(['id_user'=>auth()->user()->id]);
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      return view('admin/transaksi/list', compact('transaksi','transaksi2','pengaturan','komoditas'));
    }
    public function detail($kode_transaksi)
    {
      $transaksimodel = new Transaksi();
      $transaksi = $transaksimodel->lengkap(['kode'=>$kode_transaksi]);
      $transaksidetail = new TransaksiDetail();
      if ($transaksi!=null) {
        $transaksi=$transaksi[0];
        $transaksidetail = $transaksidetail->semua(['id_transaksi'=>$transaksi->id_transaksi]);
      } else {
        return redirect('404');
      }
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      $dokumen = new Dokumen(); $dokumen = $dokumen->semua();
      return view('admin/transaksi/detail', compact('transaksi','transaksimodel','transaksidetail','pengaturan','komoditas','dokumen'));
    }


}
