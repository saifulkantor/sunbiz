<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Komoditas;
use App\Pasar;

class KomoditasController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      $pasar = new Pasar(); $pasar = $pasar->semua();
      return view('admin/komoditas',compact('pasar','komoditas'));
    }

}
