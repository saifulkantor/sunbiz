<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contactus;
use App\Provinsi;
use App\Kota;
use App\Komoditas;
use App\Pengaturan;
use App\Artikel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $artikel = new Artikel(); $artikel = $artikel->semua(null,false,1,3);
      $komoditas = new Komoditas(); $komoditasutamajml = $komoditas->getutamajml();
      $komoditas = $komoditas->semua();
      return view('home', compact('pengaturan','komoditas','komoditasutamajml','artikel'));
    }

    public function data()
    {
      $komoditas = new Komoditas(); $komoditasall = $komoditas->semua();
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      return view('data', compact('komoditasall','pengaturan'));
    }

    public function contactus()
    {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      return view('contactus', compact('pengaturan'));
    }
    public function contactusemail(request $request)
    {
      $subject = '[Contact Us] Pesan dari Website';
      Mail::to('saiful.kantor@gmail.com')->send(new Contactus($request,$subject));
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      return view('contactus', compact('pengaturan'));
    }

    public function getkomoditasparent() {
      $komoditas = new Komoditas();
      return $komoditas->semua(['id_parent'=>NULL]);
    }

    public function getkomoditas($id_parent) {
      $komoditas = new Komoditas();
      return $komoditas->semua(['id_parent'=>id_parent]);
    }

}
