<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $kotaku = Auth::user()->kota();
      if (Auth::user()->level=='admin') return redirect('admin'); else if (Auth::user()->level=='member') return redirect('/transaksi'); else return view('backend/home',compact('kotaku'));
    }
}
