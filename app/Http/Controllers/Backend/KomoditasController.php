<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Komoditas;

class KomoditasController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      $komoditas = new Komoditas();
      $komoditas = $komoditas->semua();
      $kotaku = Auth::user()->kota();
      return view('backend/komoditas',compact('kotaku','komoditas'));
    }

    protected function simpan(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => str_slug(strtolower($data['name']),'_'),
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

}
