<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use App\Pengaturan;
use App\Artikel;
use App\ArtikelKategori;
use App\Komoditas;

class ArtikelController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }

    public function index() {
      $artikelmdl = new Artikel(); $artikel = $artikelmdl->semua();
      $artikelkategori = new ArtikelKategori(); $artikelkategori = $artikelkategori->semua();
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      return view('artikel/home',compact('artikel','pengaturan','artikelmdl','artikelkategori'));
    }
    public function detail($slug) {
      $artikelmdl = new Artikel(); $artikel = $artikelmdl->semua(['slug'=>$slug]);
      $artikelkategori = new ArtikelKategori(); $artikelkategori = $artikelkategori->semua();
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      if ($artikel){
        $artikel=$artikel[0];
        return view('artikel/detail',compact('artikel','pengaturan','artikelmdl','artikelkategori','komoditas'));
      } else {
        return redirect('404');
      }
    }

}
