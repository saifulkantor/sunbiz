<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provinsi;
use App\Kota;
use App\Komoditas;
use App\Transaksi;
use App\TransaksiDetail;
use App\Pengaturan;
use Auth;

class TransaksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      if (Auth::user()->level!='member') return redirect('/backend');
      $provinsi = new Provinsi(); $provinsi = $provinsi->semua();
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      return view('transaksi/home', compact('provinsi','pengaturan','komoditas'));
    }

    public function list()
    {
      $transaksi2 = new Transaksi(); $transaksi = $transaksi2->semua(['id_user'=>auth()->user()->id]);
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
      return view('transaksi/list', compact('transaksi','transaksi2','pengaturan','komoditas'));
    }
    public function detail($kode_transaksi)
    {
      $transaksimodel = new Transaksi();
      $transaksi = $transaksimodel->semua(['id_user'=>auth()->user()->id,'kode'=>$kode_transaksi]);
      if ($transaksi) {
        $transaksi=$transaksi[0];
        $transaksidetail = new TransaksiDetail();
        if ($transaksi!=null) {
          $transaksidetail = $transaksidetail->semua(['id_transaksi'=>$transaksi->id]);
        } else {
          return redirect('404');
        }
        $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
        $komoditas = new Komoditas(); $komoditas = $komoditas->semua();
        return view('transaksi/detail', compact('transaksi','transaksimodel','transaksidetail','pengaturan','komoditas'));
      } else {
        return redirect('404');
      }
    }

    public function komoditas()
    {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditasparent = new Komoditas(); $komoditasparent = $komoditasparent->semua(['id_parent'=>NULL]);
      return view('transaksi/komoditas', compact('komoditasparent','pengaturan'));
    }

    public function keranjang()
    {
      $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
      $komoditasparent = new Komoditas(); $komoditasparent = $komoditasparent->semua(['id_parent'=>NULL]);
      return view('transaksi/keranjang', compact('komoditasparent','pengaturan'));
    }

    public function getkomoditasparent() {
      $komoditas = new Komoditas();
      return $komoditas->semua(['id_parent'=>NULL]);
    }

    public function getkomoditas($id_parent) {
      $komoditas = new Komoditas();
      return $komoditas->semua(['id_parent'=>id_parent]);
    }

}
