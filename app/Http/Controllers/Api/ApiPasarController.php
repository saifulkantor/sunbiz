<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Pasar;

class ApiPasarController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }


    public function postdata(request $request) {
      $pasarcreate = new Pasar();
      $pasar=Pasar::where(['id'=>$request->id_pasar])->first();
      if ($pasar!=null) {
        $pasar->updateone(['id_kota'=>$request->id_kota,'nama'=>$request->nama],['id'=>$request->id_pasar]);
      } else {
        $pasarcreate->createone(['id_kota'=>$request->id_kota,'nama'=>$request->nama]);
      }
      return json_encode('sukses');
    }

    public function getdata(request $request) {
      $pasar = new Pasar();
      return $pasar->semua($request->all());
    }

}
