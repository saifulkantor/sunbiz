<?php
namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Transaksi;
use App\TransaksiDetail;
use App\Komoditas;
use App\Keranjang;
use App\Pengaturan;
use App\KomoditasData;
use App\Mail\Invoice;

class ApiTransaksiController extends Controller
{

    public function __construct() {
        // $this->middleware('auth:api');
    }

    public function tambahkeranjang(request $request) {
        $id=$request->id; $user = $request->id_user;
        $komoditas = Komoditas::find($id);
        if(!$komoditas) {
            return json_encode('Komoditas Tidak Ada');
        } else if($komoditas->stok<$request->jumlah) {
            return json_encode('Jumlah Melebihi Stok');
        }
        $expired_date=date('Y-m-d H:i:s', strtotime('+1 day', time()));
        $keranjang = new Keranjang();
        $datakeranjang=$keranjang->semua(['id_user'=>$user,'id_komoditas'=>$id]);

        if(!$datakeranjang) {
            $keranjang->createone(['jumlah'=>$request->jumlah,'expired_date'=>$expired_date,'id_user'=>$user,'id_komoditas'=>$id]);
        } else {
            $keranjang->updateone(['jumlah'=>($datakeranjang[0]->jumlah+$request->jumlah),'expired_date'=>$expired_date],['id_user'=>$user,'id_komoditas'=>$id]);
        }

        $komoditas->updateone(['stok'=>($komoditas->stok-$request->jumlah)],['id'=>$id]);
        $datakeranjang=$keranjang->semua(['id_user'=>$user]);

        return json_encode(count($datakeranjang));
    }

    public function hapuskeranjang(request $request) {
        $id=$request->id; $user = $request->id_user;
        $komoditas = Komoditas::find($id);
        if(!$komoditas) {
            return json_encode('Komoditas Tidak Ada');
        }
        $keranjang = new Keranjang();
        $datakeranjang=$keranjang->semua(['id_user'=>$user,'id_komoditas'=>$id]);

        if(!$datakeranjang) {
            return json_encode('Komoditas Tidak Ada di Keranjang');
        } else {
            $komoditas->updateone(['stok'=>($komoditas->stok+$datakeranjang[0]->jumlah)],['id'=>$id]);
            $keranjang->deleteone(['id_user'=>$user,'id_komoditas'=>$id]);
        }
        $datakeranjang=$keranjang->semua(['id_user'=>$user]);

        return json_encode(count($datakeranjang));
    }

    public function buattransaksi(request $request) {
      DB::beginTransaction();
      try {
        $transaksi = new Transaksi();
        $pengaturan = new Pengaturan(); $pengaturan = $pengaturan->semua();
        $user = $request->id_user;
        $total=0;
        $request->catatan=($request->catatan=='')?'-':$request->catatan;
        $tanggal=date('Y-m-d H:i:s', time());
        $id_transaksi=$transaksi->createone(['kode'=>0,'id_user'=>$user,'tanggal'=>$tanggal,'alamat'=>$request->alamat,'metode_pembayaran'=>$request->metode_pembayaran,'ongkir'=>$request->ongkir,'diskon'=>$request->diskon,'total'=>$total,'catatan'=>$request->catatan,'status'=>'pendingadmin']);
        $keranjang = new Keranjang();
        $transaksidetail = new TransaksiDetail();
        foreach ($request->id_komoditas as $key => $value) {
          $datakeranjang=$keranjang->semua(['id_user'=>$user,'id_komoditas'=>$value]);

          $komoditasmdl = new Komoditas();
          $komoditas = $komoditasmdl->semua(['id'=>$value]);
          if(!$komoditas) {
              // tidak diproses
          } else if($komoditas[0]->stok<($request->jumlah[$value] - $datakeranjang[0]->jumlah)) {
              // tidak diproses
          } else {
            $transaksidetail->createone(['id_transaksi'=>$id_transaksi,'id_komoditas'=>$value,'jumlah'=>$request->jumlah[$value],'harga'=>$komoditas[0]->harga]);
            $komoditasmdl->updateone(['stok'=>($komoditas[0]->stok-($request->jumlah[$value] - $datakeranjang[0]->jumlah))],['id'=>$value]);
            $keranjang->deleteone(['id_user'=>$request->id_user,'id_komoditas'=>$value]);
            $total+= ($request->jumlah[$value]*$komoditas[0]->harga);
          }
        }
        $total-=($request->ongkir-$request->diskon);
        $kode = $transaksi->kodereplace($pengaturan[7]->data,['id_user'=>$user,'id_transaksi'=>$id_transaksi]);
        $transaksi->updateone(['total'=>$total,'kode'=>$kode],['id'=>$id_transaksi]);

        DB::commit();
        return json_encode($kode);
      } catch (\Exception $e) {
        DB::rollback();
        return json_encode('gagal');
      }
      //return json_encode('gagal');
    }

    public function ubahdetail($kode_transaksi,request $request){
      $transaksimodel = new Transaksi();
      $transaksidetailmodel = new TransaksiDetail();
      $komoditasmodel = new Komoditas();
      $transaksi = $transaksimodel->semua(['kode'=>$kode_transaksi]);
      $status=isset($request->status)?$request->status:'';
      if ($transaksi!=null) {
        $transaksi=$transaksi[0];
        $transaksidetail = $transaksidetailmodel->semua(['id_transaksi'=>$transaksi->id]);
        $total=0;
        if ($transaksi->status=='pendingadmin') {
          foreach ($transaksidetail as $key => $value) {
            $jumlah=$request->jumlah[$value->id];
            $komoditas = $komoditasmodel->semua(['id'=>$value->id_komoditas]);
            if ($status=='rejected') {
              $jumlah=0;
              $total=$transaksi->total;
            } else {
              $transaksidetailmodel->updateone(['jumlah'=>$jumlah],['id'=>$value->id,'id_transaksi'=>$transaksi->id]);
              $total+=($value->harga*$jumlah);
            }
            $komoditasmodel->updateone(['stok'=>($komoditas[0]->stok-($jumlah - $value->jumlah))],['id'=>$value->id_komoditas]);
          }
          $transaksimodel->updateone(['ongkir'=>$request->ongkir],['id'=>$transaksi->id,'kode'=>$kode_transaksi]);
          $total+=$request->ongkir;
        } else {
          $total=$transaksi->total;
        }
        if ($status!='') {
          $transaksimodel->updateone(['status'=>$status,'total'=>$total],['id'=>$transaksi->id,'kode'=>$kode_transaksi]);
        }
        return json_encode('sukses');
      } else {
        return json_encode('Data Tidak Ditemukan');
      }
    }

    public function kiriminvoice($kode_transaksi,request $request) {
      $subject = '[Invoice] '.$kode_transaksi;
      Mail::to($request->email)->send(new Invoice($kode_transaksi,['email'=>$request->email,'template'=>$request->template,'subject'=>$subject]));
      return json_encode('sukses');
    }

    public function getlisttransaksi(request $request) {
      $transaksi = new Transaksi();
      return $transaksi->semua($request->except(['page','perpage']),false,$request->page,$request->perpage);
    }

}
