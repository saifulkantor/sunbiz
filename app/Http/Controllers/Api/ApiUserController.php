<?php
namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Userkota;
use App\Alamat;

class ApiUserController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        return User::create([
            'name' => $request->name,'username' => str_slug(strtolower($request->name),'_'),'email' => $request->email,'password' => Hash::make($request->password),
        ]);
    }


    public function postdata(request $request) {
      DB::beginTransaction();
      $lokasi='awal';
      try {
          $usercreate = new User();
          $userkota = new Userkota();
          $alamat = new Alamat();
          $user=User::where(['id'=>$request->id_user])->first();
          $lokasi='awal user';
          if ($user!=null) {
            $lokasi='awal user 2';
            $password=($request->password=='')?$user->password:Hash::make($request->password);
            //$lokasi=$user->updateonecontoh(['id'=>$request->id_user,'name' => $request->name,'username' => str_slug(strtolower($request->name),'_'),'email' => $request->email,'password' => $password]);
            $user->updateone(['id'=>$request->id_user,'name' => $request->name,'username' => str_slug(strtolower($request->name),'_'),'email' => $request->email,'password' => $password]);
          } else {
            $lokasi='awal user 3';
            $request->id_user = $usercreate->createone(['name' => $request->name,'username' => str_slug(strtolower($request->name),'_'), 'level' => $request->level,'email' => $request->email,'password' => $password,'remember_token'=>'-','email_verified_at'=>NULL,'created_at'=>now(),'updated_at'=>now()]);
          }
          $lokasi='createuser';
          if ($request->level=='contributor') {
            if ($request->id_kota!=null) {
              $userkota->hapusdata(['id_user'=>$request->id_user]);
              foreach ($request->id_kota as $key => $id_kota) {
                $userkota->createone(['id_user'=>$request->id_user,'id_kota'=>$id_kota]);
              }
            }
          } else if ($request->level=='member') {
            if ($request->judul!=null) {
              $lokasi='awal member';
              if ($request->hapusalamat!='') {
                $hapusalamat=explode(",",$request->hapusalamat);
                foreach ($hapusalamat as $key => $id_alamat) {
                  $alamat->hapusdata(['id_alamat'=>$id_alamat]);
                }
              }
              $lokasi='hapus member';
              foreach ($request->judul as $key => $judul) {
                $id=$request->id[$key]??0;
                $cekalamat = Alamat::where('id', $id)->first();
                $lokasi='hapus member 2';
                if ($cekalamat) {
                  $lokasi='cekalamat 3--'.$id.'--'.$cekalamat->id;
                  $cekalamat->judul=$request->judul[$key]??$cekalamat->judul;
                  $cekalamat->alamat=$request->alamat[$key]??$cekalamat->alamat;
                  $cekalamat->id_kota=$request->id_kota[$key]??$cekalamat->id_kota;
                  $cekalamat->kodepos=$request->kodepos[$key]??$cekalamat->kodepos;
                  $cekalamat->telp=$request->telp[$key]??$cekalamat->telp;
                  $cekalamat->utama=$request->utama[$key]??$cekalamat->utama;
                  $cekalamat->timestamps = false;
                  $cekalamat->save();
                } else {
                  $lokasi='cekalamat 2'.$key;
                  $alamat->createone([
                    'judul'=>$request->judul[$key]??'',
                    'alamat'=>$request->alamat[$key]??'',
                    'id_kota'=>$request->id_kota[$key],
                    'kodepos'=>$request->kodepos[$key]??0,
                    'telp'=>$request->telp[$key]??0,
                    'id_user'=>$request->id_user,
                    'utama'=>$request->utama[$key]??0
                  ]);
                }
              }
              //$lokasi='akhir member';
            }
          }
          // return json_encode($lokasi);

          DB::commit();
          return json_encode('sukses');
      } catch (\Exception $e) {
          DB::rollback();
          return json_encode($lokasi);
      }
    }

    public function getdata(request $request) {
      $user = new User();
      $data = $user->semuawithalamat($request->all());
      return json_encode($data);
    }

}
