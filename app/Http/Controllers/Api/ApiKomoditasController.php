<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Komoditas;
use App\Keranjang;
use App\KomoditasData;

class ApiKomoditasController extends Controller
{

    public function __construct() {
        // $this->middleware('auth:api');
    }

    // Contributor
    public function postdata(request $request) {
      $kodacreate = new KomoditasData();
      foreach ($request->hargakomoditas as $key => $data) {
        $koda=KomoditasData::where(['id_komoditas'=>$key,'tanggal'=>$request->tanggal,'id_kota'=>$request->id_kota])->first();
        if ($koda!=null) {
          $koda->updateone(['id_komoditas'=>$key,'harga'=>$data,'tanggal'=>$request->tanggal,'stok'=>0,'id_kota'=>$request->id_kota,'id_user'=>$request->id_user]);
        } else if ($data>0){
          $kodacreate->pushcreatebulk(['id_komoditas'=>$key,'harga'=>$data,'tanggal'=>$request->tanggal,'stok'=>0,'id_kota'=>$request->id_kota,'id_user'=>$request->id_user]);
        }
      }
      $kodacreate->createbulk();
      return json_encode('sukses');
    }

    // Admin
    public function adminpostdata(request $request) {
      $komcreate = new Komoditas(); $id=$request->id;
      $request->tanggal = str_replace('/', '-', $request->tanggal );
      $request->tanggal=date('Y-m-d',strtotime($request->tanggal));
      $kom=Komoditas::where(['id'=>$id])->first();
      if ($kom!=null) {
        $komcreate->updateone(['id_user'=>$request->id_user,'id_parent'=>$request->id_parent,'satuan'=>$request->satuan,'nama'=>$request->nama,'tanggal'=>$request->tanggal,'harga'=>$request->harga,'stok'=>$request->stok],['id'=>$id]);
      } else {
        $id=$komcreate->createone(['id_user'=>$request->id_user,'id_parent'=>$request->id_parent,'satuan'=>$request->satuan,'nama'=>$request->nama,'tanggal'=>$request->tanggal,'harga'=>$request->harga,'stok'=>$request->stok]);
      }
      $files = $request->allFiles();
      if ($files!=null) {
        foreach ($files as $key => $file) {
            $file->storeAs('images/komoditas/',$id.'.jpg');
            // $target_path = public_path().'/images/komoditas/'.$request->id.'/.jpg';
            // move_uploaded_file($file['tmp_name'], $target_path);
            // $target_path = $file['tmp_name'];
        }
      }
      return json_encode('sukses');
    }

    public function getlistkomoditas(request $request) {
      $komoditas = new Komoditas();
      return $komoditas->semua($request->except(['page','perpage']),false,$request->page,$request->perpage);
    }

    public function getdata(request $request) {
      $koda = new KomoditasData();
      return $koda->semua($request->all());
    }

    public function getdataavg(request $request) {
      $koda = new Komoditas();
      if ($request->id_komoditas=='') $data = $request->except(['id_komoditas']); else $data = $request->all();
      return $koda->semua($data);
    }

    public function getavgperprovinsi(request $request) {
      $koda = new KomoditasData();
      return $koda->getavgperprovinsi($request->all());
    }

}
