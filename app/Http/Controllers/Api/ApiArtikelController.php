<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Artikel;

class ApiArtikelController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }


    public function getdata(request $request) {
      $cekartikel = Artikel::where('id', $request->id)->first();
      if ($cekartikel) {
        if(file_exists(public_path('dataartikel/'.$cekartikel->id.'.txt'))) {
          $response = file_get_contents(url('/').'/dataartikel/'.$cekartikel->id.'.txt');
        } else {
          $response = file_get_contents(url('/').'/dataartikel/default.txt');
        }
      } else {
        $response = file_get_contents(url('/').'/dataartikel/default.txt');
      }
      return json_encode($response);
    }

    public function getlistartikel(request $request) {
      $artikel = new Artikel();
      return $artikel->semua($request->except(['page','perpage']),false,$request->page,$request->perpage);
    }

    function hapusdata(request $request) {
      $cekartikel = Artikel::where('id', $request->id)->first();
      if ($cekartikel) {
        if(file_exists(public_path('dataartikel/'.$cekartikel->id.'.txt'))) {
          unlink(public_path().'/dataartikel/'.$cekartikel->id.'.txt');
        }
        $cekartikel->delete();
        return json_encode('sukses');
      } else {
        return json_encode('gagal');
      }
    }

}
