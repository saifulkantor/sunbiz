<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Dokumen;

class ApiDokumenController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }


    public function getdata(request $request) {
      $cekdokumen = Dokumen::where('id', $request->id)->first();
      if ($cekdokumen) {
        if(file_exists(public_path('dokumen/'.$cekdokumen->id.'.txt'))) {
          $response = file_get_contents(url('/').'/dokumen/'.$cekdokumen->id.'.txt');
        } else {
          $response = file_get_contents(url('/').'/dokumen/default.txt');
        }
      } else {
        $response = file_get_contents(url('/').'/dokumen/default.txt');
      }
      return json_encode($response);
    }

    function hapusdata(request $request) {
      $cekdokumen = Dokumen::where('id', $request->id)->first();
      if ($cekdokumen) {
        if(file_exists(public_path('dokumen/'.$cekdokumen->id.'.txt'))) {
          unlink(public_path().'/dokumen/'.$cekdokumen->id.'.txt');
        }
        $cekdokumen->delete();
        return json_encode('sukses');
      } else {
        return json_encode('gagal');
      }
    }

}
