<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Kota;

class ApiKotaController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');
    }


    public function postdata(request $request) {
      $kotacreate = new Kota();
      $kota=Kota::where(['id'=>$request->id_kota])->first();
      if ($kota!=null) {
        $kota->updateone(['jenis'=>$request->jenis,'nama'=>$request->nama,'id_provinsi'=>$request->id_provinsi],['id'=>$request->id_kota]);
      } else {
        $kotacreate->createone(['jenis'=>$request->jenis,'nama'=>$request->nama,'id_provinsi'=>$request->id_provinsi]);
      }
      return json_encode('sukses');
    }

    public function getdata(request $request) {
      $kota = new Kota();
      return $kota->semua($request->all());
    }

}
