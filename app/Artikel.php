<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = 'artikel';
    protected $fillable = ['judul','id_kategori','overview','id_user'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT a.*,ak.nama as kategori FROM artikel a LEFT OUTER JOIN artikel_kategori ak ON a.id_kategori=ak.id';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $key = ($key=='id_artikel')?'id':$key;
          if ($key=='judul')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else
            $query .= ' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      $query .=' ORDER by updated_at DESC';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function createone($datas=null) {
      if ($datas!='') {
        $query = '(';
        $val = '(';
        foreach ($datas as $key => $data) {
          $val .= $key.',';
          $query .= ($data==null)?'NULL,':'"'.$data.'",';
        }
        $val=substr($val, 0, -1); $val.=')';
        $query=substr($query, 0, -1); $query.=');';
        $query = 'INSERT INTO artikel '.$val.' VALUES '.$query;
        // return $query;
        // $datas = DB::select($query);
        $datas = DB::table('artikel')->insertGetId($datas);
        return $datas;
      }
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE artikel SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::select($query);
        return $datas;
      } else {
        return '';
      }
    }

    function listkode(){
      $kode =  [
        'Waktu Sekarang'=>'',
        'tanggal'=>'{{tanggal}}',
        'bulan'=>'{{bulan}}',
        'tahun'=>'{{tahun}}',
        'Website'=>'',
        'nama_website'=>'{{nama_website}}',
        'alamat_website'=>'{{alamat_website}}',
        'telp_website'=>'{{telp_website}}',
        'email_website'=>'{{email_website}}',
        'sosmed_website'=>'{{sosmed_website}}',
      ];
      return $kode;

    }

}
