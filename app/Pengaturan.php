<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Pengaturan extends Model
{
    protected $table = 'pengaturan';
    protected $fillable = ['nama', 'data'];

    function semua($filters=null,$lengkap=true,$page=1,$limit=25) {
      $query = 'SELECT * FROM pengaturan';
      if ($filters!=null) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
          $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      }
      // $query = 'SELECT * FROM komoditas ORDER BY urutan';
      if (!$lengkap && (int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      $datas = DB::select($query);
      return $datas;
    }

    function updateone($datas=null,$wheres=null) {
      if ($datas!=null) {
        $query = 'UPDATE pengaturan SET ';
        foreach ($datas as $key => $data) {
          $query .= ' '.$key.'="'.$data.'",';
        }
        $query=substr($query, 0, -1);
        if ($wheres!=null) {
          $query .= ' WHERE ';
          foreach ($wheres as $key => $where) {
            $key = ($key=='id_pengaturan')?'id':$key;
            $query .= ' '.$key.'="'.$where.'" AND';
          }
          $query=substr($query, 0, -3);
        }
        $query.=';';

        // return $query;
        $datas = DB::update($query);
        return $datas;
      } else {
        return '';
      }
    }

}
