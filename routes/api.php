<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getkomoditas_dataavg', 'Api\ApiKomoditasController@getdataavg');
Route::post('getkomoditas_data', 'Api\ApiKomoditasController@getdata');
Route::post('komoditas_data', 'Api\ApiKomoditasController@postdata');
Route::post('getavgkomoditas_dataperprov', 'Api\ApiKomoditasController@getavgperprovinsi');

Route::post('getcontributor', 'Api\ApiUserController@getdata');
Route::post('contributor', 'Api\ApiUserController@postdata');

Route::post('getmember', 'Api\ApiUserController@getdata');
Route::post('member', 'Api\ApiUserController@postdata');

Route::post('getpasar', 'Api\ApiPasarController@getdata');
Route::post('pasar', 'Api\ApiPasarController@postdata');

Route::post('getkota', 'Api\ApiKotaController@getdata');
Route::post('kota', 'Api\ApiKotaController@postdata');

Route::post('komoditas', 'Api\ApiKomoditasController@adminpostdata');
Route::post('listkomoditas', 'Api\ApiKomoditasController@getlistkomoditas');

Route::post('listtransaksi', 'Api\ApiTransaksiController@getlisttransaksi');

Route::post('tambahkeranjang', 'Api\ApiTransaksiController@tambahkeranjang');
Route::post('hapuskeranjang', 'Api\ApiTransaksiController@hapuskeranjang');

Route::post('buattransaksi', 'Api\ApiTransaksiController@buattransaksi');
Route::post('transaksi/ubahdetail/{kode_transaksi}', 'Api\ApiTransaksiController@ubahdetail');
Route::post('transaksi/kiriminvoice/{kode_transaksi}', 'Api\ApiTransaksiController@kiriminvoice');

Route::post('getdokumen', 'Api\ApiDokumenController@getdata');
Route::post('hapusdokumen', 'Api\ApiDokumenController@hapusdata');

Route::post('listartikel', 'Api\ApiArtikelController@getlistartikel');
Route::post('getartikel', 'Api\ApiArtikelController@getdata');
Route::post('hapusartikel', 'Api\ApiArtikelController@hapusdata');
