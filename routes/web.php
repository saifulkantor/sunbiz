<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');

Route::get('/data', 'HomeController@data');
Route::get('/contactus', 'HomeController@contactus');
Route::post('/contactus', 'HomeController@contactusemail');
/*
Route::get('/user', 'UserController@index');
Route::get('/user-register', 'UserController@create');
Route::post('/user-register', 'UserController@store');
Route::get('/user-edit/{id}', 'UserController@edit');
*/

Route::prefix('backend')->group(function () {
    Route::get('/', 'Backend\HomeController@index');
    Route::get('/komoditas', 'Backend\KomoditasController@index');
    Route::post('/komoditas', 'Backend\KomoditasController@simpan');
});

Route::prefix('transaksi')->group(function () {
    Route::get('/', 'TransaksiController@index');
    Route::get('/komoditas', 'TransaksiController@komoditas');
    Route::get('/keranjang', 'TransaksiController@keranjang');
    Route::get('/list', 'TransaksiController@list');
    Route::get('/detail/{kode_transaksi}', 'TransaksiController@detail');
});

Route::prefix('artikel')->group(function () {
    Route::get('/', 'ArtikelController@index');
    Route::get('/{slug}', 'ArtikelController@detail');
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\HomeController@index');
    Route::get('/komoditas', 'Admin\KomoditasController@index');
    Route::post('/komoditas', 'Admin\KomoditasController@simpan');
    Route::get('/contributor', 'Admin\ContributorController@index');
    Route::post('/contributor', 'Admin\ContributorController@simpan');
    Route::get('/member', 'Admin\MemberController@index');
    Route::post('/member', 'Admin\MemberController@simpan');
    Route::post('/getmemberalamat/{id_member}', 'Admin\MemberController@getalamat');
    Route::get('/pasar', 'Admin\PasarController@index');
    Route::post('/pasar', 'Admin\PasarController@simpan');
    Route::get('/kota', 'Admin\KotaController@index');
    Route::post('/kota', 'Admin\KotaController@simpan');
    Route::prefix('pengaturan')->group(function () {
      Route::get('/', 'Admin\PengaturanController@index');
      Route::get('/image', 'Admin\PengaturanController@image');
      Route::post('/image', 'Admin\PengaturanController@imagesave');
      Route::get('/identitas', 'Admin\PengaturanController@identitas');
      Route::post('/identitas', 'Admin\PengaturanController@identitassave');
      Route::get('/transaksi', 'Admin\PengaturanController@transaksi');
      Route::post('/transaksi', 'Admin\PengaturanController@transaksisave');
      Route::get('/dokumen', 'Admin\PengaturanController@dokumen');
      Route::post('/dokumen', 'Admin\PengaturanController@dokumensave');
      Route::get('/andalan', 'Admin\PengaturanController@andalan');
      Route::post('/andalan', 'Admin\PengaturanController@andalansave');
    });
    Route::prefix('transaksi')->group(function () {
        Route::get('/', 'Admin\TransaksiController@list');
        Route::get('/detail/{kode_transaksi}', 'Admin\TransaksiController@detail');
    });
    Route::prefix('artikel')->group(function () {
        Route::get('/', 'Admin\ArtikelController@index');
        Route::post('/', 'Admin\ArtikelController@artikelsave');
    });
});
