-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2019 at 04:30 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisko`
--
CREATE DATABASE IF NOT EXISTS `sisko` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sisko`;

-- --------------------------------------------------------

--
-- Table structure for table `komoditas`
--

CREATE TABLE `komoditas` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `satuan` varchar(100) NOT NULL DEFAULT 'kg',
  `id_parent` int(11) DEFAULT NULL,
  `urutan` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komoditas`
--

INSERT INTO `komoditas` (`id`, `nama`, `satuan`, `id_parent`, `urutan`) VALUES
(1, 'SAYUR - MAYUR', 'kg', NULL, 1),
(2, 'Bawang Bombay', 'kg', 1, 1),
(3, 'Bawang Merah', 'kg', 1, 1),
(4, 'Bawang Putih', 'kg', 1, 1),
(5, 'Bayam Baby', 'kg', 1, 1),
(6, 'Bayam', 'kg', 1, 1),
(7, 'Brokoli Hijau', 'kg', 1, 1),
(8, 'Brokoli Putih', 'kg', 1, 1),
(9, 'Buncis', 'kg', 1, 1),
(10, 'Cabai Keriting Hijau', 'kg', 1, 1),
(11, 'Cabai Keriting Merah', 'kg', 1, 1),
(12, 'Cabai Sreet Hijau', 'kg', 1, 1),
(13, 'Cabai Srett Merah', 'kg', 1, 1),
(14, 'Cabai Rawit Hijau', 'kg', 1, 1),
(15, 'Cabai Rawit Merah', 'kg', 1, 1),
(16, 'Cabai Teropong Hijau', 'kg', 1, 1),
(17, 'Cabai Teropong Merah', 'kg', 1, 1),
(18, 'Daun Bawang', 'kg', 1, 1),
(19, 'Daun Ketumbar', 'kg', 1, 1),
(20, 'Daun Melinjo / Daun So', 'kg', 1, 1),
(21, 'Daun Pepaya', 'kg', 1, 1),
(22, 'Daun Salam', 'kg', 1, 1),
(23, 'Daun Singkong', 'kg', 1, 1),
(24, 'Gambas', 'kg', 1, 1),
(25, 'Ilu / Bunga Onclang', 'kg', 1, 1),
(26, 'Jagung Baby', 'kg', 1, 1),
(27, 'Jagung Manis', 'kg', 1, 1),
(28, 'Jamur Tiram', 'kg', 1, 1),
(29, 'Kacang Panjang', 'kg', 1, 1),
(30, 'Jeruk Nipis', 'kg', 1, 1),
(31, 'Kangkung', 'kg', 1, 1),
(32, 'Kapri', 'kg', 1, 1),
(33, 'Kaylan', 'kg', 1, 1),
(34, 'Kaylan Baby', 'kg', 1, 1),
(35, 'Kenci/ Slada Air', 'kg', 1, 1),
(36, 'Keningkir', 'kg', 1, 1),
(37, 'Kentang', 'kg', 1, 1),
(38, 'Kentang Baby', 'kg', 1, 1),
(39, 'Kol', 'kg', 1, 1),
(40, 'Kol Merah', 'kg', 1, 1),
(41, 'Kreces / Daun Lotus', 'kg', 1, 1),
(42, 'Kucai', 'kg', 1, 1),
(43, 'Labu Siam', 'kg', 1, 1),
(44, 'Labu Siam Baby', 'kg', 1, 1),
(45, 'Lobak', 'kg', 1, 1),
(46, 'Letus', 'kg', 1, 1),
(47, 'Nanas', 'kg', 1, 1),
(48, 'Onclang', 'kg', 1, 1),
(49, 'Paprika Hijau', 'kg', 1, 1),
(50, 'Paprika Kuning', 'kg', 1, 1),
(51, 'Paprika Merah', 'kg', 1, 1),
(52, 'Pare', 'kg', 1, 1),
(53, 'Pare Merah', 'kg', 1, 1),
(54, 'Parsley', 'kg', 1, 1),
(55, 'Poiling', 'kg', 1, 1),
(56, 'Sawi Bakso', 'kg', 1, 1),
(57, 'Sawi Jabung', 'kg', 1, 1),
(58, 'Sawi Pahit', 'kg', 1, 1),
(59, 'Sawi Putih', 'kg', 1, 1),
(60, 'Sawi Sendok', 'kg', 1, 1),
(61, 'Selada', 'kg', 1, 1),
(62, 'Seledri', 'kg', 1, 1),
(63, 'Seledri Steak', 'kg', 1, 1),
(64, 'Serai', 'kg', 1, 1),
(65, 'Terong Bulat', 'kg', 1, 1),
(66, 'Terong Hijau', 'kg', 1, 1),
(67, 'Terong Ungu Besar', 'kg', 1, 1),
(68, 'Terong Ungu Kecil', 'kg', 1, 1),
(69, 'Timun Acar', 'kg', 1, 1),
(70, 'Timun Krei', 'kg', 1, 1),
(71, 'Timun Sukini / Timun Jepang', 'kg', 1, 1),
(72, 'Toge Kacang Hijau', 'kg', 1, 1),
(73, 'Toge Kedelai', 'kg', 1, 1),
(74, 'Tomat Hijau', 'kg', 1, 1),
(75, 'Tomat Merah', 'kg', 1, 1),
(76, 'Wortel Buah', 'kg', 1, 1),
(77, 'Wortel Sayur', 'kg', 1, 1),
(78, 'BUAH-BUAHAN', 'kg', NULL, 2),
(79, 'ANGGUR HIJAU', 'kg', 78, 1),
(80, 'ANGGUR MERAH', 'kg', 78, 1),
(81, 'ALPUKAT', 'kg', 78, 1),
(82, 'APEL HIJAU', 'kg', 78, 1),
(83, 'APEL MERAH', 'kg', 78, 1),
(84, 'BELIMBING', 'kg', 78, 1),
(85, 'BENGKUANG', 'kg', 78, 1),
(86, 'DURIAN MONTONG', 'kg', 78, 1),
(87, 'DUKUH', 'kg', 78, 1),
(88, 'JAMBU', 'kg', 78, 1),
(89, 'JAMBU BATU', 'kg', 78, 1),
(90, 'JERUK BALI', 'kg', 78, 1),
(91, 'JERUK LEMON', 'kg', 78, 1),
(92, 'JERUK MEDAN', 'kg', 78, 1),
(93, 'JERUK NIPIS', 'kg', 78, 1),
(94, 'JERUK PERAS', 'kg', 78, 1),
(95, 'JERUK PURUT', 'kg', 78, 1),
(96, 'JERUK SUNKIST', 'kg', 78, 1),
(97, 'KEDONDONG', 'kg', 78, 1),
(98, 'KONYAL', 'kg', 78, 1),
(99, 'MANGGA', 'kg', 78, 1),
(100, 'MELON', 'kg', 78, 1),
(101, 'NANAS', 'kg', 78, 1),
(102, 'NANGKA', 'kg', 78, 1),
(103, 'PEPAYA', 'kg', 78, 1),
(104, 'SALAK PONDOH', 'kg', 78, 1),
(105, 'SAWO', 'kg', 78, 1),
(106, 'SEMANGKA', 'kg', 78, 1),
(107, 'SIRSAK', 'kg', 78, 1),
(108, 'STRAWBERRY FRESH', 'kg', 78, 1);

-- --------------------------------------------------------

--
-- Table structure for table `komoditas_data`
--

CREATE TABLE `komoditas_data` (
  `id_komoditas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `stok` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komoditas_data`
--

INSERT INTO `komoditas_data` (`id_komoditas`, `harga`, `tanggal`, `stok`, `id_kota`, `id_user`) VALUES
(75, 1000, '2019-09-10', 0, 444, 1),
(79, 51000, '2019-09-11', 0, 444, 1),
(80, 51000, '2019-09-11', 0, 444, 1),
(81, 15000, '2019-09-11', 0, 444, 1),
(82, 18500, '2019-09-11', 0, 444, 1),
(83, 26000, '2019-09-11', 0, 444, 1),
(84, 9500, '2019-09-11', 0, 444, 1),
(85, 4500, '2019-09-11', 0, 444, 1),
(86, 88000, '2019-09-11', 0, 444, 1),
(87, 12000, '2019-09-11', 0, 444, 1),
(88, 9000, '2019-09-11', 0, 444, 1),
(89, 6000, '2019-09-11', 0, 444, 1),
(90, 10000, '2019-09-11', 0, 444, 1),
(91, 8000, '2019-09-11', 0, 444, 1),
(92, 10000, '2019-09-11', 0, 444, 1),
(93, 11000, '2019-09-11', 0, 444, 1),
(94, 8500, '2019-09-11', 0, 444, 1),
(95, 26000, '2019-09-11', 0, 444, 1),
(96, 18000, '2019-09-11', 0, 444, 1),
(97, 8000, '2019-09-11', 0, 444, 1),
(98, 14000, '2019-09-11', 0, 444, 1),
(99, 16500, '2019-09-11', 0, 444, 1),
(100, 6500, '2019-09-11', 0, 444, 1),
(101, 4000, '2019-09-11', 0, 444, 1),
(102, 4000, '2019-09-11', 0, 444, 1),
(103, 6000, '2019-09-11', 0, 444, 1),
(104, 12000, '2019-09-11', 0, 444, 1),
(105, 11000, '2019-09-11', 0, 444, 1),
(106, 5000, '2019-09-11', 0, 444, 1),
(107, 8500, '2019-09-11', 0, 444, 1),
(108, 25000, '2019-09-11', 0, 444, 1),
(79, 2300, '2019-09-10', 0, 444, 1),
(27, 5800, '2019-09-11', 0, 444, 1),
(2, 5465, '2019-09-11', 0, 2, 1),
(3, 6884, '2019-09-11', 0, 2, 1),
(4, 78646, '2019-09-11', 0, 2, 1),
(5, 4, '2019-09-11', 0, 2, 1),
(6, 4545, '2019-09-11', 0, 2, 1),
(7, 45, '2019-09-11', 0, 2, 1),
(8, 5454, '2019-09-11', 0, 2, 1),
(9, 545, '2019-09-11', 0, 2, 1),
(10, 5855, '2019-09-11', 0, 2, 1),
(11, 54, '2019-09-11', 0, 2, 1),
(12, 46, '2019-09-11', 0, 2, 1),
(13, 67, '2019-09-11', 0, 2, 1),
(14, 676, '2019-09-11', 0, 2, 1),
(15, 46, '2019-09-11', 0, 2, 1),
(16, 6764, '2019-09-11', 0, 2, 1),
(17, 64, '2019-09-11', 0, 2, 1),
(18, 66, '2019-09-11', 0, 2, 1),
(19, 46, '2019-09-11', 0, 2, 1),
(20, 45, '2019-09-11', 0, 2, 1),
(21, 46, '2019-09-11', 0, 2, 1),
(22, 64, '2019-09-11', 0, 2, 1),
(23, 649, '2019-09-11', 0, 2, 1),
(24, 646, '2019-09-11', 0, 2, 1),
(25, 64, '2019-09-11', 0, 2, 1),
(26, 64, '2019-09-11', 0, 2, 1),
(27, 64, '2019-09-11', 0, 2, 1),
(28, 636, '2019-09-11', 0, 2, 1),
(29, 46, '2019-09-11', 0, 2, 1),
(79, 645, '2019-09-11', 0, 2, 1),
(80, 866, '2019-09-11', 0, 2, 1),
(81, 645, '2019-09-11', 0, 2, 1),
(82, 454, '2019-09-11', 0, 2, 1),
(83, 645, '2019-09-11', 0, 2, 1),
(84, 4898, '2019-09-11', 0, 2, 1),
(85, 6464, '2019-09-11', 0, 2, 1),
(86, 6454, '2019-09-11', 0, 2, 1),
(87, 6545, '2019-09-11', 0, 2, 1),
(88, 6825, '2019-09-11', 0, 2, 1),
(79, 58520, '2019-09-11', 0, 256, 1),
(80, 25250, '2019-09-11', 0, 256, 1),
(81, 25510, '2019-09-11', 0, 256, 1),
(82, 25450, '2019-09-11', 0, 256, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(5) NOT NULL,
  `jenis` varchar(50) NOT NULL DEFAULT 'Kabupaten',
  `nama` varchar(100) NOT NULL DEFAULT '-',
  `id_provinsi` int(2) NOT NULL,
  `kodepos` int(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `jenis`, `nama`, `id_provinsi`, `kodepos`) VALUES
(1, 'Kabupaten', 'Aceh Barat', 21, 23681),
(2, 'Kabupaten', 'Aceh Barat Daya', 21, 23764),
(3, 'Kabupaten', 'Aceh Besar', 21, 23951),
(4, 'Kabupaten', 'Aceh Jaya', 21, 23654),
(5, 'Kabupaten', 'Aceh Selatan', 21, 23719),
(6, 'Kabupaten', 'Aceh Singkil', 21, 24785),
(7, 'Kabupaten', 'Aceh Tamiang', 21, 24476),
(8, 'Kabupaten', 'Aceh Tengah', 21, 24511),
(9, 'Kabupaten', 'Aceh Tenggara', 21, 24611),
(10, 'Kabupaten', 'Aceh Timur', 21, 24454),
(11, 'Kabupaten', 'Aceh Utara', 21, 24382),
(12, 'Kabupaten', 'Agam', 32, 26411),
(13, 'Kabupaten', 'Alor', 23, 85811),
(14, 'Kota', 'Ambon', 19, 97222),
(15, 'Kabupaten', 'Asahan', 34, 21214),
(16, 'Kabupaten', 'Asmat', 24, 99777),
(17, 'Kabupaten', 'Badung', 1, 80351),
(18, 'Kabupaten', 'Balangan', 13, 71611),
(19, 'Kota', 'Balikpapan', 15, 76111),
(20, 'Kota', 'Banda Aceh', 21, 23238),
(21, 'Kota', 'Bandar Lampung', 18, 35139),
(22, 'Kabupaten', 'Bandung', 9, 40311),
(23, 'Kota', 'Bandung', 9, 40115),
(24, 'Kabupaten', 'Bandung Barat', 9, 40721),
(25, 'Kabupaten', 'Banggai', 29, 94711),
(26, 'Kabupaten', 'Banggai Kepulauan', 29, 94881),
(27, 'Kabupaten', 'Bangka', 2, 33212),
(28, 'Kabupaten', 'Bangka Barat', 2, 33315),
(29, 'Kabupaten', 'Bangka Selatan', 2, 33719),
(30, 'Kabupaten', 'Bangka Tengah', 2, 33613),
(31, 'Kabupaten', 'Bangkalan', 11, 69118),
(32, 'Kabupaten', 'Bangli', 1, 80619),
(33, 'Kabupaten', 'Banjar', 13, 70619),
(34, 'Kota', 'Banjar', 9, 46311),
(35, 'Kota', 'Banjarbaru', 13, 70712),
(36, 'Kota', 'Banjarmasin', 13, 70117),
(37, 'Kabupaten', 'Banjarnegara', 10, 53419),
(38, 'Kabupaten', 'Bantaeng', 28, 92411),
(39, 'Kabupaten', 'Bantul', 5, 55715),
(40, 'Kabupaten', 'Banyuasin', 33, 30911),
(41, 'Kabupaten', 'Banyumas', 10, 53114),
(42, 'Kabupaten', 'Banyuwangi', 11, 68416),
(43, 'Kabupaten', 'Barito Kuala', 13, 70511),
(44, 'Kabupaten', 'Barito Selatan', 14, 73711),
(45, 'Kabupaten', 'Barito Timur', 14, 73671),
(46, 'Kabupaten', 'Barito Utara', 14, 73881),
(47, 'Kabupaten', 'Barru', 28, 90719),
(48, 'Kota', 'Batam', 17, 29413),
(49, 'Kabupaten', 'Batang', 10, 51211),
(50, 'Kabupaten', 'Batang Hari', 8, 36613),
(51, 'Kota', 'Batu', 11, 65311),
(52, 'Kabupaten', 'Batu Bara', 34, 21655),
(53, 'Kota', 'Bau-Bau', 30, 93719),
(54, 'Kabupaten', 'Bekasi', 9, 17837),
(55, 'Kota', 'Bekasi', 9, 17121),
(56, 'Kabupaten', 'Belitung', 2, 33419),
(57, 'Kabupaten', 'Belitung Timur', 2, 33519),
(58, 'Kabupaten', 'Belu', 23, 85711),
(59, 'Kabupaten', 'Bener Meriah', 21, 24581),
(60, 'Kabupaten', 'Bengkalis', 26, 28719),
(61, 'Kabupaten', 'Bengkayang', 12, 79213),
(62, 'Kota', 'Bengkulu Barat', 4, 38229),
(63, 'Kabupaten', 'Bengkulu Selatan', 4, 38519),
(64, 'Kabupaten', 'Bengkulu Tengah', 4, 38319),
(65, 'Kabupaten', 'Bengkulu Utara', 4, 38619),
(66, 'Kabupaten', 'Berau', 15, 77311),
(67, 'Kabupaten', 'Biak Numfor', 24, 98119),
(68, 'Kabupaten', 'Bima', 22, 84171),
(69, 'Kota', 'Bima', 22, 84139),
(70, 'Kota', 'Binjai', 34, 20712),
(71, 'Kabupaten', 'Bintan', 17, 29135),
(72, 'Kabupaten', 'Bireuen', 21, 24219),
(73, 'Kota', 'Bitung', 31, 95512),
(74, 'Kabupaten', 'Blitar', 11, 66171),
(75, 'Kota', 'Blitar', 11, 66124),
(76, 'Kabupaten', 'Blora', 10, 58219),
(77, 'Kabupaten', 'Boalemo', 7, 96319),
(78, 'Kabupaten', 'Bogor', 9, 16911),
(79, 'Kota', 'Bogor', 9, 16119),
(80, 'Kabupaten', 'Bojonegoro', 11, 62119),
(81, 'Kabupaten', 'Bolaang Mongondow (Bolmong)', 31, 95755),
(82, 'Kabupaten', 'Bolaang Mongondow Selatan', 31, 95774),
(83, 'Kabupaten', 'Bolaang Mongondow Timur', 31, 95783),
(84, 'Kabupaten', 'Bolaang Mongondow Utara', 31, 95765),
(85, 'Kabupaten', 'Bombana', 30, 93771),
(86, 'Kabupaten', 'Bondowoso', 11, 68219),
(87, 'Kabupaten', 'Bone', 28, 92713),
(88, 'Kabupaten', 'Bone Bolango', 7, 96511),
(89, 'Kota', 'Bontang', 15, 75313),
(90, 'Kabupaten', 'Boven Digoel', 24, 99662),
(91, 'Kabupaten', 'Boyolali', 10, 57312),
(92, 'Kabupaten', 'Brebes', 10, 52212),
(93, 'Kota', 'Bukittinggi', 32, 26115),
(94, 'Kabupaten', 'Buleleng', 1, 81111),
(95, 'Kabupaten', 'Bulukumba', 28, 92511),
(96, 'Kabupaten', 'Bulungan (Bulongan)', 16, 77211),
(97, 'Kabupaten', 'Bungo', 8, 37216),
(98, 'Kabupaten', 'Buol', 29, 94564),
(99, 'Kabupaten', 'Buru', 19, 97371),
(100, 'Kabupaten', 'Buru Selatan', 19, 97351),
(101, 'Kabupaten', 'Buton', 30, 93754),
(102, 'Kabupaten', 'Buton Utara', 30, 93745),
(103, 'Kabupaten', 'Ciamis', 9, 46211),
(104, 'Kabupaten', 'Cianjur', 9, 43217),
(105, 'Kabupaten', 'Cilacap', 10, 53211),
(106, 'Kota', 'Cilegon', 3, 42417),
(107, 'Kota', 'Cimahi', 9, 40512),
(108, 'Kabupaten', 'Cirebon', 9, 45611),
(109, 'Kota', 'Cirebon', 9, 45116),
(110, 'Kabupaten', 'Dairi', 34, 22211),
(111, 'Kabupaten', 'Deiyai (Deliyai)', 24, 98784),
(112, 'Kabupaten', 'Deli Serdang', 34, 20511),
(113, 'Kabupaten', 'Demak', 10, 59519),
(114, 'Kota', 'Denpasar', 1, 80227),
(115, 'Kota', 'Depok', 9, 16416),
(116, 'Kabupaten', 'Dharmasraya', 32, 27612),
(117, 'Kabupaten', 'Dogiyai', 24, 98866),
(118, 'Kabupaten', 'Dompu', 22, 84217),
(119, 'Kabupaten', 'Donggala', 29, 94341),
(120, 'Kota', 'Dumai', 26, 28811),
(121, 'Kabupaten', 'Empat Lawang', 33, 31811),
(122, 'Kabupaten', 'Ende', 23, 86351),
(123, 'Kabupaten', 'Enrekang', 28, 91719),
(124, 'Kabupaten', 'Fakfak', 25, 98651),
(125, 'Kabupaten', 'Flores Timur', 23, 86213),
(126, 'Kabupaten', 'Garut', 9, 44126),
(127, 'Kabupaten', 'Gayo Lues', 21, 24653),
(128, 'Kabupaten', 'Gianyar', 1, 80519),
(129, 'Kabupaten', 'Gorontalo Selatan', 7, 96218),
(130, 'Kota', 'Gorontalo', 7, 96115),
(131, 'Kabupaten', 'Gorontalo Utara', 7, 96611),
(132, 'Kabupaten', 'Gowa', 28, 92111),
(133, 'Kabupaten', 'Gresik', 11, 61115),
(134, 'Kabupaten', 'Grobogan', 10, 58111),
(135, 'Kabupaten', 'Gunung Kidul', 5, 55812),
(136, 'Kabupaten', 'Gunung Mas', 14, 74511),
(137, 'Kota', 'Gunungsitoli', 34, 22813),
(138, 'Kabupaten', 'Halmahera Barat', 20, 97757),
(139, 'Kabupaten', 'Halmahera Selatan', 20, 97911),
(140, 'Kabupaten', 'Halmahera Tengah', 20, 97853),
(141, 'Kabupaten', 'Halmahera Timur', 20, 97862),
(142, 'Kabupaten', 'Halmahera Utara', 20, 97762),
(143, 'Kabupaten', 'Hulu Sungai Selatan', 13, 71212),
(144, 'Kabupaten', 'Hulu Sungai Tengah', 13, 71313),
(145, 'Kabupaten', 'Hulu Sungai Utara', 13, 71419),
(146, 'Kabupaten', 'Humbang Hasundutan', 34, 22457),
(147, 'Kabupaten', 'Indragiri Hilir', 26, 29212),
(148, 'Kabupaten', 'Indragiri Hulu', 26, 29319),
(149, 'Kabupaten', 'Indramayu', 9, 45214),
(150, 'Kabupaten', 'Intan Jaya', 24, 98771),
(151, 'Kota', 'Jakarta Barat', 6, 11220),
(152, 'Kota', 'Jakarta Pusat', 6, 10540),
(153, 'Kota', 'Jakarta Selatan', 6, 12230),
(154, 'Kota', 'Jakarta Timur', 6, 13330),
(155, 'Kota', 'Jakarta Utara', 6, 14140),
(156, 'Kota', 'Jambi', 8, 36111),
(157, 'Kabupaten', 'Jayapura', 24, 99352),
(158, 'Kota', 'Jayapura', 24, 99114),
(159, 'Kabupaten', 'Jayawijaya', 24, 99511),
(160, 'Kabupaten', 'Jember', 11, 68113),
(161, 'Kabupaten', 'Jembrana', 1, 82251),
(162, 'Kabupaten', 'Jeneponto', 28, 92319),
(163, 'Kabupaten', 'Jepara', 10, 59419),
(164, 'Kabupaten', 'Jombang', 11, 61415),
(165, 'Kabupaten', 'Kaimana', 25, 98671),
(166, 'Kabupaten', 'Kampar', 26, 28411),
(167, 'Kabupaten', 'Kapuas', 14, 73583),
(168, 'Kabupaten', 'Kapuas Hulu', 12, 78719),
(169, 'Kabupaten', 'Karanganyar', 10, 57718),
(170, 'Kabupaten', 'Karangasem', 1, 80819),
(171, 'Kabupaten', 'Karawang', 9, 41311),
(172, 'Kabupaten', 'Karimun', 17, 29611),
(173, 'Kabupaten', 'Karo', 34, 22119),
(174, 'Kabupaten', 'Katingan', 14, 74411),
(175, 'Kabupaten', 'Kaur', 4, 38911),
(176, 'Kabupaten', 'Kayong Utara', 12, 78852),
(177, 'Kabupaten', 'Kebumen', 10, 54319),
(178, 'Kabupaten', 'Kediri', 11, 64184),
(179, 'Kota', 'Kediri', 11, 64125),
(180, 'Kabupaten', 'Keerom', 24, 99461),
(181, 'Kabupaten', 'Kendal', 10, 51314),
(182, 'Kota', 'Kendari', 30, 93126),
(183, 'Kabupaten', 'Kepahiang', 4, 39319),
(184, 'Kabupaten', 'Kepulauan Anambas', 17, 29991),
(185, 'Kabupaten', 'Kepulauan Aru', 19, 97681),
(186, 'Kabupaten', 'Kepulauan Mentawai', 32, 25771),
(187, 'Kabupaten', 'Kepulauan Meranti', 26, 28791),
(188, 'Kabupaten', 'Kepulauan Sangihe', 31, 95819),
(189, 'Kabupaten', 'Kepulauan Seribu', 6, 14550),
(190, 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 31, 95862),
(191, 'Kabupaten', 'Kepulauan Sula', 20, 97995),
(192, 'Kabupaten', 'Kepulauan Talaud', 31, 95885),
(193, 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', 24, 98211),
(194, 'Kabupaten', 'Kerinci', 8, 37167),
(195, 'Kabupaten', 'Ketapang', 12, 78874),
(196, 'Kabupaten', 'Klaten', 10, 57411),
(197, 'Kabupaten', 'Klungkung', 1, 80719),
(198, 'Kabupaten', 'Kolaka', 30, 93511),
(199, 'Kabupaten', 'Kolaka Utara', 30, 93911),
(200, 'Kabupaten', 'Konawe', 30, 93411),
(201, 'Kabupaten', 'Konawe Selatan', 30, 93811),
(202, 'Kabupaten', 'Konawe Utara', 30, 93311),
(203, 'Kabupaten', 'Kotabaru', 13, 72119),
(204, 'Kota', 'Kotamobagu', 31, 95711),
(205, 'Kabupaten', 'Kotawaringin Barat', 14, 74119),
(206, 'Kabupaten', 'Kotawaringin Timur', 14, 74364),
(207, 'Kabupaten', 'Kuantan Singingi', 26, 29519),
(208, 'Kabupaten', 'Kubu Raya', 12, 78311),
(209, 'Kabupaten', 'Kudus', 10, 59311),
(210, 'Kabupaten', 'Kulon Progo', 5, 55611),
(211, 'Kabupaten', 'Kuningan', 9, 45511),
(212, 'Kabupaten', 'Kupang', 23, 85362),
(213, 'Kota', 'Kupang', 23, 85119),
(214, 'Kabupaten', 'Kutai Barat', 15, 75711),
(215, 'Kabupaten', 'Kutai Kartanegara', 15, 75511),
(216, 'Kabupaten', 'Kutai Timur', 15, 75611),
(217, 'Kabupaten', 'Labuhan Batu', 34, 21412),
(218, 'Kabupaten', 'Labuhan Batu Selatan', 34, 21511),
(219, 'Kabupaten', 'Labuhan Batu Utara', 34, 21711),
(220, 'Kabupaten', 'Lahat', 33, 31419),
(221, 'Kabupaten', 'Lamandau', 14, 74611),
(222, 'Kabupaten', 'Lamongan', 11, 64125),
(223, 'Kabupaten', 'Lampung Barat', 18, 34814),
(224, 'Kabupaten', 'Lampung Selatan', 18, 35511),
(225, 'Kabupaten', 'Lampung Tengah', 18, 34212),
(226, 'Kabupaten', 'Lampung Timur', 18, 34319),
(227, 'Kabupaten', 'Lampung Utara', 18, 34516),
(228, 'Kabupaten', 'Landak', 12, 78319),
(229, 'Kabupaten', 'Langkat', 34, 20811),
(230, 'Kota', 'Langsa', 21, 24412),
(231, 'Kabupaten', 'Lanny Jaya', 24, 99531),
(232, 'Kabupaten', 'Lebak', 3, 42319),
(233, 'Kabupaten', 'Lebong', 4, 39264),
(234, 'Kabupaten', 'Lembata', 23, 86611),
(235, 'Kota', 'Lhokseumawe', 21, 24352),
(236, 'Kabupaten', 'Lima Puluh Koto/Kota', 32, 26671),
(237, 'Kabupaten', 'Lingga', 17, 29811),
(238, 'Kabupaten', 'Lombok Barat', 22, 83311),
(239, 'Kabupaten', 'Lombok Tengah', 22, 83511),
(240, 'Kabupaten', 'Lombok Timur', 22, 83612),
(241, 'Kabupaten', 'Lombok Utara', 22, 83711),
(242, 'Kota', 'Lubuk Linggau', 33, 31614),
(243, 'Kabupaten', 'Lumajang', 11, 67319),
(244, 'Kabupaten', 'Luwu', 28, 91994),
(245, 'Kabupaten', 'Luwu Timur', 28, 92981),
(246, 'Kabupaten', 'Luwu Utara', 28, 92911),
(247, 'Kabupaten', 'Madiun', 11, 63153),
(248, 'Kota', 'Madiun', 11, 63122),
(249, 'Kabupaten', 'Magelang', 10, 56519),
(250, 'Kota', 'Magelang', 10, 56133),
(251, 'Kabupaten', 'Magetan', 11, 63314),
(252, 'Kabupaten', 'Majalengka', 9, 45412),
(253, 'Kabupaten', 'Majene', 27, 91411),
(254, 'Kota', 'Makassar', 28, 90111),
(255, 'Kabupaten', 'Malang', 11, 65163),
(256, 'Kota', 'Malang', 11, 65112),
(257, 'Kabupaten', 'Malinau', 16, 77511),
(258, 'Kabupaten', 'Maluku Barat Daya', 19, 97451),
(259, 'Kabupaten', 'Maluku Tengah', 19, 97513),
(260, 'Kabupaten', 'Maluku Tenggara', 19, 97651),
(261, 'Kabupaten', 'Maluku Tenggara Barat', 19, 97465),
(262, 'Kabupaten', 'Mamasa', 27, 91362),
(263, 'Kabupaten', 'Mamberamo Raya', 24, 99381),
(264, 'Kabupaten', 'Mamberamo Tengah', 24, 99553),
(265, 'Kabupaten', 'Mamuju', 27, 91519),
(266, 'Kabupaten', 'Mamuju Utara', 27, 91571),
(267, 'Kota', 'Manado', 31, 95247),
(268, 'Kabupaten', 'Mandailing Natal', 34, 22916),
(269, 'Kabupaten', 'Manggarai', 23, 86551),
(270, 'Kabupaten', 'Manggarai Barat', 23, 86711),
(271, 'Kabupaten', 'Manggarai Timur', 23, 86811),
(272, 'Kabupaten', 'Manokwari', 25, 98311),
(273, 'Kabupaten', 'Manokwari Selatan', 25, 98355),
(274, 'Kabupaten', 'Mappi', 24, 99853),
(275, 'Kabupaten', 'Maros', 28, 90511),
(276, 'Kota', 'Mataram', 22, 83131),
(277, 'Kabupaten', 'Maybrat', 25, 98051),
(278, 'Kota', 'Medan', 34, 20228),
(279, 'Kabupaten', 'Melawi', 12, 78619),
(280, 'Kabupaten', 'Merangin', 8, 37319),
(281, 'Kabupaten', 'Merauke', 24, 99613),
(282, 'Kabupaten', 'Mesuji', 18, 34911),
(283, 'Kota', 'Metro', 18, 34111),
(284, 'Kabupaten', 'Mimika', 24, 99962),
(285, 'Kabupaten', 'Minahasa', 31, 95614),
(286, 'Kabupaten', 'Minahasa Selatan', 31, 95914),
(287, 'Kabupaten', 'Minahasa Tenggara', 31, 95995),
(288, 'Kabupaten', 'Minahasa Utara', 31, 95316),
(289, 'Kabupaten', 'Mojokerto', 11, 61382),
(290, 'Kota', 'Mojokerto', 11, 61316),
(291, 'Kabupaten', 'Morowali', 29, 94911),
(292, 'Kabupaten', 'Muara Enim', 33, 31315),
(293, 'Kabupaten', 'Muaro Jambi', 8, 36311),
(294, 'Kabupaten', 'Muko Muko', 4, 38715),
(295, 'Kabupaten', 'Muna', 30, 93611),
(296, 'Kabupaten', 'Murung Raya', 14, 73911),
(297, 'Kabupaten', 'Musi Banyuasin', 33, 30719),
(298, 'Kabupaten', 'Musi Rawas', 33, 31661),
(299, 'Kabupaten', 'Nabire', 24, 98816),
(300, 'Kabupaten', 'Nagan Raya', 21, 23674),
(301, 'Kabupaten', 'Nagekeo', 23, 86911),
(302, 'Kabupaten', 'Natuna', 17, 29711),
(303, 'Kabupaten', 'Nduga', 24, 99541),
(304, 'Kabupaten', 'Ngada', 23, 86413),
(305, 'Kabupaten', 'Nganjuk', 11, 64414),
(306, 'Kabupaten', 'Ngawi', 11, 63219),
(307, 'Kabupaten', 'Nias', 34, 22876),
(308, 'Kabupaten', 'Nias Barat', 34, 22895),
(309, 'Kabupaten', 'Nias Selatan', 34, 22865),
(310, 'Kabupaten', 'Nias Utara', 34, 22856),
(311, 'Kabupaten', 'Nunukan', 16, 77421),
(312, 'Kabupaten', 'Ogan Ilir', 33, 30811),
(313, 'Kabupaten', 'Ogan Komering Ilir', 33, 30618),
(314, 'Kabupaten', 'Ogan Komering Ulu', 33, 32112),
(315, 'Kabupaten', 'Ogan Komering Ulu Selatan', 33, 32211),
(316, 'Kabupaten', 'Ogan Komering Ulu Timur', 33, 32312),
(317, 'Kabupaten', 'Pacitan', 11, 63512),
(318, 'Kota', 'Padang', 32, 25112),
(319, 'Kabupaten', 'Padang Lawas', 34, 22763),
(320, 'Kabupaten', 'Padang Lawas Utara', 34, 22753),
(321, 'Kota', 'Padang Panjang', 32, 27122),
(322, 'Kabupaten', 'Padang Pariaman', 32, 25583),
(323, 'Kota', 'Padang Sidempuan', 34, 22727),
(324, 'Kota', 'Pagar Alam', 33, 31512),
(325, 'Kabupaten', 'Pakpak Bharat', 34, 22272),
(326, 'Kota', 'Palangka Raya', 14, 73112),
(327, 'Kota', 'Palembang', 33, 31512),
(328, 'Kota', 'Palopo', 28, 91911),
(329, 'Kota', 'Palu', 29, 94111),
(330, 'Kabupaten', 'Pamekasan', 11, 69319),
(331, 'Kabupaten', 'Pandeglang', 3, 42212),
(332, 'Kabupaten', 'Pangandaran', 9, 46511),
(333, 'Kabupaten', 'Pangkajene Kepulauan', 28, 90611),
(334, 'Kota', 'Pangkal Pinang', 2, 33115),
(335, 'Kabupaten', 'Paniai', 24, 98765),
(336, 'Kota', 'Parepare', 28, 91123),
(337, 'Kota', 'Pariaman', 32, 25511),
(338, 'Kabupaten', 'Parigi Moutong', 29, 94411),
(339, 'Kabupaten', 'Pasaman', 32, 26318),
(340, 'Kabupaten', 'Pasaman Barat', 32, 26511),
(341, 'Kabupaten', 'Paser', 15, 76211),
(342, 'Kabupaten', 'Pasuruan', 11, 67153),
(343, 'Kota', 'Pasuruan', 11, 67118),
(344, 'Kabupaten', 'Pati', 10, 59114),
(345, 'Kota', 'Payakumbuh', 32, 26213),
(346, 'Kabupaten', 'Pegunungan Arfak', 25, 98354),
(347, 'Kabupaten', 'Pegunungan Bintang', 24, 99573),
(348, 'Kabupaten', 'Pekalongan', 10, 51161),
(349, 'Kota', 'Pekalongan', 10, 51122),
(350, 'Kota', 'Pekanbaru', 26, 28112),
(351, 'Kabupaten', 'Pelalawan', 26, 28311),
(352, 'Kabupaten', 'Pemalang', 10, 52319),
(353, 'Kota', 'Pematang Siantar', 34, 21126),
(354, 'Kabupaten', 'Penajam Paser Utara', 15, 76311),
(355, 'Kabupaten', 'Pesawaran', 18, 35312),
(356, 'Kabupaten', 'Pesisir Barat', 18, 35974),
(357, 'Kabupaten', 'Pesisir Selatan', 32, 25611),
(358, 'Kabupaten', 'Pidie', 21, 24116),
(359, 'Kabupaten', 'Pidie Jaya', 21, 24186),
(360, 'Kabupaten', 'Pinrang', 28, 91251),
(361, 'Kabupaten', 'Pohuwato', 7, 96419),
(362, 'Kabupaten', 'Polewali Mandar', 27, 91311),
(363, 'Kabupaten', 'Ponorogo', 11, 63411),
(364, 'Kabupaten', 'Pontianak', 12, 78971),
(365, 'Kota', 'Pontianak', 12, 78112),
(366, 'Kabupaten', 'Poso', 29, 94615),
(367, 'Kota', 'Prabumulih', 33, 31121),
(368, 'Kabupaten', 'Pringsewu', 18, 35719),
(369, 'Kabupaten', 'Probolinggo', 11, 67282),
(370, 'Kota', 'Probolinggo', 11, 67215),
(371, 'Kabupaten', 'Pulang Pisau', 14, 74811),
(372, 'Kabupaten', 'Pulau Morotai', 20, 97771),
(373, 'Kabupaten', 'Puncak', 24, 98981),
(374, 'Kabupaten', 'Puncak Jaya', 24, 98979),
(375, 'Kabupaten', 'Purbalingga', 10, 53312),
(376, 'Kabupaten', 'Purwakarta', 9, 41119),
(377, 'Kabupaten', 'Purworejo', 10, 54111),
(378, 'Kabupaten', 'Raja Ampat', 25, 98489),
(379, 'Kabupaten', 'Rejang Lebong', 4, 39112),
(380, 'Kabupaten', 'Rembang', 10, 59219),
(381, 'Kabupaten', 'Rokan Hilir', 26, 28992),
(382, 'Kabupaten', 'Rokan Hulu', 26, 28511),
(383, 'Kabupaten', 'Rote Ndao', 23, 85982),
(384, 'Kota', 'Sabang', 21, 23512),
(385, 'Kabupaten', 'Sabu Raijua', 23, 85391),
(386, 'Kota', 'Salatiga', 10, 50711),
(387, 'Kota', 'Samarinda', 15, 75133),
(388, 'Kabupaten', 'Sambas', 12, 79453),
(389, 'Kabupaten', 'Samosir', 34, 22392),
(390, 'Kabupaten', 'Sampang', 11, 69219),
(391, 'Kabupaten', 'Sanggau', 12, 78557),
(392, 'Kabupaten', 'Sarmi', 24, 99373),
(393, 'Kabupaten', 'Sarolangun', 8, 37419),
(394, 'Kota', 'Sawah Lunto', 32, 27416),
(395, 'Kabupaten', 'Sekadau', 12, 79583),
(396, 'Kabupaten', 'Selayar (Kepulauan Selayar)', 28, 92812),
(397, 'Kabupaten', 'Seluma', 4, 38811),
(398, 'Kabupaten', 'Semarang', 10, 50511),
(399, 'Kota', 'Semarang', 10, 50135),
(400, 'Kabupaten', 'Seram Bagian Barat', 19, 97561),
(401, 'Kabupaten', 'Seram Bagian Timur', 19, 97581),
(402, 'Kabupaten', 'Serang', 3, 42182),
(403, 'Kota', 'Serang', 3, 42111),
(404, 'Kabupaten', 'Serdang Bedagai', 34, 20915),
(405, 'Kabupaten', 'Seruyan', 14, 74211),
(406, 'Kabupaten', 'Siak', 26, 28623),
(407, 'Kota', 'Sibolga', 34, 22522),
(408, 'Kabupaten', 'Sidenreng Rappang/Rapang', 28, 91613),
(409, 'Kabupaten', 'Sidoarjo', 11, 61219),
(410, 'Kabupaten', 'Sigi', 29, 94364),
(411, 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', 32, 27511),
(412, 'Kabupaten', 'Sikka', 23, 86121),
(413, 'Kabupaten', 'Simalungun', 34, 21162),
(414, 'Kabupaten', 'Simeulue', 21, 23891),
(415, 'Kota', 'Singkawang', 12, 79117),
(416, 'Kabupaten', 'Sinjai', 28, 92615),
(417, 'Kabupaten', 'Sintang', 12, 78619),
(418, 'Kabupaten', 'Situbondo', 11, 68316),
(419, 'Kabupaten', 'Sleman', 5, 55513),
(420, 'Kabupaten', 'Solok', 32, 27365),
(421, 'Kota', 'Solok', 32, 27315),
(422, 'Kabupaten', 'Solok Selatan', 32, 27779),
(423, 'Kabupaten', 'Soppeng', 28, 90812),
(424, 'Kabupaten', 'Sorong', 25, 98431),
(425, 'Kota', 'Sorong', 25, 98411),
(426, 'Kabupaten', 'Sorong Selatan', 25, 98454),
(427, 'Kabupaten', 'Sragen', 10, 57211),
(428, 'Kabupaten', 'Subang', 9, 41215),
(429, 'Kota', 'Subulussalam', 21, 24882),
(430, 'Kabupaten', 'Sukabumi', 9, 43311),
(431, 'Kota', 'Sukabumi', 9, 43114),
(432, 'Kabupaten', 'Sukamara', 14, 74712),
(433, 'Kabupaten', 'Sukoharjo', 10, 57514),
(434, 'Kabupaten', 'Sumba Barat', 23, 87219),
(435, 'Kabupaten', 'Sumba Barat Daya', 23, 87453),
(436, 'Kabupaten', 'Sumba Tengah', 23, 87358),
(437, 'Kabupaten', 'Sumba Timur', 23, 87112),
(438, 'Kabupaten', 'Sumbawa', 22, 84315),
(439, 'Kabupaten', 'Sumbawa Barat', 22, 84419),
(440, 'Kabupaten', 'Sumedang', 9, 45326),
(441, 'Kabupaten', 'Sumenep', 11, 69413),
(442, 'Kota', 'Sungaipenuh', 8, 37113),
(443, 'Kabupaten', 'Supiori', 24, 98164),
(444, 'Kota', 'Surabaya', 11, 60119),
(445, 'Kota', 'Surakarta (Solo)', 10, 57113),
(446, 'Kabupaten', 'Tabalong', 13, 71513),
(447, 'Kabupaten', 'Tabanan', 1, 82119),
(448, 'Kabupaten', 'Takalar', 28, 92212),
(449, 'Kabupaten', 'Tambrauw', 25, 98475),
(450, 'Kabupaten', 'Tana Tidung', 16, 77611),
(451, 'Kabupaten', 'Tana Toraja', 28, 91819),
(452, 'Kabupaten', 'Tanah Bumbu', 13, 72211),
(453, 'Kabupaten', 'Tanah Datar', 32, 27211),
(454, 'Kabupaten', 'Tanah Laut', 13, 70811),
(455, 'Kabupaten', 'Tangerang', 3, 15914),
(456, 'Kota', 'Tangerang', 3, 15111),
(457, 'Kota', 'Tangerang Selatan', 3, 15332),
(458, 'Kabupaten', 'Tanggamus', 18, 35619),
(459, 'Kota', 'Tanjung Balai', 34, 21321),
(460, 'Kabupaten', 'Tanjung Jabung Barat', 8, 36513),
(461, 'Kabupaten', 'Tanjung Jabung Timur', 8, 36719),
(462, 'Kota', 'Tanjung Pinang', 17, 29111),
(463, 'Kabupaten', 'Tapanuli Selatan', 34, 22742),
(464, 'Kabupaten', 'Tapanuli Tengah', 34, 22611),
(465, 'Kabupaten', 'Tapanuli Utara', 34, 22414),
(466, 'Kabupaten', 'Tapin', 13, 71119),
(467, 'Kota', 'Tarakan', 16, 77114),
(468, 'Kabupaten', 'Tasikmalaya', 9, 46411),
(469, 'Kota', 'Tasikmalaya', 9, 46116),
(470, 'Kota', 'Tebing Tinggi', 34, 20632),
(471, 'Kabupaten', 'Tebo', 8, 37519),
(472, 'Kabupaten', 'Tegal', 10, 52419),
(473, 'Kota', 'Tegal', 10, 52114),
(474, 'Kabupaten', 'Teluk Bintuni', 25, 98551),
(475, 'Kabupaten', 'Teluk Wondama', 25, 98591),
(476, 'Kabupaten', 'Temanggung', 10, 56212),
(477, 'Kota', 'Ternate', 20, 97714),
(478, 'Kota', 'Tidore Kepulauan', 20, 97815),
(479, 'Kabupaten', 'Timor Tengah Selatan', 23, 85562),
(480, 'Kabupaten', 'Timor Tengah Utara', 23, 85612),
(481, 'Kabupaten', 'Toba Samosir', 34, 22316),
(482, 'Kabupaten', 'Tojo Una-Una', 29, 94683),
(483, 'Kabupaten', 'Toli-Toli', 29, 94542),
(484, 'Kabupaten', 'Tolikara', 24, 99411),
(485, 'Kota', 'Tomohon', 31, 95416),
(486, 'Kabupaten', 'Toraja Utara', 28, 91831),
(487, 'Kabupaten', 'Trenggalek', 11, 66312),
(488, 'Kota', 'Tual', 19, 97612),
(489, 'Kabupaten', 'Tuban', 11, 62319),
(490, 'Kabupaten', 'Tulang Bawang', 18, 34613),
(491, 'Kabupaten', 'Tulang Bawang Barat', 18, 34419),
(492, 'Kabupaten', 'Tulungagung', 11, 66212),
(493, 'Kabupaten', 'Wajo', 28, 90911),
(494, 'Kabupaten', 'Wakatobi', 30, 93791),
(495, 'Kabupaten', 'Waropen', 24, 98269),
(496, 'Kabupaten', 'Way Kanan', 18, 34711),
(497, 'Kabupaten', 'Wonogiri', 10, 57619),
(498, 'Kabupaten', 'Wonosobo', 10, 56311),
(499, 'Kabupaten', 'Yahukimo', 24, 99041),
(500, 'Kabupaten', 'Yalimo', 24, 99481),
(501, 'Kota', 'Yogyakarta', 5, 55222);

-- --------------------------------------------------------

--
-- Table structure for table `pasar`
--

CREATE TABLE `pasar` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `id_kota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasar`
--

INSERT INTO `pasar` (`id`, `nama`, `id_kota`) VALUES
(1, 'Pasar ABC', 1),
(2, 'Pasar BCA', 31),
(3, 'Pasar Haha Loh', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama`, `data`) VALUES
(1, 'komoditasandalan', '37,75,79,82,101'),
(2, 'namawebsite', 'Sun||Biz||PT. Indraco'),
(3, 'detailnamawebsite', 'Sun Biz PT Indraco adalah ......');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id` int(2) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id`, `nama`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam'),
(22, 'Nusa Tenggara Barat '),
(23, 'Nusa Tenggara Timur '),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `username` varchar(200) NOT NULL,
  `level` enum('admin','contributor') NOT NULL DEFAULT 'contributor',
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `remember_token` varchar(200) NOT NULL DEFAULT '-',
  `email_verified_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `level`, `email`, `password`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(1, 'saiful', 'saiful', 'contributor', 'saiful@gmail.com', '$2y$10$qw3IoXs7esemH.GfhMt/WuRzBecdhrSooh6fIfX9vr0fe5U0FhTka', 'vxFsbdGUZjyb4vMVC8xN93BB6whdTzA2E9otGk4Kjm5oJ254gjEnugCQTNy3', NULL, '2019-09-04 07:06:08', '2019-09-04 07:06:08'),
(2, 'admin', 'admin', 'admin', 'admin@gmail.com', '$2y$10$D1erSMgLpyoviQWtYUCXN.scwUVtXWNq9gnZv.mB//ndfRHwmlUba', 'AxYuHcP4eLOv8ti9DY426IbYfYHZ9N70xpKUEEqrMImEIITtraht9i8eVcxR', NULL, '2019-09-04 07:06:08', '2019-09-04 07:06:08'),
(3, 'ff', 'ff', 'contributor', 'ff@gmail.com', '$2y$10$QH1TQwmROViHOG3.KKi2JOjIykLDraiOvhaKMA7frc/7SoCqbg6ai', '-', NULL, '2019-09-11 01:39:22', '2019-09-11 01:39:22'),
(4, 'dada', 'dada', 'contributor', 'dadadudu@gmail.com', '$2y$10$Uoto6OdONvJerisVJoO0v.MRJgbnrH.FtsG3bVYO1s8JLzFWFJOBy', '-', NULL, '2019-09-11 06:38:31', '2019-09-11 06:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `user_kota`
--

CREATE TABLE `user_kota` (
  `id_user` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_kota`
--

INSERT INTO `user_kota` (`id_user`, `id_kota`) VALUES
(3, 256),
(3, 280),
(4, 48),
(4, 19),
(1, 444),
(1, 2),
(1, 256);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `komoditas`
--
ALTER TABLE `komoditas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_provinsi` (`id_provinsi`);

--
-- Indexes for table `pasar`
--
ALTER TABLE `pasar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pasar_kota` (`id_kota`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komoditas`
--
ALTER TABLE `komoditas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `pasar`
--
ALTER TABLE `pasar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kota`
--
ALTER TABLE `kota`
  ADD CONSTRAINT `kota_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pasar`
--
ALTER TABLE `pasar`
  ADD CONSTRAINT `pasar_kota` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
