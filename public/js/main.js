function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.previewimage'+$(input).attr('data-name')).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
$(".inputimage").change(function() {
  readURL(this);
});
function getlabelstatustransaksi(status=''){
  var html='';
  switch (status) {
    case 'pendingadmin':
      html+='<span class="label label-lg label-default pull-right">Pending Admin</span>';
      break;
    case 'pendingpayment':
      html+='<span class="label label-lg label-warning pull-right">Menunggu Pembayaran</span>';
      break;
    case 'rejected':
      html+='<span class="label label-lg label-danger pull-right">Ditolak</span>';
      break;
    default:
      html+='<span class="label label-lg label-success pull-right">'+status+'</span>';
      break;
  }
  return html;
}
function kodereplace(format,datatambahan){
  var date = new Date();
  var koder = {
    'tahun':date.getFullYear(),
    'bulan':date.getMonth()+1,
    'tanggal':date.getDate(),
  };
  $.each(koder,function(index,value){
    format=format.split('[['+index+']]').join(value);
  });
  $.each(datatambahan,function(index,value){
    format=format.split('[['+index+']]').join(value);
  });
  return format;
}

function replacevariabeldokumen(teks,datas){
  $.each(datas, function( index, data ) {
    var datalama=new RegExp('{{'+index+'}}','g');
    teks=teks.replace(datalama,data);
  });
  return teks;
}
function imageExists(image_url){
    var http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status != 404;
}
